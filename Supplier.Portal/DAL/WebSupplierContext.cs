﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Supplier.Portal.DAL.WebEntities;

namespace Supplier.Portal.DAL
{
    public class WebSupplierContext :DbContext
    {
        public WebSupplierContext(): base("Name=WebSupplierContext")
        {
            Database.SetInitializer<WebSupplierContext>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<OrderShippingEntity> OrderShipping { get; set; }

        public DbSet<TransactionEntity> Order { get; set; }

        public DbSet<BasketEntity> Basket { get; set; }

        public DbSet<BaskeLineEntity> BasketLine { get; set; }

        public DbSet<SkuEntity> Sku { get; set; }

        public DbSet<SkuSupplierEntity> SkuSupplier { get; set; }

        public DbSet<TelephoneEntity> Telephone { get; set; }

        public DbSet<CountryEntity> Country { get; set; }
    }
}