﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Supplier.Portal.DAL.Entities
{
    [Table("Company")]
    public class CompanyEntity
    {
        [Key]
        public int CompanyId { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        
        public int? CountryId { get; set; }

        [MaxLength(50)]
        public string Line1 { get; set; }

        [MaxLength(50)]
        public string Line2 { get; set; }

        [MaxLength(50)]
        public string Town { get; set; }

        [MaxLength(50)]
        public string County { get; set; }

        [MaxLength(10)]
        public string PostCode { get; set; }

        [MaxLength(15)]
        public string TelePhone { get; set; }

        [MaxLength(3)]
        public string Extension { get; set; }

        [MaxLength(15)]
        public string Fax { get; set; }
        
        [MaxLength(10)]
        public string UpsAccountNumber { get; set; }

        public virtual ICollection<SupplierEntity> Suppliers { get; set; }
    }
}