﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Supplier.Portal.DAL.Entities
{
    [Table("FileImportLog")]
    public class FileimportLogEntity
    {
        public FileimportLogEntity()
        {

        }

        public FileimportLogEntity(string fileName)
        {
            this.FileName = fileName;
            this.ImportDate = DateTime.Now;
            this.SkusUpdated = 0;
        }

        [Key]
        public long LogId { get; set; }

        [Required]
        [MaxLength(50)]
        public string FileName { get; set; }

        [Required]
        public DateTime ImportDate { get; set; }

        [Required]
        public int CompanyId { get; set; }

        [Required]
        public int SkusUpdated { get; set; }

        public string ErrorDescription { get; set; }
    }
}