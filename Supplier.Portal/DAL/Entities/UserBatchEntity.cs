﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Supplier.Portal.DAL.Entities
{
    [Table("UserBatch")]
    public class UserBatchEntity
    {
        [Key]
        public int UserBatchId { get; set; }

        [Required]
        public int UserId { get; set; }

        [Required]
        public int BatchNumber { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime BatchDate { get; set; }

        [Required]
        public bool BatchCompleted { get; set; }

        [DataType(DataType.Date)]
        public DateTime? DatePicked { get; set; }
    }
}