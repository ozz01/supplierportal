﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Supplier.Portal.DAL.Entities
{
    [Table("Supplier")]
    public class SupplierEntity
    {
        [Key]
        public int SupplierId { get; set; }

        [Required]
        public int CompanyId { get; set; }

        [MaxLength(10)]
        public string Code { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        
        [Required]
        [MaxLength(50)]
        public string AddressLine1 { get; set; }

        [Required]
        [MaxLength(50)]
        public string AddressLine2 { get; set; }
        
        [MaxLength(50)]
        public string Town { get; set; }

        [MaxLength(50)]
        public string County { get; set; }

        [Required]
        [MaxLength(5)]
        public string CountyCode { get; set; }

        [Required]
        [MaxLength(10)]
        public string Postcode { get; set; }

        [Required]
        [MaxLength(15)]
        public string TelePhone { get; set; }

        [MaxLength(3)]
        public string Extension { get; set; }

        [MaxLength(15)]
        public string Fax { get; set; }

        [MaxLength(100)]
        public string ShipperAttnName { get; set; }

        [Required]
        [MaxLength(150)]
        public string LabelPrinterName { get; set; }

        [Required]
        [MaxLength(150)]
        public string StandarPrinterName { get; set; }

        [MaxLength(20)]
        public string LabelPrinterIP { get; set; }

        [Required]
        [MaxLength(20)]
        public string UpsAccountNumber { get; set; }
    }
}