﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Supplier.Portal.DAL.Entities
{
    [Table("User")]
    public class UserEntity
    {
        [Key]
        public int UserId { get; set; }

        [Required]
        [MaxLength(100)]
        public string Username { get; set; }

        [Required]
        [MaxLength(150)]
        public string HashedPassword { get; set; }
        
        [Required]
        [MaxLength(10)]
        public string Title { get; set; }

        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }

        [Required]
        [MaxLength(250)]
        public string Email { get; set; }

        [MaxLength(15)]
        [Column("PhoneMobile")]
        public string Phone_Mobile { get; set; }

        [MaxLength(15)]
        [Column("PhoneOffice")]
        public string Phone_Office { get; set; }


        [Required]
        public int RoleId { get; set; }

        public virtual RoleEntity Role { get; set; }


        public int? CompanyId { get; set; }

        public virtual CompanyEntity Company { get; set; }


        public int? SupplierId { get; set; }

        public virtual SupplierEntity Supplier { get; set; }        
    }
}