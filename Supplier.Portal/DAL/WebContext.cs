﻿using System.Data.Entity;

namespace Supplier.Portal.DAL
{
    public class WebContext :DbContext
    {
        public WebContext(): base("Name=WebContext")
        {
            Database.SetInitializer<WebContext>(null);
        }
    }
}