﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Supplier.Portal.DAL.Entities;

namespace Supplier.Portal.DAL
{
    public class SupplierContext : DbContext
    {
        public SupplierContext(): base("name=SuppliersContext")
        {
            Database.SetInitializer<SupplierContext>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<CompanyEntity> Company { get; set; }

        public DbSet<SupplierEntity> Supplier { get; set; }
        
        public DbSet<RoleEntity> Role { get; set; }
        
        public DbSet<UserEntity> User { get; set; }
        
        public DbSet<FileimportLogEntity> ImportLog { get; set; }
       
        public DbSet<UserBatchEntity> UserBatch { get; set; }
    }
}

