﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Supplier.Portal.DAL.WebEntities
{
    [Table("commerce_Sku_Supplier")]
    public class SkuSupplierEntity
    {
        [Required]
        [Key, Column(Order = 0)]
        public long SkuId { get; set; }

        [Required]
        [MaxLength(12)]
        [Key, Column(Order = 1)]
        public string SupplierCode { get; set; }
    }
}