﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Supplier.Portal.DAL.WebEntities
{
    [Table("core_Country")]
    public class CountryEntity
    {
        [Key]
        [Required]
        public long CountryId { get; set; }

        [Required]
        [MaxLength(255)]
        public string Name { get; set; }

        [Column("ISO3166Alpha2")]
        public string ShortCountryCode { get; set; }
    }
}