﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Supplier.Portal.DAL.WebEntities
{
    [Table("core_Telephone")]
    public class TelephoneEntity
    {
        [Key]
        [Required]
        public long TelePhoneId { get; set; }

        [Required]
        public long PersonId { get; set; }

        [MaxLength(255)]
        [Required]
        public string Number { get; set; }

        [Required]
        public bool IsPrimary { get; set; }
    }
}