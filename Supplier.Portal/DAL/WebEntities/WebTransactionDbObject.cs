﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Linq;
using Supplier.Portal.Helpers;

namespace Supplier.Portal.DAL.WebEntities
{
    public class WebTransactionDbObject
    {
        public long TransactionID { get; set; }
        public long TransactionTypeID { get; set; }
        public long PaymentMethodID { get; set; }   
        public string PaymentMethod { get; set; }
        public string TransactionType { get; set; }
        public long CurrencyID { get; set; }
        public string Currency { get; set; }
        public long TransactionStatusID { get; set; } 
        public string TransactionStatus { get; set; }
        public decimal AmountInc { get; set; }
        public decimal AmountEx { get; set; }
        public decimal VAT { get; set; }
        public Guid? UserAccountID { get; set; }
        public long TrackedVisitorID { get; set; }
        public string PurchaseXml { get; set; }
        public string PaymentXml { get; set; }
        public string CheckoutXml { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastModified { get; set; }
        public bool Deleted { get; set; }
        public string OrderNumber { get; set; }
        public long? ShopId { get; set; }
        public string Shop { get; set; }
        public long? SiteID { get; set; }
        public string Site { get; set; }
        public bool? ReceiptSent { get; set; }
        public bool? OrderSentToSOP { get; set; }
        public Int16? OrderSOPStatus { get; set; } //enum
        public Int16? ProcessAttempts { get; set; }
        public bool? InvoiceSent { get; set; }
        public DateTime? InvoiceSentDate { get; set; }

        [NotMapped]
        public XElement Purchase_Xml 
        {
            get { return !string.IsNullOrWhiteSpace(PurchaseXml) ? XElement.Parse(PurchaseXml) : null; }
            set
            {
                PurchaseXml = value == null ? null : value.ToString(SaveOptions.DisableFormatting);
            }
        }

        [NotMapped]
        public XElement Checkout_Xml
        {
            get { return !string.IsNullOrWhiteSpace(CheckoutXml) ? XElement.Parse(CheckoutXml) : null; }
            set
            {
                CheckoutXml = value == null ? null : value.ToString(SaveOptions.DisableFormatting);
            }
        }

        [NotMapped]
        public XElement Payment_Xml //Mertex
        {
            get
            {
                return !string.IsNullOrWhiteSpace(PaymentXml)
                    ? ConvertMertexXmlStringToXmlObject(PaymentXml) 
                    : null;
            }
            set
            {
                PaymentXml = value == null ? null : value.ToString(SaveOptions.DisableFormatting);
            }
        }

        private XElement ConvertMertexXmlStringToXmlObject(string xmlString)
        {
            string formattedXml = FormatMertexXmlString(xmlString);

            return XElement.Parse(formattedXml);
        }

        private string FormatMertexXmlString(string xmlString)
        {
            //replace spurious double quotes
            string tidyXml = xmlString.Replace("\"&lt;", "<").Replace("&gt;\"", ">");

            //replace < and > character codes
            string tidyXml2 = tidyXml.Replace("&lt;", "<").Replace("&gt;", ">");

            //replace old xml format info and replace with new format info
            string tidyXml3 = 
                tidyXml2
                .Replace(GlobalContants.MertexXmlFormatLine, "")
                .Replace(GlobalContants.MertextHeader, GlobalContants.Xmlheader);

            return tidyXml3;
        }
    }
}