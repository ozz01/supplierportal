﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Supplier.Portal.DAL.WebEntities
{
    [Table("commerce_BasketLine")]
    public class BaskeLineEntity
    {
        [Key]
        [Required]
        public long BasketLineId { get; set; }

        [Required]
        public long BasketId { get; set; }

        [Required]
        public long SkuId { get; set; }

        [Required]
        public int Quantity { get; set; }

        //added property to table
        public bool? OutOfStock { get; set; }

        //added property to table
        public int? QtyAvailable { get; set; }

        public virtual BasketEntity Basket { get; set; }

        public virtual SkuEntity Sku {get; set; } 
    }
}