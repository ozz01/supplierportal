﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Supplier.Portal.Helpers;

namespace Supplier.Portal.DAL.WebEntities
{
    [Table("supplierPortal_OrderShipping")]
    public class OrderShippingEntity
    {
        public OrderShippingEntity()
        {
            ShipDate = DateTime.Now;
        }

        [Key]
        [Required]
        public int OrderShippingId { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime ShipDate { get; set; }

        [Required]
        [ForeignKey("OrderTransaction")]
        public long TransactionID { get; set; }  

        [Required]
        [MaxLength(50)]
        public string ShipmentId { get; set; }
   
        [Required]
        [MaxLength(50)]
        public string TrackingNumber { get; set; }

        [DecimalPrecision(10, 2)]
        public decimal TransportationCharges { get; set; }

        [DecimalPrecision(10, 2)]
        public decimal ServiceOptionCharges { get; set; }

        [DecimalPrecision(10, 2)]
        public decimal NegotiatedRateCharges { get; set; }

        [DecimalPrecision(10, 2)]       
        public decimal TotalCharges { get; set; }

        [Required]
        public string LabelData { get; set; }

        public string HtmlData { get; set; }

        [Required]
        public bool Cancelled { get; set; }

        [MaxLength(250)]
        public string CancellationReason { get; set; }

        public virtual TransactionEntity OrderTransaction { get; set; }
    }
}