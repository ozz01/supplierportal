﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Supplier.Portal.DAL.WebEntities
{
    [Table("commerce_Basket")]
    public class BasketEntity
    {            
        [Key]
        [Required]
        public long BasketId { get; set; }

        [Required]
        [ForeignKey("OrderTransaction")]
        public long? TransactionID { get; set; }
        
        public virtual ICollection<BaskeLineEntity> BasketItems { get; set; }

        public virtual TransactionEntity OrderTransaction { get; set; }
    }
}