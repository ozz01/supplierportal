﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Supplier.Portal.Helpers;

namespace Supplier.Portal.DAL.WebEntities
{
    [Table("commerce_Transaction")]
    public class TransactionEntity
    {
         [Key]
         [Required]
         public long TransactionID { get; set; }

         [Required]
         public long TransactionStatusID { get; set; }

         [Required]
         [Column("Created")]
         public DateTime OrderDate { get; set; }

         [Required]
         [MaxLength(50)]
         public string OrderNumber { get; set; }

         public int? UserBatchId { get; set; }

         [MaxLength(25)]
         public string DespatchStatus { get; set; }

         public string CheckoutXml { get; set; }

         public string PaymentXml { get; set; }

         public virtual ICollection<BasketEntity> Basket { get; set; }

         [NotMapped]
         public string FormattedPaymentXml
         {
            get { return FormatMertexXmlString(PaymentXml); }
         }

         private string FormatMertexXmlString(string xmlString)
         {
             //replace spurious double quotes
             string tidyXml = xmlString.Replace("\"&lt;", "<").Replace("&gt;\"", ">");

             //replace < and > character codes
             string tidyXml2 = tidyXml.Replace("&lt;", "<").Replace("&gt;", ">");

             //replace old xml format info and replace with new format info
             string tidyXml3 =
                 tidyXml2
                 .Replace(GlobalContants.MertexXmlFormatLine, "")
                 .Replace(GlobalContants.MertextHeader, GlobalContants.Xmlheader);

             //Tidy up Mertex field Names to sentence case
             string tidyXml4 = tidyXml3
                 .Replace("ORDERS", "Orders")
                 .Replace("ORDER", "Order")
                 .Replace("results", "Results")
                 .Replace("TYPE", "Type")
                 .Replace("NUMBER", "Number")
                 .Replace("BRAND", "Brand")
                 .Replace("ACCOUNT", "Account");

             return tidyXml4;
         }

         public virtual ICollection<OrderShippingEntity> ShippedOrders { get; set; } 
    }
}