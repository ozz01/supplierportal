﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Supplier.Portal.DAL.WebEntities
{
    [Table("commerce_Sku")]
    public class SkuEntity
    {
        [Key]
        [Required]
        public long SkuId { get; set; }

        [Required]
        [MaxLength(500)]
        public string Code { get; set; }

        [MaxLength(20)]
        public string EanCode { get; set; }

        [MaxLength(35)]
        public string SupplierProductCode { get; set; }

        [MaxLength(5)]
        public string UnitOfMeasurement { get; set; }
        
        [Required]
        [MaxLength(255)]
        public string Name { get; set; }

        public double? Weight { get; set; }

        public bool Deleted { get; set; }

        public virtual ICollection<SkuSupplierEntity> SkuSuppliers { get; set; } 
    }
}