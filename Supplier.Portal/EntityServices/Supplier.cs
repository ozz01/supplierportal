﻿using Supplier.Portal.DAL;
using Supplier.Portal.DAL.Entities;

namespace Supplier.Portal.EntityServices
{
    public class Supplier
    {
        public SupplierEntity GetSupplierForUser(int userId)
        {
            using (SupplierContext context = new SupplierContext())
            {
                int? supplierId = context.User.Find(userId).SupplierId;

                if (supplierId != null)
                {
                    var supplier = context.Supplier.Find(supplierId);

                    return supplier;
                }
            }

            return null;
        } 
    }
}