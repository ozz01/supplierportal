﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using Supplier.Portal.DAL;
using Supplier.Portal.DAL.Entities;
using Supplier.Portal.Helpers;
using Supplier.Portal.Services;
using Supplier.Portal.ViewModels.User;

namespace Supplier.Portal.EntityServices
{
    public class User
    {
        //public readonly List<string> UserRoles = new List<string>
        //{
        //    "Supplier Admin Processor",
        //    "Supplier Order Processor",
        //    "WG Tools Administrator",
        //    "WG Customer Support",
        //    "WG Supplier Support"
        //};
        
        public void Add(UserRegisterViewModel model)
        {
            using (SupplierContext context = new SupplierContext())
            {
                UserEntity user = new UserEntity
                {
                    Username = model.Username,
                    Email = model.Email,
                    HashedPassword = PasswordEncryption.Encrypt(model.Password),
                    Title = model.Title,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Phone_Mobile = model.Phone_Mobile,
                    Phone_Office = model.Phone_Office,
                    CompanyId = model.CompanyId,
                    RoleId = model.RoleId,
                };

                try
                {
                    context.User.Add(user);
                    context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);

                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        }
                    }

                    throw;
                }
            }
        }

        public bool Login(LoginViewModel model, ref LoggedInUserViewModel userModel)
        {
           
           using (SupplierContext context = new SupplierContext())
           {
                var user = context.User.FirstOrDefault(i => i.Username == model.Username);

                if (user != null)
                {
                    if (PasswordEncryption.CheckPassword(user.HashedPassword, model.Password))
                    {
                        EntityToViewModelMapper.MapToViewModel<UserEntity, LoggedInUserViewModel>(user, out userModel);
                        
                        return true;
                    }
                }
           
                return false;
            }
        }

        public List<LoggedInUserViewModel> All()
        {
            using (SupplierContext context = new SupplierContext())
            {
                List<UserEntity> users = context.User.ToList<UserEntity>();

                List<LoggedInUserViewModel> result = new List<LoggedInUserViewModel>();

                foreach (var user in users)
                {
                    var mappedOutput = new LoggedInUserViewModel();

                    bool mapped = EntityToViewModelMapper.MapToViewModel<UserEntity, LoggedInUserViewModel>(user, out mappedOutput);

                    if (mapped)
                    {
                        result.Add(mappedOutput);
                    }
                }

                return result;
            }
        }

        public List<UserEntity> GetUsersByCompanyandRoles(int companyId, List<string> roleNames)
        {
            SupplierContext context = new SupplierContext();

            return context.User
                .Where(i => i.CompanyId == companyId
                    && roleNames.Contains(i.Role.Name))
                .ToList();
        }

        public UserEntity GetUserById(int userId)
        {
            SupplierContext context = new SupplierContext();

            return context.User.Find(userId);
        }
    }
}
