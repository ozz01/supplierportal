﻿using System;
using System.Collections.Generic;
using System.Linq;
using Supplier.Portal.DAL;
using Supplier.Portal.DAL.Entities;
using Supplier.Portal.Helpers;
using Supplier.Portal.ViewModels;
using Supplier.Portal.ViewModels.Dashboad;

namespace Supplier.Portal.EntityServices
{
    public class FileImportLog
    {
        public FileUploadStatsViewModel Stats(DateTime datefilter)
        {
            FileUploadStatsViewModel stats = new FileUploadStatsViewModel
            {
                ProcessDate = datefilter
            };

            using (SupplierContext context = new SupplierContext())
            {
                var statsRecords = context.ImportLog.Where(i => i.ImportDate > stats.ProcessDate);

                stats.FilesProcessed = statsRecords.Count(i => i.ErrorDescription == null);

                stats.ErroneousFiles = statsRecords.Count(i => i.ErrorDescription != null);

                stats.SkusUpdated =
                   statsRecords.Any(i => i.ErrorDescription == null)
                   ? statsRecords.Where(i => i.ErrorDescription == null).Sum(x => x.SkusUpdated)
                   : 0;
            }

            return stats;
        }

        public FileUploadStatsViewModel CompanyStats(int userId, DateTime datefilter)
        {
            FileUploadStatsViewModel stats = new FileUploadStatsViewModel
            {
                ProcessDate = datefilter
            };

            using (SupplierContext context = new SupplierContext())
            {
                var user = context.User.FirstOrDefault(x => x.UserId == userId && x.CompanyId.HasValue);

                int companyId = user != null ? user.CompanyId.Value : 0;

                var statsRecords = context.ImportLog.Where(i => i.ImportDate > stats.ProcessDate && i.CompanyId == companyId);

                stats.FilesProcessed = statsRecords.Count(i => i.ErrorDescription == null);

                stats.ErroneousFiles = statsRecords.Count(i => i.ErrorDescription != null);

                stats.SkusUpdated =
                   statsRecords.Any(i => i.ErrorDescription == null)
                   ? statsRecords.Where(i => i.ErrorDescription == null).Sum(x => x.SkusUpdated)
                   : 0;
            }

            return stats;
        }

        public List<FileImportLogViewModel> SupplierLogs(int userId, DateTime? datefilter = null)
        {
            using (SupplierContext context = new SupplierContext())
            {
                var logs = new List<FileimportLogEntity> { };
                var user = context.User.FirstOrDefault(x => x.UserId == userId && x.CompanyId.HasValue);

                int companyId = user != null ? user.CompanyId.Value : 0;

                logs = context.ImportLog
                    .Where(i => i.CompanyId == companyId)
                    .ToList();

                if (datefilter != null)
                {
                    logs = logs
                      .Where(i => i.ImportDate >= datefilter)
                      .ToList();
                }
        

                List<FileImportLogViewModel> result2 = new List<FileImportLogViewModel>();

                foreach (var log in logs)
                {
                    var mappedOutput = new FileImportLogViewModel();

                    bool mapped = EntityToViewModelMapper.MapToViewModel<FileimportLogEntity, FileImportLogViewModel>(log, out mappedOutput);

                    if (mapped)
                    {
                        result2.Add(mappedOutput);
                    }
                }

                return result2;
            }
        }

        public List<FileImportLogViewModel> All(DateTime? datefilter = null)
        {
            using (SupplierContext context = new SupplierContext())
            {
                var logs = new List<FileimportLogEntity>{};

                if (datefilter.HasValue)
                {
                    logs = context.ImportLog.Where(i => i.ImportDate > datefilter).ToList();
                }
                else
                {
                    logs = context.ImportLog.ToList();                   
                }
                
                List<FileImportLogViewModel> result2 = new List<FileImportLogViewModel>();

                foreach (var log in logs)
                {
                    var mappedOutput = new FileImportLogViewModel();

                    bool mapped = EntityToViewModelMapper.MapToViewModel<FileimportLogEntity, FileImportLogViewModel>(log, out mappedOutput);

                    if (mapped)
                    {
                        result2.Add(mappedOutput);
                    }
                }

                return result2;
            }
        }
    }
}
