﻿using Supplier.Portal.DAL;

namespace Supplier.Portal.EntityServices
{
    public class Country
    {
        public string CountryName(long countryId)
        {
            string countryName;

            using (WebSupplierContext context = new WebSupplierContext())
            {
                var country = context.Country.Find(countryId);

                countryName = country != null ? country.Name : "";
            }

            return countryName;
        }

        public string CountryCode(long countryId)
        {
            string countryCode;

            using (WebSupplierContext context = new WebSupplierContext())
            {
                var country = context.Country.Find(countryId);

                countryCode = country != null ? country.ShortCountryCode: "";
            }

            return countryCode;
        } 
    }
}