﻿using System.Collections.Generic;
using System.Linq;
using Supplier.Portal.DAL;
using Supplier.Portal.DAL.Entities;
using Supplier.Portal.Helpers;
using Supplier.Portal.ViewModels.Company;

namespace Supplier.Portal.EntityServices
{
    public class Company
    {
        public List<CompanyViewModel> All()
        {
            using (SupplierContext context = new SupplierContext())
            {
                var companies = context.Company.ToList();

                List<CompanyViewModel> result = new List<CompanyViewModel>();

                foreach (var compamy in companies)
                {
                    var mappedOutput = new CompanyViewModel();

                    bool mapped = EntityToViewModelMapper.MapToViewModel<CompanyEntity, CompanyViewModel>(compamy, out mappedOutput);

                    if (mapped)
                    {
                        result.Add(mappedOutput);
                    }
                }

                return result;
            }
        }

        public List<string> GetSupplierCodesForUser(long userId)
        {
            List<string> companySupplierCodes = new List<string>();

            using (SupplierContext context = new SupplierContext())
            {
                CompanyEntity company = context.User
                    .Where(i => i.UserId == userId
                                && i.Company != null)
                    .Select(i => i.Company)
                    .FirstOrDefault();

                if (company != null)
                {
                    companySupplierCodes = company
                        .Suppliers
                        .Select(i => i.Code)
                        .ToList();
                }

                return companySupplierCodes;
            }
        }
    }
}