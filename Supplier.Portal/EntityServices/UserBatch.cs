﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Supplier.Portal.DAL;
using Supplier.Portal.DAL.Entities;
using Supplier.Portal.Helpers;

namespace Supplier.Portal.EntityServices
{
    public class UserBatch
    {
        public UserBatchEntity GetNewBatchCode(int userId)
        {          
            UserBatchEntity newRecord;

            try
            {
                //if user has existing batch increase the BatchNumber for that user
                using (SupplierContext context = new SupplierContext())
                {
                    int batchCount = 1;

                    if (context.UserBatch.Any(i => i.UserId == userId))
                    {
                        batchCount = context
                            .UserBatch
                            .Where(i => i.UserId == userId)
                            .Max(i => i.BatchNumber);

                        batchCount = batchCount + 1;
                    }

                    newRecord = new UserBatchEntity
                    {
                        UserId = userId,
                        BatchDate = DateTime.Today,
                        BatchNumber = batchCount,
                        BatchCompleted = false
                    };

                    context.UserBatch.Add(newRecord);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Could not create new user batch record");
            }

            return newRecord;
        }

        public List<UserBatchEntity> GetUserBatches()
        {
            List<UserBatchEntity> userBatches = new List<UserBatchEntity>();

            using (SupplierContext context = new SupplierContext())
            {
                userBatches = context.UserBatch.ToList();
            }

            return userBatches;
        } 

        public bool UpdateUserBatchStatus(int userBatchId, GlobalContants.DespatchStatus status)
        {
            try
            {
                using (SupplierContext context = new SupplierContext())
                {
                    UserBatchEntity userBatch = context.UserBatch.Find(userBatchId);

                    if (userBatch != null)
                    {
                        if (status == GlobalContants.DespatchStatus.Picked ||
                            status == GlobalContants.DespatchStatus.ReadyToShip)
                        {
                            userBatch.BatchCompleted = true;
                            userBatch.DatePicked = DateTime.Today;
                        }
                      
                        context.UserBatch.AddOrUpdate(userBatch);

                        context.SaveChanges();

                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                //throw new Exception("Could not update user batch record");
                return false;
            }

            return false;
        }

        public int GetCurrentUserBatchIdForUser(int userId)
        {
            int userBatchId;

            using (SupplierContext context = new SupplierContext())
            {
                var userBatch = context.UserBatch
                    .FirstOrDefault(i => i.UserId == userId
                        && i.BatchCompleted == false
                        && i.BatchDate == DateTime.Today);

                userBatchId = userBatch != null ? userBatch.UserBatchId : 0;


            }

            return userBatchId;
        }

        public int GetLastCompleteUserBatchIdForUser(int userId)
        {
            int userBatchId = 0 ;

            using (SupplierContext context = new SupplierContext())
            {
                var userBatch = context.UserBatch
                    .Where(i => i.UserId == userId && i.BatchCompleted)
                    .OrderByDescending(i => i.BatchNumber)
                    .Select(x => x.UserBatchId);

                if (userBatch.Any())
                {
                    userBatchId = userBatch != null ? userBatch.First() : 0;
                }
            }

            return userBatchId;
        }
    }
}