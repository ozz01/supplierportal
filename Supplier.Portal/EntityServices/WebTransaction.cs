﻿using System.Collections.Generic;
using System.Linq;
using Supplier.Portal.DAL;
using Supplier.Portal.DAL.WebEntities;
using Supplier.Portal.Helpers;
using Supplier.Portal.ViewModels;

namespace Supplier.Portal.EntityServices
{
    public class WebTransaction
    {
        public List<TransactionViewModel> FindByMertexReference(string reference)
        {
            string orderNoLokkup = string.Format("ORDER NUMBER=\"{0}\"", reference);

            List<WebTransactionDbObject> result = new List<WebTransactionDbObject>();

            string qry = "EXEC admin_GetTransactiobByMertexReference";

            using (WebContext context = new WebContext())
            {
                result = context.Database.SqlQuery<WebTransactionDbObject>(qry).ToList();
                result = result.Where(i => i.PaymentXml.Contains(orderNoLokkup)).ToList();
            }

            return MapResultsToViewModels(result);
        }

        public List<TransactionViewModel> FindByEmailAddress(string emailAddress)
        {
            List<WebTransactionDbObject> result = new List<WebTransactionDbObject>();

            string qry = "EXEC admin_GetTransactionByEmailAddress @email = '" + emailAddress + "'";

            using (WebContext context = new WebContext())
            {
                result = context.Database.SqlQuery<WebTransactionDbObject>(qry).ToList();
            }

            return MapResultsToViewModels(result);
        }

        public List<TransactionViewModel> FindByTransactionId(long transactionId)
        {
            List<WebTransactionDbObject> result = new List<WebTransactionDbObject>();

            string qry = "EXEC admin_GetTransactionByTransactionId @transactionId = " + transactionId;

            using (WebContext context = new WebContext())
            {
                result = context.Database.SqlQuery<WebTransactionDbObject>(qry).ToList();
            }

            return MapResultsToViewModels(result);
        }

        private List<TransactionViewModel> MapResultsToViewModels(List<WebTransactionDbObject> data)
        {
            List<TransactionViewModel> results = new List<TransactionViewModel>();

            foreach (WebTransactionDbObject dataRow in data)
            {
                TransactionViewModel mappedModel = new TransactionViewModel();

                EntityToViewModelMapper.MapToViewModel<WebTransactionDbObject, TransactionViewModel>(dataRow, out mappedModel);

                results.Add(mappedModel);
            }

            return results;
        }
    }
}