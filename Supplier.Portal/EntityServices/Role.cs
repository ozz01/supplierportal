﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using Supplier.Portal.DAL;
using Supplier.Portal.DAL.Entities;
using Supplier.Portal.Helpers;
using Supplier.Portal.ViewModels.Role;

namespace Supplier.Portal.EntityServices
{
    public class Role
    {
        public void Add(RoleViewModel model)
        {
            using (SupplierContext context = new SupplierContext())
            {
                RoleEntity role = new RoleEntity
                {
                    Name = model.Name
                };

                try
                {
                    context.Role.Add(role);
                    context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);

                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        }
                    }

                    throw;
                }
            }
        }

        public List<RoleViewModel> All()
        {
            using (SupplierContext context = new SupplierContext())
            {
                var roles = context.Role.ToList();

                List<RoleViewModel> result = new List<RoleViewModel>();

                foreach (var role in roles)
                {
                    var mappedOutput = new RoleViewModel();

                    bool mapped = EntityToViewModelMapper.MapToViewModel<RoleEntity, RoleViewModel>(role, out mappedOutput);

                    if (mapped)
                    {
                        result.Add(mappedOutput);
                    }
                }

                return result;
            }
        }      
    }
}