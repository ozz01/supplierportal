﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;
using Supplier.Portal.DAL;
using Supplier.Portal.DAL.Entities;
using Supplier.Portal.DAL.WebEntities;
using Supplier.Portal.Helpers;
using Supplier.Portal.Models;
using Supplier.Portal.Services;
using Supplier.Portal.Services.ExcelGeneration;
using Supplier.Portal.ViewModels;
using Supplier.Portal.ViewModels.Company;
using Supplier.Portal.ViewModels.Order;

namespace Supplier.Portal.EntityServices
{
    public class WebOrder
    {
        private Company _company;
        private UserBatch _userBatch;
        private Country _country;
        private Telephone _telephone;
        private Supplier _supplier;

        private string pendingStatus = GlobalContants.GetDespatchStatus(GlobalContants.DespatchStatus.Pending);
        private string beingPickedStatus = GlobalContants.GetDespatchStatus(GlobalContants.DespatchStatus.BeingPicked);
        private string pickedStatus = GlobalContants.GetDespatchStatus(GlobalContants.DespatchStatus.Picked);
        private string readyToShipStatus = GlobalContants.GetDespatchStatus(GlobalContants.DespatchStatus.ReadyToShip);

        public WebOrder()
        {
            _company = new Company();
            _userBatch = new UserBatch();
            _country = new Country();
            _telephone = new Telephone();
            _supplier = new Supplier();
        }

        public bool GetUserBatchForDisplay(int userId, ref List<OrderItemViewModel> results, out string message)
        {
            message = String.Empty;

            try
            {
                int userBatchId = _userBatch.GetCurrentUserBatchIdForUser(userId);
              
                using (WebSupplierContext context = new WebSupplierContext())
                {
                    results = context.BasketLine
                        .Where(i => i.Basket.OrderTransaction.TransactionStatusID == 4 //Important
                                    && i.Basket.OrderTransaction.DespatchStatus == beingPickedStatus
                                    && i.Basket.OrderTransaction.UserBatchId.HasValue
                                    && i.Basket.OrderTransaction.UserBatchId == userBatchId
                                    && i.Sku.Deleted == false)
                        .GroupBy(i => i.Sku.Code)
                        .OrderBy(i => i.Key)
                        .Select(i => new OrderItemViewModel
                        {
                            UserBatchId = userBatchId,
                            ProductCode = i.Key,
                            SupplierProductCode = i.FirstOrDefault().Sku.SupplierProductCode,
                            EanCode = i.FirstOrDefault().Sku.EanCode,
                            UnitOfMeasurement = i.FirstOrDefault().Sku.UnitOfMeasurement,
                            ProductName = i.FirstOrDefault().Sku.Name,
                            OrderQty = i.Sum(x => x.Quantity)
                        }).ToList();
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }

            return true;
        }

        public bool GetUserBatchForExport(int userId, ref List<ExcelPickingListDto> results, out string message)
        {
            message = String.Empty;

            try
            {
                //Cannot join to table in other context so have to create a list
                int userBatchId = _userBatch.GetCurrentUserBatchIdForUser(userId);

                using (WebSupplierContext context = new WebSupplierContext())
                {
                    results = context.BasketLine
                        .AsNoTracking()
                        .Where(i => i.Basket.OrderTransaction.TransactionStatusID == 4              //Important
                                    && i.Basket.OrderTransaction.DespatchStatus == beingPickedStatus
                                    && i.Basket.OrderTransaction.UserBatchId.HasValue
                                    && i.Basket.OrderTransaction.UserBatchId == userBatchId
                                    && i.Sku.Deleted == false)
                        .GroupBy(i => i.Sku.Code)
                        .OrderBy(i => i.Key)
                        .Select(i => new ExcelPickingListDto
                        {
                            WGProductCode = i.Key,
                            ProductCode = i.FirstOrDefault().Sku.SupplierProductCode,
                            EanCode = i.FirstOrDefault().Sku.EanCode,                            
                            ProductName = i.FirstOrDefault().Sku.Name,
                            OrderQty = i.Sum(x => x.Quantity),
                            StockQty = 0
                        }).ToList();
                }
            }

            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }

            return true;
        }

        public bool EmailUserBatch(int userId, string userEmailAddress, out string responseMessage)
        {
            responseMessage = String.Empty;
            List<OrderItemViewModel> pickingList = new List<OrderItemViewModel>();

            try
            {
                GetUserBatchForDisplay(userId, ref pickingList, out responseMessage);

                List<string> emailReceipients = new List<string> { userEmailAddress };

                string emailBody = HtmlEmailTemplate.GenerateFromList("PickingList", pickingList);

                EmailService.SendEmail(emailReceipients, GlobalContants.PickingListEmailSubject, true, emailBody);
            }
            catch (Exception ex)
            {
                responseMessage = ex.Message;
                return false;
            }

            return true;
        }

        public bool AssignNewUserBatch(int userId, out string message)
        {
            message = String.Empty;
          
            try
            {
                UserBatchEntity batchCode = _userBatch.GetNewBatchCode(userId);
                List<string> supplierCodes = _company.GetSupplierCodesForUser(userId);

                using (WebSupplierContext context = new WebSupplierContext())
                {
                    var initialList = context.BasketLine
                        .Where(i => i.Basket.OrderTransaction.TransactionStatusID == 4
                            && i.Basket.OrderTransaction.DespatchStatus == pendingStatus
                            && i.Sku.SkuSuppliers.Any(x => supplierCodes.Contains(x.SupplierCode)));

                    //Filter out orders where basket lines are for other suppliers to prevent split supplier orders
                    var orderRecords = initialList
                        .Select(i => i.Basket)
                        .Where(i => !i.BasketItems
                            .Any(y => y.Sku.SkuSuppliers
                                .Any(x => !supplierCodes.Contains(x.SupplierCode))))
                        .Distinct()
                        .Select(i => i.OrderTransaction);

                    foreach (TransactionEntity order in orderRecords)
                    {
                        order.UserBatchId = batchCode.UserBatchId;
                        order.DespatchStatus =
                            GlobalContants.GetDespatchStatus(GlobalContants.DespatchStatus.BeingPicked);

                        context.Order.AddOrUpdate(order);
                    }

                    context.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }
        }

        public bool AllocateStockToUserBatch(int userBatchId, int userId, List<OrderItemViewModel> pickingList, out string message)
        {
            message = String.Empty;
            var outOfstockList = pickingList.ToList();
            List<OrderViewModel> outOfStockOrders = new List<OrderViewModel>();
          
            try
            {
                using (WebSupplierContext context = new WebSupplierContext())
                {
                    List<BasketEntity> orders = context.Basket
                        .Where(i => i.OrderTransaction.TransactionStatusID == 4             //Important
                            && i.OrderTransaction.DespatchStatus == beingPickedStatus         
                            && i.OrderTransaction.UserBatchId.HasValue
                            && i.OrderTransaction.UserBatchId == userBatchId)
                        .OrderBy(i => i.OrderTransaction.OrderDate)
                        .ToList();

                    foreach (BasketEntity basket in orders)
                    {
                        List<BaskeLineEntity> basketItems = basket.BasketItems.ToList();
                        List<OrderItemViewModel> orderItemsList = new List<OrderItemViewModel>();
                        bool _orderOfStock = false;

                        foreach (BaskeLineEntity basketItem in basketItems)
                        {
                            var outOfStockItem = outOfstockList.FirstOrDefault(i => i.ProductCode == basketItem.Sku.Code && i.OutOfStock);

                            if (outOfStockItem != null)
                            {
                                //Stock Qty >= to Order Qty so reduce Sku Qty in Picking List but do NOT Mark order item as OOS
                                if (outOfStockItem.QtyAvailable >= basketItem.Quantity)
                                {
                                    outOfStockItem.QtyAvailable -= basketItem.Quantity;                                    
                                }
                                //Mark order item as OOS
                                else
                                {
                                    _orderOfStock = true;
                                    basketItem.OutOfStock = true;
                                    basketItem.QtyAvailable = outOfStockItem.QtyAvailable;

                                    context.BasketLine.AddOrUpdate(basketItem);
                                }
                            }

                            OrderItemViewModel orderItemModel = new OrderItemViewModel();

                            bool mapped = EntityToViewModelMapper.MapToViewModel<BaskeLineEntity, OrderItemViewModel>(basketItem, out orderItemModel);

                            if (mapped)
                            {
                                orderItemModel.OutOfStock = basketItem.OutOfStock != null && basketItem.OutOfStock.Value;
                                orderItemModel.QtyAvailable = (basketItem.OutOfStock == null ||basketItem.OutOfStock == false)
                                    ? basketItem.Quantity
                                    : basketItem.QtyAvailable != null
                                        ? basketItem.QtyAvailable.Value
                                        : 0;

                                orderItemsList.Add(orderItemModel);
                            }
                        }

                        //Add to Order List for OOS email task
                        if (_orderOfStock)
                        {
                            OrderViewModel orderModel = new OrderViewModel();

                            orderModel.OrderNumber = basket.OrderTransaction.OrderNumber;
                            orderModel.OrderDate = basket.OrderTransaction.OrderDate;
                            orderModel.OrderItems = orderItemsList;

                            //Pupulate Customer Addresses and contact details
                            if (!string.IsNullOrEmpty(basket.OrderTransaction.CheckoutXml))
                            {
                                orderModel.InvoiceCustomer = PopluateAddress(basket.OrderTransaction, GlobalContants.AddrerssType.Billing);
                            }

                            outOfStockOrders.Add(orderModel);
                        }

                        //Set Despatch status according to orders being out of stock or not
                        basket.OrderTransaction.DespatchStatus = _orderOfStock ? pickedStatus : readyToShipStatus;
                    }

                    //Update UserBatch Record
                    bool userBatchUpdated = false;

                    if (outOfStockOrders.Any())
                    {
                        userBatchUpdated = _userBatch.UpdateUserBatchStatus(userBatchId, GlobalContants.DespatchStatus.Picked);
                    
                        SendOutOfStockEmail(userId, outOfStockOrders);
                    }
                    else
                    {
                        userBatchUpdated = _userBatch.UpdateUserBatchStatus(userBatchId, GlobalContants.DespatchStatus.ReadyToShip);
                    }

                    //Update Order records if UserBatch record updaed successfully
                    if (userBatchUpdated) { context.SaveChanges(); }
                }

                return true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }
        }

        public bool GetOutOfStockOrders(int userId, List<OrderViewModel> outOfStockOrders, out string message)
        {
            message = String.Empty;
           
            try
            {                
                List<string> supplierCodes = _company.GetSupplierCodesForUser(userId);

                using (WebSupplierContext context = new WebSupplierContext())
                {
                    var outOfStockOrdersQuery = context.BasketLine
                        .Where(i => i.Basket.OrderTransaction.TransactionStatusID == 4                          //Important
                                    && i.Basket.OrderTransaction.DespatchStatus == pickedStatus
                                    && i.OutOfStock.HasValue && i.OutOfStock == true
                                    && i.Sku.SkuSuppliers.Any(x => supplierCodes.Contains(x.SupplierCode)));

                    List<BasketEntity> orders = outOfStockOrdersQuery
                        .Select(x => x.Basket)
                        .Distinct()
                        .ToList();


                    foreach (BasketEntity basket in orders)
                    {
                        OrderViewModel orderModel = new OrderViewModel();

                        bool mapped = EntityToViewModelMapper.MapToViewModel<BasketEntity, OrderViewModel>(basket, out orderModel);

                        if (mapped)
                        {
                            //Pupulate Customer Addresses and contact details
                            if (!string.IsNullOrEmpty(basket.OrderTransaction.CheckoutXml))
                            {
                                orderModel.InvoiceCustomer = PopluateAddress(basket.OrderTransaction, GlobalContants.AddrerssType.Billing);
                            }

                            outOfStockOrders.Add(orderModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }

            return true;
        }

        //todo: Pull in all "ReadyToShip" oders for supplier 
        public bool GetOrdersForShipping(int userId, ref List<ShippingOrderViewModel> ordersForShipping, out string message)
        {
            message = String.Empty;

            try
            {             
                int userBatchId = _userBatch.GetLastCompleteUserBatchIdForUser(userId);

                using (WebSupplierContext context = new WebSupplierContext())
                {
                    var readyTohipOrders = context.Basket
                        .Where(i => i.OrderTransaction.TransactionStatusID == 4                     //Important
                                    && i.OrderTransaction.DespatchStatus == readyToShipStatus
                                    && i.OrderTransaction.UserBatchId == userBatchId                
                                    && (!i.BasketItems
                                        .Any(x => x.OutOfStock.HasValue)));

                    foreach (BasketEntity basket in readyTohipOrders)
                    {
                        ShippingOrderViewModel shippingOrderModel = new ShippingOrderViewModel();

                        bool mapped = EntityToViewModelMapper.MapToViewModel<BasketEntity, ShippingOrderViewModel>(basket, out shippingOrderModel);

                        if (mapped)
                        {
                            //Pupulate Customer Addresses and contact details
                            if (!string.IsNullOrEmpty(basket.OrderTransaction.CheckoutXml))
                            {
                                shippingOrderModel.DeliveryAddress = PopluateAddress(basket.OrderTransaction, GlobalContants.AddrerssType.Delivery);

                                shippingOrderModel.Supplier = PopluateSupplierAddress(userId);
                            }

                            var totalWeight = shippingOrderModel.OrderItems.Sum(x => x.Weight);
                            shippingOrderModel.PackageWeight = (decimal) Math.Round(totalWeight, 2);

                            ordersForShipping.Add(shippingOrderModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }

            return true;
        }

        public bool GetOrderForShipping(int transactionId, int userId, ref ShippingOrderViewModel orderModel, out string message)
        {
            message = String.Empty;

            try
            {
                using (WebSupplierContext context = new WebSupplierContext())
                {
                    var readyTohipOrder = context.Basket
                        .FirstOrDefault(i => i.OrderTransaction.TransactionID == transactionId);

                    if (readyTohipOrder != null)
                    {
                        bool mapped = EntityToViewModelMapper.MapToViewModel<BasketEntity, ShippingOrderViewModel>(readyTohipOrder, out orderModel);

                        if (mapped)
                        {
                            //Pupulate Customer Addresses and contact details
                            if (!string.IsNullOrEmpty(readyTohipOrder.OrderTransaction.CheckoutXml))
                            {
                                orderModel.DeliveryAddress = PopluateAddress(readyTohipOrder.OrderTransaction, GlobalContants.AddrerssType.Delivery);

                                orderModel.Supplier = PopluateSupplierAddress(userId);
                            }

                            var totalWeight = orderModel.OrderItems.Sum(x => x.Weight);
                            orderModel.PackageWeight = (decimal)Math.Round(totalWeight, 2);

                            List<string> trackingNumbers = readyTohipOrder.OrderTransaction.ShippedOrders
                                .Where(x => x.Cancelled == false)
                                .Select(i => i.TrackingNumber)
                                .ToList();

                            orderModel.TrackingNumbers = trackingNumbers;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }

            return true;
        }

        public bool ResetOrderStatus(int trnasactionId, out string message)
        {
            message = String.Empty;

            try
            {
                using (WebSupplierContext _webSupplierContext = new WebSupplierContext())
                {

                    TransactionEntity order = _webSupplierContext.Order.Find(trnasactionId);

                    if (order != null)
                    {
                        order.DespatchStatus = GlobalContants.GetDespatchStatus(GlobalContants.DespatchStatus.Pending);
                        order.UserBatchId = null;

                        _webSupplierContext.SaveChanges();
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return false;
        }

        /// Private Methods ///
              
        private void SendOutOfStockEmail(int userId, List<OrderViewModel> oosOrders)
        {
            User _user = new User();

            UserEntity userEntity = _user.GetUserById(userId);

            if (userEntity != null && userEntity.CompanyId.HasValue)
            {
                int companyid = userEntity.CompanyId.Value;

                List<string> supplierAdminReceipient = _user.GetUsersByCompanyandRoles(companyid, new List<string> { "Supplier Admin Processor" })
                    .Select(x => x.Email)
                    .ToList();

                List<string> supplierSupportReceipient = _user.GetUsersByCompanyandRoles(companyid, new List<string> { "WG Supplier Support" })
                    .Select(x => x.Email)
                    .ToList();

                try
                {
                    //Use External Template - No Customer Address
                    string emailBody = HtmlEmailTemplate.GenerateFromList("OutOfStockOrders", oosOrders);
                    EmailService.SendEmail(supplierAdminReceipient, GlobalContants.OOSEmailSubject, true, emailBody);

                    //Use External Template with Customer Address
                    emailBody = HtmlEmailTemplate.GenerateFromList("OutOfStockOrdersExternal", oosOrders);
                    EmailService.SendEmail(supplierSupportReceipient, GlobalContants.OOSEmailSubject, true, emailBody);

                }
                catch (Exception ex)
                {

                }
            }
        }

        private AddressViewModel PopluateAddress(TransactionEntity orderTransaction, GlobalContants.AddrerssType addressType)
        {
            //long countryId;
            long primaryTelephoneId;

            AddressViewModel _model = new AddressViewModel();

            //Get the Checkout Data from the transaction record
            CheckoutType checkoutData = XmlConvertor.GetCheckoutDataFromXml(orderTransaction.CheckoutXml);
            PersonType person = checkoutData.Person;

            //Get the Billing Adddress
            var address = checkoutData.Addresses
               .Where(x => x.key.AddressType == addressType.ToString())
               .Select(x => x.value.Address)
               .FirstOrDefault();

            if (address != null)
            {
                _model.Title = !string.IsNullOrEmpty(address.Title) ? CultureInfo.CurrentCulture.TextInfo.ToTitleCase(address.Title) : "";
                _model.FirstName = !string.IsNullOrEmpty(address.FirstName) ? CultureInfo.CurrentCulture.TextInfo.ToTitleCase(address.FirstName) : "";
                _model.LastName = !string.IsNullOrEmpty(address.LastName) ? CultureInfo.CurrentCulture.TextInfo.ToTitleCase(address.LastName): "";
                _model.AddressLine1 = !string.IsNullOrEmpty(address.Line1) ? CultureInfo.CurrentCulture.TextInfo.ToTitleCase(address.Line1) : "";
                _model.AddressLine2 = !string.IsNullOrEmpty(address.Line2) ? CultureInfo.CurrentCulture.TextInfo.ToTitleCase(address.Line2): "";
                _model.Town = !string.IsNullOrEmpty(address.Town) ? CultureInfo.CurrentCulture.TextInfo.ToTitleCase(address.Town) : "";
                _model.County = !string.IsNullOrEmpty(address.County) ? CultureInfo.CurrentCulture.TextInfo.ToTitleCase(address.County) : "";
                _model.Postcode = !string.IsNullOrEmpty(address.Postcode) ? address.Postcode.ToUpper() : "";
                _model.CountryId = address.CountryId;
                _model.Country = _country.CountryName(address.CountryId);
                _model.CountryCode = _country.CountryCode(address.CountryId);
                _model.Email = !string.IsNullOrEmpty(person.EmailAddress) ? person.EmailAddress : "";


                if (!string.IsNullOrEmpty(person.PrimaryTelephoneId) && Int64.TryParse(person.PrimaryTelephoneId, out primaryTelephoneId))
                {
                    _model.Telephone = _telephone.TelephoneNumber(primaryTelephoneId);
                }
            }

            return _model;
        }

        private SupplierViewModel PopluateSupplierAddress(int userId)
        {
            SupplierViewModel _model = new SupplierViewModel();

            SupplierEntity supplier = _supplier.GetSupplierForUser(userId);

            bool mapped = EntityToViewModelMapper.MapToViewModel<SupplierEntity, SupplierViewModel>(supplier, out _model);
    
            return _model;
        }
    }
}