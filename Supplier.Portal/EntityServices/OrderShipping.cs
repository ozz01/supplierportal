﻿using System;
using System.Collections.Generic;
using System.Linq;
using Supplier.Portal.DAL;
using Supplier.Portal.DAL.WebEntities;
using Supplier.Portal.Helpers;
using Supplier.Portal.Services;
using Supplier.Portal.ViewModels.Order;
using Supplier.UpsModels;
using Supplier.UpsModels.Common;
using Supplier.UpsModels.HttpRequest;
using Supplier.UpsModels.HttpResponse;

namespace Supplier.Portal.EntityServices
{
    /// <summary>
    /// Need to use correct:
    /// 1. Ship to country code (E.g. "GB")
    /// 2. Ship to state province code
    /// 3. Shipper country code (E.g. "GB")
    /// 4. Postal Codes (5-7 chars No Spaces) 
    /// 5. Service -> Code ("11" for "Standard")
    /// 6. Service -> Description ("Standard" for 11)
    /// 7. UnitOfMeasurement -> Code ("CM" for Centimetres)
    /// 8. UnitOfMeasurement -> Description ("Centimetres" for "CM")
    /// 9. PackageWeight -> UnitOfMeasurement -> Code ("KGS" for "Kilograms" )
    /// 10. PackageWeight -> UnitOfMeasurement -> Description  ("Kilograms" for "KGS")
    /// 11. Package -> Packaging -> Code (02 for "Box") ??? Need to check this
    /// 12. Package Code needs to match Package Dimensions
    /// 13. PaymentInformation -> ShipmentCharge -> Type
    /// 14. 13. PaymentInformation -> ShipmentCharge -> BillShipper -> AccountNumber
    
    /// Need To Store response values for te following in DB
    /// 1. TransactionReference
    
    /// May need to obtain following details from client UPS Account:
    /// 1. ShipperNumber (Account Number)
    /// 2. Account Number
    /// </summary>
    public class OrderShipping
    {
        private Ups _ups;
        private UserBatch _UserBatch;

        public OrderShipping()
        {
            _ups = new Ups();
            _UserBatch = new UserBatch();
        }

        /*** Public Methods ***/

        public bool ShipOrder(ShippingOrderViewModel order, out string message)
        {
            message = String.Empty;
            int labelsCounter = 1;
        
            List<string> trackingNumbers = new List<string>();
            List<ShippingResponseContainer> dataToSave = new List<ShippingResponseContainer>();

            //Set UPS Request Credetials in Request Object
            UpsRequest.Shipping _shippingData = new UpsRequest.Shipping();
            SetCredentials(_shippingData.UPSSecurity);

            //SET Request Options
            SetRequestOptions(_shippingData.ShipmentRequest.Request);
            
            //SET Shiping Info
            SetShippingData(order, _shippingData.ShipmentRequest.Shipment);

            //SET Label Type
            SetShippingLabelData(_shippingData.ShipmentRequest, UpsConstants.GetLabelFormatCode(UpsConstants.LabelFormat.ZPL));

            //Process Request(s)

            while (labelsCounter <= order.NumberOfLabels + 1)
            {
                _shippingData.ShipmentRequest.Shipment.Package.Description = string.Format("Package {0} of {1}", labelsCounter, order.NumberOfLabels);

                ShippingResponseContainer _requestResult = new ShippingResponseContainer();
                bool upsResult = _ups.SubmitShipOrderRequest(_shippingData, _requestResult);

                if (upsResult == false)
                {
                    message = _requestResult.RequestError.Description;

                    return false;
                }

                trackingNumbers.Add(_requestResult.ShipmentData.PackageResults.TrackingNumber);
                order.ShipmentId = _requestResult.ShipmentData.ShipmentIdentificationNumber;

                dataToSave.Add(_requestResult);

                labelsCounter++;
            }

            //Save response data to DB and update status
            bool dbaction = SaveShippingData(dataToSave, order);

            if (!dbaction)
            {
                message = "Unable to save shipping data";

                return false;
            }

            order.TrackingNumbers = trackingNumbers;
            order.Shipped = true;

            return true;
        }

        //todo: test in live as test environment has not shipments to cancel
        public bool CancelShipment(string trackingNumber, string reason, ShippingOrderViewModel order, out string message)
        {
            message = string.Empty;
            VoidResponseContainer _requestResult = new VoidResponseContainer();
            UpsRequest.VoidShipping _voidShippingData = new UpsRequest.VoidShipping();

            //Set UPS Request Credetials in Request Object
            SetCredentials(_voidShippingData.UPSSecurity);

            //_voidShippingData.VoidShipmentRequest.Request.TransactionReference.CustomerContext = transactionReference;
            _voidShippingData.VoidShipmentRequest.VoidShipment.ShipmentIdentificationNumber = order.ShipmentId;


            //Process Request
            bool upsResult = _ups.SubmitVoidShipmentRequest(_voidShippingData, _requestResult);

            //todo: remove
            upsResult = true;

            if (upsResult)
            {               
                CancelOrderShippingRecord(trackingNumber, reason);
            }
            else
            {
                message = _requestResult.RequestError.Description;
            }

            return upsResult;
        }

        //todo: hook up to controller and find out about getting pdf download for label
        public bool RecoverLabelData(string trackingNumber, out LabelResponseContainer _requestResult)
        {
            _requestResult = new LabelResponseContainer();

            UpsRequest.LabelRecovery _labelRecoveryData = new UpsRequest.LabelRecovery();

            //Set UPS Request Credetials in Request Object
            SetCredentials(_labelRecoveryData.UPSSecurity);


            //todo - get this data from order data using Order Number
            //_labelRecoveryData.LabelRecoveryRequest.TrackingNumber = trackingNumber;
            _labelRecoveryData.LabelRecoveryRequest.TrackingNumber = UpsConstants.TestTrackingNumber_Pdf;

            //_labelRecoveryData.LabelRecoveryRequest.LabelSpecification.HTTPUserAgent = "";

            //Process Request
            bool upsResult = _ups.SubmitLabelRequest(_labelRecoveryData, _requestResult);

            return upsResult;
        }


        //todo: Finish implementation -> Process Images
        public bool RetrieveShippingDocumentData(int transactionId, out FileDataModel model)
        {
            model = new FileDataModel();

            try
            {
                model = new FileDataModel();

                //todo get despatch note
                model.DespatchNoteData = "";

                using (WebSupplierContext _webContext = new WebSupplierContext())
                {
                    List<OrderShippingEntity> orderShippingRecords = _webContext.OrderShipping
                        .Where(x => x.TransactionID == transactionId)
                        .ToList();

                    foreach (var orderShippingRecord in orderShippingRecords)
                    {
                        model.LabelData.Add(orderShippingRecord.LabelData);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
            }

            return false;
        }


        /*** Private Methods ***/
        
        //todo: Get account details and license key for ups request from DB / Settings
        private void SetCredentials(Shared.UpsSecurity securityData)
        {
            securityData.UsernameToken.Username = UpsConstants.UserName;
            securityData.UsernameToken.Password = UpsConstants.Password;
            securityData.ServiceAccessToken.AccessLicenseNumber = UpsConstants.AccessLicenseNumber;
        }

        private void SetRequestOptions(UpsRequest.Request requestData)
        {
            requestData.RequestOption = UpsConstants.GetShippingRequestType(UpsConstants.RequestValidationOption.Validated);
            requestData.TransactionReference.CustomerContext = "TDL Shipping";                  //todo: What should this be ? We can specify whatever we want
        }
        
        //todo - Get ShippingData from DB
        private void SetShippingData(ShippingOrderViewModel order, UpsRequest.Shipment shippmentData)
        {
            shippmentData.Shipper.ShipperNumber = order.Supplier.UpsAccountNumber;
            shippmentData.Shipper.Name = order.Supplier.Name;
            shippmentData.Shipper.FaxNumber = order.Supplier.Fax;
            shippmentData.Shipper.Phone.Number = order.Supplier.TelePhone;
            shippmentData.Shipper.Phone.Extension = order.Supplier.Extension;
            shippmentData.Shipper.Address.AddressLine = string.Format("{0},{1}.{2}", order.Supplier.AddressLine1, order.Supplier.AddressLine2, order.Supplier.Town);
            shippmentData.Shipper.Address.City = order.Supplier.County;
            shippmentData.Shipper.Address.StateProvinceCode = "";
            shippmentData.Shipper.Address.PostalCode = order.Supplier.Postcode.Replace(" ", "");           //reomove spaces in middle
            shippmentData.Shipper.Address.CountryCode = order.Supplier.CountryCode;                       


            //Pickup Address data
            shippmentData.ShipFrom.Name = order.Supplier.Name;
            shippmentData.ShipFrom.AttentionName = order.Supplier.ShipperAttnName;
            shippmentData.ShipFrom.FaxNumber = order.Supplier.Fax;
            shippmentData.ShipFrom.Phone.Number = order.Supplier.TelePhone;
            shippmentData.ShipFrom.Phone.Extension = order.Supplier.Extension;
            shippmentData.ShipFrom.Address.AddressLine = string.Format("{0},{1}.{2}", order.Supplier.AddressLine1, order.Supplier.AddressLine2, order.Supplier.Town);
            shippmentData.ShipFrom.Address.City = order.Supplier.County;
            shippmentData.ShipFrom.Address.PostalCode = order.Supplier.Postcode.Replace(" ", "");           //reomove spaces in middle
            shippmentData.ShipFrom.Address.CountryCode = order.Supplier.CountryCode;                        

            
            //Customer Address data
            shippmentData.ShipTo.Name =  string.Format("{0}{1} {2}",
                !string.IsNullOrEmpty(order.DeliveryAddress.Title) ? order.DeliveryAddress.Title + ". " :"",  
                order.DeliveryAddress.FirstName, 
                order.DeliveryAddress.LastName);
            
            shippmentData.ShipTo.Address.AddressLine = string.Format("{0},{1}.{2}",order.DeliveryAddress.AddressLine1, order.DeliveryAddress.AddressLine2, order.DeliveryAddress.Town);
            shippmentData.ShipTo.Address.City = order.DeliveryAddress.County;
            shippmentData.ShipTo.Address.PostalCode = order.DeliveryAddress.Postcode.Replace(" ", "");
            shippmentData.ShipTo.Address.CountryCode = order.DeliveryAddress.CountryCode;


            //todo: Payment Data
            shippmentData.PaymentInformation.ShipmentCharge.Type = "01";                            //todo: find out what options there are for this
            shippmentData.PaymentInformation.ShipmentCharge.BillShipper.AccountNumber = order.Supplier.UpsAccountNumber;

            //todo: Service Type
            shippmentData.Service.Code = "11";                                                      //todo: Confirm Service to be used. 11 = Standard
           
            //todo: RateOptions
            shippmentData.ShipmentRatingOptions.NegotiatedRatesIndicator = "1";                     //todo: Confirm

            //todo: Package Data
            shippmentData.Package.Packaging.Code = "02";                                            //todo: Confirm Packaging to be used
            shippmentData.Package.Dimensions.UnitOfMeasurement.Code = "CM";                         //todo: Get from settings?
            shippmentData.Package.Dimensions.Length = "25";                                         //todo: Get from settings?
            shippmentData.Package.Dimensions.Width = "15";                                          //todo: Get from settings?
            shippmentData.Package.Dimensions.Height = "3";                                          //todo: Get from settings?
            shippmentData.Package.PackageWeight.UnitOfMeasurement.Code = UpsConstants.WeightCode;
            shippmentData.Package.PackageWeight.Weight = UpsConstants.PackageWeight;


            /*** Not Needed ***/
            //shippmentData.Shipper.TaxIdentificationNumber = "";                                   
            //shippmentData.ShipFrom.Address.StateProvinceCode = "";
            //shippmentData.ShipTo.AttentionName = "";                                                            
            //shippmentData.ShipTo.Phone.Extension = "";
            //shippmentData.ShipTo.Address.StateProvinceCode = "";
            //shippmentData.ShipTo.Phone.Number = !string.IsNullOrEmpty(order.DeliveryAddress.Telephone) ? order.DeliveryAddress.Telephone :"";
            //shippmentData.Package.Description = "";                                              
        }

        private void SetShippingLabelData(UpsRequest.ShipmentRequest shipmentRequestTypeData, string labelFormatCode)
        {
            shipmentRequestTypeData.LabelSpecification.HTTPUserAgent = "";                   

            //todo: Confirm Label Type to use 
            if (!string.IsNullOrEmpty(labelFormatCode))
            {
                shipmentRequestTypeData.LabelSpecification.LabelImageFormat.Code = labelFormatCode;

                if (labelFormatCode == "ZPL" || labelFormatCode == "EPL")
                {
                    shipmentRequestTypeData.LabelSpecification.LabelStockSize.Height = UpsConstants.LabelHeight;
                    shipmentRequestTypeData.LabelSpecification.LabelStockSize.Width = UpsConstants.LabelWidth;
                }
            }
        }

        private bool SaveShippingData(List<ShippingResponseContainer> upsResponses, ShippingOrderViewModel order)
        {
            try
            {
                using (WebSupplierContext webContext = new WebSupplierContext())
                {
                    var orderToUpdate = webContext.Order.Find(order.TransactionId);
                    
                    if (orderToUpdate != null)
                    {                                             
                        //Add OrderShipping Record(s)
                        foreach (ShippingResponseContainer upsResponseData in upsResponses)
                        {
                            OrderShippingEntity orderShiping = new OrderShippingEntity();

                            //orderShiping.Cancelled = false;
                            orderShiping.ShipDate = DateTime.Now;
                            orderShiping.ShipmentId = upsResponseData.ShipmentData.ShipmentIdentificationNumber;
                            orderShiping.TransactionID = order.TransactionId;
                            orderShiping.TrackingNumber = upsResponseData.ShipmentData.PackageResults.TrackingNumber;

                            //Charges
                            orderShiping.TransportationCharges = upsResponseData.ShipmentData.ShipmentCharges.TransportationCharges.MonetaryValue;
                            orderShiping.ServiceOptionCharges = upsResponseData.ShipmentData.ShipmentCharges.ServiceOptionsCharges.MonetaryValue;
                            orderShiping.NegotiatedRateCharges = upsResponseData.ShipmentData.NegotiatedRateCharges.TotalCharge.MonetaryValue;
                            orderShiping.TotalCharges = upsResponseData.ShipmentData.ShipmentCharges.TotalCharges.MonetaryValue;

                            orderShiping.LabelData = upsResponseData.ShipmentData.PackageResults.ShippingLabel.GraphicImage;
                            //orderShiping.HtmlData = upsResponseData.ShipmentData.PackageResults.ShippingLabel.HTMLImage;

                            webContext.OrderShipping.Add(orderShiping);
                        }

                        //Update Status on Transaction Table
                        orderToUpdate.DespatchStatus = GlobalContants.GetDespatchStatus(GlobalContants.DespatchStatus.Shipped);

                        webContext.SaveChanges();
                    }                  
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        private void CancelOrderShippingRecord(string trackingNumber, string reason)
        {
            try
            {
                using (WebSupplierContext webContext = new WebSupplierContext())
                {
                    OrderShippingEntity orderShippingRecord = webContext.OrderShipping
                        .FirstOrDefault(x => x.TrackingNumber == trackingNumber);

                    if (orderShippingRecord != null)
                    {
                        orderShippingRecord.Cancelled = true;
                        orderShippingRecord.CancellationReason = reason;

                        //If there are no un-cancelled records re-set the status on the order
                        if (!orderShippingRecord.OrderTransaction.ShippedOrders.Any(x => x.Cancelled == false))
                        {
                            orderShippingRecord.OrderTransaction.DespatchStatus =
                                GlobalContants.GetDespatchStatus(GlobalContants.DespatchStatus.ReadyToShip);
                        }

                        webContext.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                //var error = ex.Message;
            }            
        }
    }
}