﻿using Supplier.Portal.DAL;

namespace Supplier.Portal.EntityServices
{
    public class Telephone
    {
        public string TelephoneNumber(long telephoneId)
        {
            string telephoneNumber;

            using (WebSupplierContext context = new WebSupplierContext())
            {
                var telephone = context.Telephone.Find(telephoneId);

                telephoneNumber = telephone != null ? telephone.Number : "";
            }

            return telephoneNumber;
        } 
    }
}