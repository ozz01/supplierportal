﻿using System.Collections.Generic;

namespace Supplier.Portal.Helpers
{
    public class GlobalContants
    {
        public const string DoNotTeplyEmail = "donotreply@WalkergreenBank.co.uk";
        
        public const string PickingListEmailSubject = "Walker Greenbank Orders - Picking List";

        public const string OOSEmailSubject = "Walker Greenbank Orders - Out of Stock Items Notification";

        public const string EmailGenerationError = "There was a problem generating the email content";

        //Metex Reponse Formmattors
        public const string MertexXmlFormatLine = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        public const string MertextHeader = "<paymentXml>";
        public const string Xmlheader = "<paymentXml xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">";

        //Print Server Application
        public const string PrintServerLink = "http://wgb2013/ClickOnce/Supplier.PrinterServer.application?userId=8&fileFormat=EXCEL";
        public const string PrinterServerUserIdParam = "?userId=";
        public const string PrinterServerFileTypeParam = "&fileType=";
        public const string PrinterServerFileRecordIdParam = "&recordId=";

        public enum AddrerssType{
            Billing,
            Delivery            
        }

        public enum DespatchStatus
        {
            Pending,
            BeingPicked,
            Picked,
            ReadyToShip,
            Shipped
        }

        //** Label Formats **//
        private static readonly Dictionary<DespatchStatus, string> _despatchStatus = new Dictionary<DespatchStatus, string>
        {
            {DespatchStatus.Pending, ""},
            {DespatchStatus.BeingPicked, "Being Picked"},
            {DespatchStatus.Picked, "Picked, OOS Items"},
            {DespatchStatus.ReadyToShip, "Picked, Ready to ship"},
            {DespatchStatus.Shipped, "Shipped"},
        };

        public static string GetDespatchStatus(DespatchStatus key)
        {
            string value;
            _despatchStatus.TryGetValue(key, out value);

            return value;
        }
    }
}