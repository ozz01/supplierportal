﻿using System.Collections.Generic;
using Supplier.Portal.DAL.Entities;
using Supplier.Portal.DAL.WebEntities;
using Supplier.Portal.ViewModels;
using Supplier.Portal.ViewModels.Company;
using Supplier.Portal.ViewModels.Order;
using Supplier.Portal.ViewModels.Role;
using Supplier.Portal.ViewModels.User;

namespace Supplier.Portal.Helpers
{
    public static class EntityToViewModelMapper
    {
        public static bool MapToViewModel<T, U>(T input, out U outVM) where U : class
        {
            outVM = default(U);

            if (typeof(T) == typeof(UserEntity) && typeof(U) == typeof(LoggedInUserViewModel))
            {
                outVM = MapUserToViewModel(input as UserEntity) as U;

                return true;
            }

            if (typeof(T) == typeof(RoleEntity) && typeof(U) == typeof(RoleViewModel))
            {
                outVM = MapRoleToViewModel(input as RoleEntity) as U;

                return true;
            }

            if (typeof(T) == typeof(CompanyEntity) && typeof(U) == typeof(CompanyViewModel))
            {
                outVM = MapCompanyToViewModel(input as CompanyEntity) as U;

                return true;
            }

            if (typeof(T) == typeof(FileimportLogEntity) && typeof(U) == typeof(FileImportLogViewModel))
            {
                outVM = MapImportLogToViewModel(input as FileimportLogEntity) as U;

                return true;
            }

            if (typeof(T) == typeof(WebTransactionDbObject) && typeof(U) == typeof(TransactionViewModel))
            {
                outVM = MapTransactionToViewModel(input as WebTransactionDbObject) as U;

                return true;
            }

            if (typeof(T) == typeof(SupplierEntity) && typeof(U) == typeof(SupplierViewModel))
            {
                outVM = MapSupplierToViewModel(input as SupplierEntity) as U;

                return true;
            }
            
            if (typeof(T) == typeof(BasketEntity) && typeof(U) == typeof(OrderViewModel))
            {
                outVM = MapOrderToiewModel(input as BasketEntity) as U;

                return true;
            }

            if (typeof(T) == typeof(BasketEntity) && typeof(U) == typeof(OrderViewModel))
            {
                outVM = MapOrderToiewModel(input as BasketEntity) as U;

                return true;
            }

            if (typeof(T) == typeof(BaskeLineEntity) && typeof(U) == typeof(OrderItemViewModel))
            {
                outVM = MapOrderItemToViewModel(input as BaskeLineEntity) as U;

                return true;
            }


            if (typeof(T) == typeof(BasketEntity) && typeof(U) == typeof(ShippingOrderViewModel))
            {
                outVM = MapShippingOrderToViewModel(input as BasketEntity) as U;

                return true;
            }

            if (typeof(T) == typeof(BaskeLineEntity) && typeof(U) == typeof(ShippingOrderItemViewModel))
            {
                outVM = MapShippingOrderItemToViewModel(input as BaskeLineEntity) as U;

                return true;
            }


            return false;
        }

        /*** Private Methods ***/

        private static OrderViewModel MapOrderToiewModel(BasketEntity entity)
        {
            //Map Order
            OrderViewModel orderModel = new OrderViewModel
            {
                TransactionId = entity.OrderTransaction.TransactionID,
                OrderNumber = entity.OrderTransaction.OrderNumber,
                OrderDate = entity.OrderTransaction.OrderDate,
                OrderItems = new List<OrderItemViewModel>()
            };

            //Map Order Items
            foreach (BaskeLineEntity basketLine in entity.BasketItems)
            {
                OrderItemViewModel itemViewModel = new OrderItemViewModel();

                MapToViewModel<BaskeLineEntity, OrderItemViewModel>(basketLine, out itemViewModel);

                if (itemViewModel != null)
                {
                    orderModel.OrderItems.Add(itemViewModel);
                }
            }

            return orderModel;
        }

        private static OrderItemViewModel MapOrderItemToViewModel(BaskeLineEntity entity)
        {
            OrderItemViewModel viewModel = new OrderItemViewModel
            {
                SkuId = entity.SkuId,
                ProductCode = entity.Sku.Code,
                SupplierProductCode = entity.Sku.SupplierProductCode,
                UnitOfMeasurement = entity.Sku.UnitOfMeasurement,
                EanCode = entity.Sku.EanCode,
                ProductName = entity.Sku.Name,
                OrderQty = entity.Quantity,
                OutOfStock = entity.OutOfStock.HasValue ? entity.OutOfStock.Value : false,
                QtyAvailable = entity.QtyAvailable.HasValue ? entity.QtyAvailable.Value : entity.Quantity
            };

            return viewModel;
        }


        private static ShippingOrderViewModel MapShippingOrderToViewModel(BasketEntity entity)
        {
            //Map Order
            ShippingOrderViewModel orderModel = new ShippingOrderViewModel
            {
                TransactionId = entity.OrderTransaction.TransactionID,
                UserBatchId = entity.OrderTransaction.UserBatchId.Value,
                OrderNumber = entity.OrderTransaction.OrderNumber,
                OrderDate = entity.OrderTransaction.OrderDate,
                Shipped = entity.OrderTransaction.DespatchStatus == 
                    GlobalContants.GetDespatchStatus(GlobalContants.DespatchStatus.Shipped),
                OrderItems = new List<ShippingOrderItemViewModel>()
            };

            //Map Order Items
            foreach (BaskeLineEntity basketLine in entity.BasketItems)
            {
                ShippingOrderItemViewModel itemViewModel = new ShippingOrderItemViewModel();

                MapToViewModel<BaskeLineEntity, ShippingOrderItemViewModel>(basketLine, out itemViewModel);

                if (itemViewModel != null)
                {
                    orderModel.OrderItems.Add(itemViewModel);
                }
            }

            return orderModel;
        }

        private static ShippingOrderItemViewModel MapShippingOrderItemToViewModel(BaskeLineEntity entity)
        {
            ShippingOrderItemViewModel viewModel = new ShippingOrderItemViewModel
            {
                BasketId = entity.BasketId,
                BasketLineId = entity.BasketLineId,
                ProductCode = entity.Sku.Code,
                SupplierProductCode = entity.Sku.SupplierProductCode,               
                EanCode = entity.Sku.EanCode,
                ProductName = entity.Sku.Name,
                Quantity = entity.Quantity,
                Weight = (entity.Sku.Weight.HasValue ? entity.Sku.Weight.Value : 0)
            };

            return viewModel;
        }

        

        private static LoggedInUserViewModel MapUserToViewModel(UserEntity entity)
        {
            //Map User Role
            RoleViewModel role = new RoleViewModel();
            MapToViewModel<RoleEntity, RoleViewModel>(entity.Role, out role);

            //Map User
            LoggedInUserViewModel viewModel = new LoggedInUserViewModel
            {
                UserId = entity.UserId,
                Username = entity.Username,
                Title = entity.Title,
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                Email = entity.Email,
                Phone_Mobile = entity.Phone_Mobile,
                Phone_Office = entity.Phone_Office,
                Role = role               
            };

            //Map User Company if exists
            if (entity.Company != null)
            {
                CompanyViewModel company =new CompanyViewModel();
                MapToViewModel<CompanyEntity, CompanyViewModel>(entity.Company, out company);

                viewModel.Company = company;              
            }

            //Map User Supplier if exists
            if (entity.Supplier != null)
            {
                SupplierViewModel supplier = new SupplierViewModel();
                MapToViewModel<SupplierEntity, SupplierViewModel>(entity.Supplier, out supplier);

                viewModel.Supplier = supplier;
            }


            return viewModel;
        }

        private static RoleViewModel MapRoleToViewModel(RoleEntity entity)
        {
            RoleViewModel viewModel = new RoleViewModel
            {
                RoleId = entity.RoleId,
                Name = entity.Name,
            };

            return viewModel;
        }

        private static SupplierViewModel MapSupplierToViewModel(SupplierEntity entity)
        {
            SupplierViewModel viewModel = new SupplierViewModel
            {
                SupplierId = entity.SupplierId,
                Name = entity.Name,
                Code = entity.Code,
                CompanyId = entity.CompanyId,
                AddressLine1 = entity.AddressLine1,
                AddressLine2 = entity.AddressLine2,
                Town = entity.Town,
                County = entity.County,
                CountryCode =  entity.CountyCode,
                Postcode = entity.Postcode,
                TelePhone = entity.TelePhone,
                Extension = entity.Extension,
                Fax = entity.Fax,
                ShipperAttnName = entity.ShipperAttnName,
                LabelPrinterName = entity.LabelPrinterName,
                StandarPrinterName = entity.StandarPrinterName,
                LabelPrinterIP = entity.LabelPrinterIP,
                UpsAccountNumber = entity.UpsAccountNumber
            };

            return viewModel;
        }

        private static CompanyViewModel MapCompanyToViewModel(CompanyEntity entity)
        {
            CompanyViewModel viewModel = new CompanyViewModel
            {
                CompanyId = entity.CompanyId,
                Name = entity.Name,
                CountryId =  entity.CountryId,
                Line1 = entity.Line1,
                Line2 = entity.Line2
            };

            return viewModel;
        }



        private static FileImportLogViewModel MapImportLogToViewModel(FileimportLogEntity entity)
        {
            FileImportLogViewModel viewModel = new FileImportLogViewModel
            {
                LogId = entity.LogId,
                ImportDate = entity.ImportDate,
                FileName = entity.FileName,
                SkusUpdated = entity.SkusUpdated,
                ErrorDescription = entity.ErrorDescription
            };

            return viewModel;
        }

        private static TransactionViewModel MapTransactionToViewModel(WebTransactionDbObject entity)
        {
            TransactionViewModel viewModel = new TransactionViewModel
            {
                TransactionId  = entity.TransactionID,
                TransactionType = entity.TransactionType,
                PaymentMethod = entity.PaymentMethod,
                TransactionStatus = entity.TransactionStatus,
                Currency = entity.Currency,
                AmountInc = entity.AmountInc,
                AmountEx = entity.AmountEx,
                VAT = entity.VAT,
                UserAccountId = entity.UserAccountID,
                TrackedVisitorId = entity.TrackedVisitorID,
                Created = entity.Created,
                Deleted = entity.Deleted,
                OrderNumber = entity.OrderNumber,
                Shop = entity.Shop,
                Site = entity.Site,
                ReceiptSent = entity.ReceiptSent,
                OrderSentToSOP = entity.OrderSentToSOP,
                OrderSopStatus = entity.OrderSOPStatus,
                ProcessAttempts = entity.ProcessAttempts,
                InvoiceSent = entity.InvoiceSent,
                InvoiceSentDate = entity.InvoiceSentDate,
                Payment_Xml = entity.Payment_Xml,
                Purchase_Xml = entity.Purchase_Xml,
                Checkout_Xml = entity.Checkout_Xml
            };

            return viewModel;
        }
    }
}