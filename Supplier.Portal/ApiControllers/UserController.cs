﻿using System.Web.Http;
using Supplier.Portal.EntityServices;
using Supplier.Portal.ViewModels.User;

namespace Supplier.Portal.ApiControllers
{
    public class UserController : System.Web.Http.ApiController
    {
        private User _user;

        public UserController()
        {
            _user = new User();
        }

        public IHttpActionResult Get()
        {
            return Ok("hello");
        }

        [HttpPost]
        public IHttpActionResult Login(LoginViewModel model)
        {
            if (model == null)
                return BadRequest("Enter Username and password");

            if (string.IsNullOrEmpty(model.Password) || string.IsNullOrEmpty(model.Username))
                return BadRequest("Enter Username and password");

            LoggedInUserViewModel userDetails = new LoggedInUserViewModel();

            bool validUser = _user.Login(model, ref userDetails);

            if (validUser)
            {
                return Ok(userDetails);
            }

            return BadRequest("Invalid username or password");
        }
    }
}
