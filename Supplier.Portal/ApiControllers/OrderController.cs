﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Supplier.Portal.EntityServices;
using Supplier.Portal.ViewModels.Order;

namespace Supplier.Portal.ApiControllers
{
    public class OrderController : ApiController
    {
        private WebOrder _webOrder;
        private OrderShipping _orderShipping;

        public OrderController()
        {
            _webOrder = new WebOrder();
            _orderShipping = new OrderShipping();
        }

        [HttpGet]
        public IHttpActionResult GetUserBatchOrders(int userId)
        {
            string message = String.Empty;
            List<ShippingOrderViewModel> results = new List<ShippingOrderViewModel>();

            bool result = _webOrder.GetOrdersForShipping(userId, ref results, out message);

            if (result == false)
            {
                return BadRequest(message);
            }

            return Ok(results);
        }

        [HttpGet]
        [Route("api/Order/Reload")]
        public IHttpActionResult GetUserBatchOrder(int transactionId, int userId)
        {
            string message = String.Empty;
            ShippingOrderViewModel model = new ShippingOrderViewModel();

            bool result = _webOrder.GetOrderForShipping(transactionId, userId, ref model, out message);

            if (result == false)
            {
                return BadRequest(message);
            }

            return Ok(model);
        }

        [HttpPut]
        [Route("api/Order/Reset")]
        public IHttpActionResult PutResetOrderStatus(int transactionId)
        {
            string message = String.Empty;

            bool result = _webOrder.ResetOrderStatus(transactionId, out message);

            if (result == false)
            {
                return BadRequest(message);
            }

            return Ok();
        }

        [HttpPut]
        [Route("api/Order/Cancel")]
        public IHttpActionResult PutCancelOrderShipping(string trackingNumber, string reason, ShippingOrderViewModel order)
        {
            string message = string.Empty;
            bool result = _orderShipping.CancelShipment(trackingNumber, reason, order, out message);

            if (result == false)
            {
                return BadRequest(message);
            }

            return Ok();
        }

        //[HttpGet]
        //[Route("api/Shipping/Test")]
        //public async Task<IHttpActionResult> GetTestShippingRequest()
        //{
        //    ShippingOrderViewModel orders = new ShippingOrderViewModel
        //    {
        //        OrderNumber =  "12345"
        //    };

        //    UpsResponse.PrimaryErrorCode error = new UpsResponse.PrimaryErrorCode();

        //    ShippingResponseContainer result = _orderShipping.ShipOrder(orders, out error);

        //    if (result.RequestError != null)
        //    {
        //        return BadRequest(error.Description);
        //    }

        //    return Ok(result);
        //}

        [HttpPost]
        public async Task<IHttpActionResult> PostShipOrder(ShippingOrderViewModel order)
        {
            string message = string.Empty;
            bool result = _orderShipping.ShipOrder(order, out message);

            if (result == false)
            {
                return BadRequest(message);
            }

            return Ok(order);
        }

        //public async Task<HttpResponseMessage> GetDocumentAsAttachment()
        //{
        //    string base64MainResult = String.Empty;
        //    string base64GifImage = String.Empty;
        //    string base64PdfImage = String.Empty;
        //    //string base64Receipt = String.Empty;

        //    try
        //    {
        //        string orderNumber = "";
        //        string _imageType = UpsConstants.TestTrackingNumber_Pdf;

        //        LabelResponseContainer labelResponseData = new LabelResponseContainer();

        //        bool result = _orderShipping.DownloadShippingLabels(orderNumber, _imageType, out labelResponseData);

        //        if (result)
        //        {
        //            if (_imageType == UpsConstants.TestTrackingNumber_Pdf)
        //            {
        //                base64MainResult = labelResponseData.LabelResult.LabelImage.GraphicImage;

        //                //base64Receipt = labelResponseData.LabelResults.Receipt.Image.ImageFormat.GraphicImage;
        //            }
        //            else
        //            {
        //                base64MainResult = labelResponseData.LabelResult.LabelImage.HTMLImage;
        //                base64GifImage = labelResponseData.LabelResult.LabelImage.GraphicImage;

        //                //this is a barcode image - Do not use
        //                //base64PdfImage = labelResponseData.LabelResults.LabelImage.PDF417;

        //                //base64Receipt = labelResponseData.LabelResults.Receipt.HTMLImage;
        //            }


        //            var byteArray = Convert.FromBase64String(base64MainResult);

        //            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK)
        //            {
        //                Content = new ByteArrayContent(byteArray)
        //            };

        //            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
        //            {
        //                FileName = "test.pdf"
        //            };

        //            return response;
        //        }

        //        return new HttpResponseMessage(HttpStatusCode.InternalServerError);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new HttpResponseMessage(HttpStatusCode.InternalServerError);
        //    }
        //}

        //[HttpPost]
        //public async Task<IHttpActionResult> PostOrderToShip(ShippingOrderViewModel orders)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        UpsResponse.PrimaryErrorCode error = new UpsResponse.PrimaryErrorCode();

        //        var result = _orderShipping.ShipOrder(orders, out error);

        //        if (error.Description != "")
        //        {
        //            return BadRequest(error.Description);
        //        }

        //        return Ok();
        //    }

        //    return BadRequest(ModelState);
        //}
    }
}
