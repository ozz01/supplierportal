﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Supplier.Portal.EntityServices;
using Supplier.Portal.Services;
using Supplier.Portal.ViewModels.Order;

namespace Supplier.Portal.ApiControllers
{
    public class PickingListController : System.Web.Http.ApiController
    {
        private readonly WebOrder _webOrder;
        private readonly PickingListGenerator _pickingListGenerator;

        public PickingListController()
        {
            _webOrder = new WebOrder();
            _pickingListGenerator = new PickingListGenerator();
        }

        [HttpGet]
        public IHttpActionResult GetUserBatch(int userId)
        {
            string message = String.Empty;
            List<OrderItemViewModel> results = new List<OrderItemViewModel>();

            bool result = _webOrder.GetUserBatchForDisplay(userId, ref results, out message);

            if (result == false)
            {
                return BadRequest(message);
            }

            return Ok(results);
        }

        [HttpGet]
        [Route("api/PickingList/ReAssign")]
        public IHttpActionResult GetAssignBatchToUser(int userId)
        {
            string message = String.Empty;
            
            bool result = _webOrder.AssignNewUserBatch(userId, out message);

            if (result == false)
            {
                return BadRequest(message);
            }

            return Ok();
        }

        [HttpGet]
        [Route("api/PickingList/OutOfStockItems")]
        public IHttpActionResult GetOutOfStockItems(int userId)
        {
            string message = String.Empty;
            List<OrderViewModel> results = new List<OrderViewModel>();

            bool result = _webOrder.GetOutOfStockOrders(userId, results, out message);

            if (result == false)
            {
                return BadRequest(message);
            }

            return Ok(results);
        }

        [HttpGet]
        [Route("api/PickingList/Download")]
        public HttpResponseMessage GetDownloadPickingList(int userId, string fileFormat)
        {
            return _pickingListGenerator.GenerateDownloadStream(userId, fileFormat);
        }

        [HttpGet]
        [Route("api/PickingList/Email")]
        public IHttpActionResult EmailPickingList(int userId, string emailAddress)
        {
            string message = String.Empty;

            bool result = _webOrder.EmailUserBatch(userId, emailAddress, out message);

            if (!result)
            {
                return BadRequest(message);
            }

            return Ok();
        }

        [HttpPost]
        public async Task<IHttpActionResult> PostPickingList(int userBatchId, int userId, List<OrderItemViewModel> orderItems)
        {
            if (ModelState.IsValid)
            {
                string errorMessage = String.Empty;

                bool result = _webOrder.AllocateStockToUserBatch(userBatchId, userId, orderItems, out errorMessage);

                if(result == false)
                {
                    return BadRequest(errorMessage);
                }

                return Ok(result);
            }
            
            return BadRequest(ModelState);            
        }
    }
}
