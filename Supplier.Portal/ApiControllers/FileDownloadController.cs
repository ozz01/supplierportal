﻿using System.Web.Http;
using System.Web.Http.Results;
using Supplier.Portal.EntityServices;
using Supplier.Portal.Services;
using Supplier.UpsModels.Common;

namespace Supplier.Portal.ApiControllers
{
    public class FileDownloadController : ApiController
    {
        private PickingListGenerator _pickingListGenerator;
        private OrderShipping _orderShipping;

        public FileDownloadController()
        {
            _orderShipping = new OrderShipping();
            _pickingListGenerator = new PickingListGenerator();
        }

        [HttpGet]
        [Route("api/FileDownload/PickingList")]
        public IHttpActionResult GetPickingListWorksheetData(int userId)
        {           
            FileDataModel model = _pickingListGenerator.GenerateWorksheetBase64String(userId);

            return Ok(model);
        }

        //todo
        [HttpGet]
        [Route("api/FileDownload/Label")]
        public IHttpActionResult GetRecoverLabel()
        {
            return Ok();
        }

        //todo
        [HttpGet]
        [Route("api/FileDownload/DespatchNote")]
        public IHttpActionResult GetDespatchNote()
        {
            return Ok();

        }
        
        [HttpGet]
        [Route("api/FileDownload/ShippingDocuments")]
        public IHttpActionResult GetShippingDocumentData(int transactionId)
        {
            FileDataModel model = new FileDataModel();

            bool result = _orderShipping.RetrieveShippingDocumentData(transactionId, out model);
            
            if (result == false)
            {
                return BadRequest("Unable to retrieve shipping document data");
            }

            return Ok(model);
        }
    }
}