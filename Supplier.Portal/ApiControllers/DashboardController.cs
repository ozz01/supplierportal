﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Supplier.Portal.EntityServices;
using Supplier.Portal.Services;
using Supplier.Portal.ViewModels;
using Supplier.Portal.ViewModels.Dashboad;

namespace Supplier.Portal.ApiControllers
{
    public class DashboardController : System.Web.Http.ApiController
    {
        private WebOrderDashboard _webOrderDashboard;
        private WebTransaction _webTransaction;
 
        public DashboardController()
        {
            _webTransaction = new WebTransaction();
            _webOrderDashboard = new WebOrderDashboard();
        }

        public IHttpActionResult GetMainDashboardStats()
        {
            _webTransaction = new WebTransaction();

            DashboardViewModel model = _webOrderDashboard.MainDashbaordData();

            return Ok(model);
        }

        [Route("api/Dashboard/SupplierDashboard")]
        public IHttpActionResult GetSupplierDashboardStats(int userId)
        {
            SupplierOrderStatsViewModel model = _webOrderDashboard.SupplierOrderStats(userId);

            return Ok(model);
        }

        [Route("api/Dashboard/SupplierStockData")]
        public IHttpActionResult GetSupplierFileUploadStats(int userId)
        {
            SupplierStockUpdatesViewModel model = _webOrderDashboard.SupplierStockUpdateData(userId);

            return Ok(model);
        }      

        public IHttpActionResult GetTransactionByEmail(string email)
        {
            //hannahbing@yahoo.com
            //daubdolly@aol.com
            List<TransactionViewModel> transactionsByEmail = _webTransaction.FindByEmailAddress(email);

            return Ok(transactionsByEmail.FirstOrDefault());
        }

        public IHttpActionResult GetTransactionByMertextReference(string reference)
        {
            //3299018
            List<TransactionViewModel> transactionByMertexRef = _webTransaction.FindByMertexReference(reference);

            return Ok(transactionByMertexRef.FirstOrDefault());
        }

        public IHttpActionResult GetTransactionById(long transactionId)
        {
            //376361
           List<TransactionViewModel> transactionbyId = _webTransaction.FindByTransactionId(transactionId);

            return Ok(transactionbyId.FirstOrDefault());
        }
    }
}
