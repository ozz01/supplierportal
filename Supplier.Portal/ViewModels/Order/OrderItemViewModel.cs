namespace Supplier.Portal.ViewModels.Order
{
    public class OrderItemViewModel
    {
        public long SkuId { get; set; }

        public int UserBatchId { get; set; }

        public string ProductName { get; set; }

        //This is the WG sku code
        public string ProductCode { get; set; }

        //This is the Supplier EAN code
        public string EanCode { get; set; }

        //This is the Supplier product code
        public string SupplierProductCode { get; set; }

        public string UnitOfMeasurement { get; set; }

        public int OrderQty { get; set; }

        public bool OutOfStock { get; set; }

        public int QtyAvailable { get; set; }
    }
}