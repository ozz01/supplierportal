﻿using System;
using System.Collections.Generic;

namespace Supplier.Portal.ViewModels.Order
{
    public class OrderViewModel 
    {
        public AddressViewModel InvoiceCustomer { get; set; }

        public AddressViewModel DeliveryCustomer { get; set; }

        public long TransactionId { get; set; }

        public string OrderNumber { get; set; }

        public DateTime OrderDate { get; set; }

        public List<OrderItemViewModel> OrderItems { get; set; }
    }
}