﻿using System;
using System.Collections.Generic;
using Supplier.Portal.ViewModels.Company;
using Supplier.UpsModels;

namespace Supplier.Portal.ViewModels.Order
{
    public class ShippingOrderViewModel
    {
       public ShippingOrderViewModel()
        {
             TrackingNumbers = new List<string>();
        }
        
        public long TransactionId { get; set; }

        public string OrderNumber { get; set; }

        public DateTime OrderDate { get; set; }

        public int UserBatchId { get; set; }

        public string ShipmentId { get; set; }

        public decimal PackageWeight { get; set; }

        public string WeightCode { get { return UpsConstants.WeightCode; } }

        public int NumberOfLabels
        {
            get { return CalculateNoLabels(); }
        }

        public bool Shipped { get; set; }

        public SupplierViewModel Supplier { get; set; }

        public AddressViewModel DeliveryAddress { get; set; }

        public List<ShippingOrderItemViewModel> OrderItems { get; set; }

        public List<string> TrackingNumbers { get; set; }

        public string PrinterDownloadEndPoint
        {
            get { return String.Format("FileDownload/ShippingDocuments?transactionId={0}", TransactionId); }
        }


        private int CalculateNoLabels()
        {
            int UpsPackageWeight = Convert.ToInt32(UpsConstants.PackageWeight);

            decimal result = 0;

            result = Math.Truncate(PackageWeight / UpsPackageWeight);

            result = result == 0M ? result + 1 : result;

            return (int)result;
        }
    }
}