﻿namespace Supplier.Portal.ViewModels.Order
{
    public class ShippingOrderItemViewModel
    {
        public long BasketLineId { get; set; }

        //public long SkuId { get; set; }

        public long BasketId { get; set; }

        public string ProductName { get; set; }

        //This is the WG sku code
        public string ProductCode { get; set; }

        //This is the Supplier EAN code
        public string EanCode { get; set; }

        //This is the Supplier product code
        public string SupplierProductCode { get; set; }

        public string UserBarcode { get; set; }

        public int Quantity { get; set; }

        public double Weight { get; set; }
    }
}