﻿using System.ComponentModel.DataAnnotations;

namespace Supplier.Portal.ViewModels.User
{
    public class LoginViewModel
    {
        [MaxLength(100)]
        [Required(ErrorMessage = "*")]
        public string Username { get; set; }

        [MaxLength(150)]
        [Required(ErrorMessage = "*")]
        public string Password { get; set; }
    }
}