﻿using System.ComponentModel.DataAnnotations;

namespace Supplier.Portal.ViewModels.User
{
    public class UserRegisterViewModel
    {
        public int UserId { get; set; }

        [MaxLength(100)]
        [Required(ErrorMessage = "*")]
        public string Username { get; set; }

        [Required(ErrorMessage = "*")]
        public string Email { get; set; }

        [MaxLength(150)]
        [Required(ErrorMessage = "*")]
        public string Password { get; set; }

        [MaxLength(10)]
        [Required(ErrorMessage = "*")]
        public string Title { get; set; }

        [MaxLength(50)]
        [Required(ErrorMessage = "*")]
        public string FirstName { get; set; }

        [MaxLength(50)]
        [Required(ErrorMessage = "*")]
        public string LastName { get; set; }

        [MaxLength(15)]
        public string Phone_Mobile { get; set; }

        [MaxLength(15)]
        public string Phone_Office { get; set; }

        public int? CompanyId { get; set; }

        [Required(ErrorMessage = "*")]
        public int RoleId { get; set; }
    }
}
