﻿using Supplier.Portal.ViewModels.Company;
using Supplier.Portal.ViewModels.Role;

namespace Supplier.Portal.ViewModels.User
{
    public class LoggedInUserViewModel
    {
        public int? UserId { get; set; }

        public string Username { get; set; }

        public string Title { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Phone_Mobile { get; set; }

        public string Phone_Office { get; set; }

        public RoleViewModel Role { get; set; }

        public CompanyViewModel Company { get; set; }

        public SupplierViewModel Supplier { get; set; }
    }
}