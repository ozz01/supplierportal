﻿using System;

namespace Supplier.Portal.ViewModels.Dashboad
{
    public class FileUploadStatsViewModel
    {
        public DateTime ProcessDate { get; set; }
        public int FilesProcessed { get; set; }
        public int ErroneousFiles { get; set; }
        public int SkusUpdated { get; set; }
        public int PendingOrders { get; set; }
        public int OrderBeingProcessed { get; set; }
    }
}