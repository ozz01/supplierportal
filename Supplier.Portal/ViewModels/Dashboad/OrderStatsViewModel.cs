﻿using System;

namespace Supplier.Portal.ViewModels.Dashboad
{
    public class OrderStatsViewModel
    {
        public DateTime ProcessDate { get; set; }


        public int OrdersBeingProcessed { get; set; }
        
        public int OrderItemsBeingProcessed { get; set; }


        public int PendingOrders { get; set; }

        public int PendingOrderItems { get; set; }


        public int OutOfStockOrders { get; set; }

        public int OutOfStockOrderItems { get; set; }


        public int InStockOrders { get; set; }

        public int InStockOrderItems { get; set; }
    }
}