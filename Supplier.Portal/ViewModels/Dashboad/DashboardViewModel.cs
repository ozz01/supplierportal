﻿using System.Collections.Generic;

namespace Supplier.Portal.ViewModels.Dashboad
{
    public class DashboardViewModel
    {
        public FileUploadStatsViewModel FileUploadStats { get; set; }

        public OrderStatsViewModel OrderStats { get; set; }

        public IEnumerable<FileImportLogViewModel> FileImports { get; set; }
    }
}