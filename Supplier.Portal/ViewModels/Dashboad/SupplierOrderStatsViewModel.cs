﻿namespace Supplier.Portal.ViewModels.Dashboad
{
    public class SupplierOrderStatsViewModel : OrderStatsViewModel
    {
        public int MyPickingList { get; set; }

        public int MyPickingListItems { get; set; }

        public bool HaveOrdersToShip { get; set; }
    }
}