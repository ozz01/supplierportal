﻿using System.Collections.Generic;
using Supplier.Portal.ViewModels.Dashboad;

namespace Supplier.Portal.ViewModels
{
    public class SupplierStockUpdatesViewModel
    {
        public FileUploadStatsViewModel FileUploadStats { get; set; }

        public IEnumerable<FileImportLogViewModel> FileImports { get; set; }
    }
}