﻿using System;

namespace Supplier.Portal.ViewModels
{
    public class FileImportLogViewModel
    {
        public long LogId { get; set; }

        public string FileName { get; set; }

        public DateTime ImportDate { get; set; }

        public int SkusUpdated { get; set; }

        public string ErrorDescription { get; set; }
    }
}