﻿namespace Supplier.Portal.ViewModels
{
    public class AddressViewModel
    {
        public string Title { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string Postcode { get; set; }

        public string Town { get; set; }

        public string County { get; set; }

        public string Email { get; set; }

        public string Telephone { get; set; }

        public long CountryId { get; set; }

        public string CountryCode { get; set; }

        public string Country { get; set; }
    }
}