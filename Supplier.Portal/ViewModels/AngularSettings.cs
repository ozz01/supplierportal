﻿namespace Supplier.Portal.ViewModels
{
    public class SettingsDto
    {
        public string WebHostName { get; set; } 
    }

    public class SettingsViewModel
    {
        public string SettingsJson { get; set; }

        public string AngularModuleName { get; set; }
    }
}