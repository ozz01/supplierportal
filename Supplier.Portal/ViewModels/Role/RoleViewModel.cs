﻿namespace Supplier.Portal.ViewModels.Role
{
    public class RoleViewModel
    {
        public int RoleId { get; set; }

        public string Name { get; set; }
    }
}