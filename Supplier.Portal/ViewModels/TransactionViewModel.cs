﻿using System;
using System.Xml.Linq;

namespace Supplier.Portal.ViewModels
{
    public class TransactionViewModel
    {
        public long TransactionId { get; set; }
        
        //public long TransactionTypeId { get; set; }
        public string TransactionType { get; set; }
        
        //public long PaymentMethodId { get; set; }
        public string PaymentMethod { get; set; }
        
        //public long CurrencyId { get; set; }
        //public long TransactionStatusId { get; set; }
        
        public string TransactionStatus { get; set; }
        public string Currency { get; set; }
        public decimal AmountInc { get; set; }
        public decimal AmountEx { get; set; }
        public decimal VAT { get; set; }
        public Guid? UserAccountId { get; set; }
        public long TrackedVisitorId { get; set; }
        public DateTime Created { get; set; }
        //public DateTime LastModified { get; set; }
        public bool Deleted { get; set; }
        public string OrderNumber { get; set; }
        
        //public long? ShopId { get; set; }
        public string Shop { get; set; }
        
        //public long? SiteID { get; set; }
        public string Site { get; set; }

        public bool? ReceiptSent { get; set; }
        public bool? OrderSentToSOP { get; set; }

        public Int16? OrderSopStatus { get; set; }
        public string OrderSopStatusDescription
        {
            get
            {
                switch (OrderSopStatus)
                {
                    case 0:
                        return "Not Processed";
                    case 1:
                        return "OK";
                    case 2:
                        return "Payment Not Complete";
                    case 3:
                        return "Error Retry";
                    case 4:
                        return "Error";
                    case 5:
                        return "B2B Transaction not processed";
                    default:
                        return "";
                }
            }
        }

        public Int16? ProcessAttempts { get; set; }
        public bool? InvoiceSent { get; set; }
        public DateTime? InvoiceSentDate { get; set; }

        public XElement Purchase_Xml { get; set; }
     
        public XElement Checkout_Xml { get; set; }
     
        public XElement Payment_Xml { get; set; }
    }
}