﻿namespace Supplier.Portal.ViewModels.Company
{
    public class CompanyViewModel
    {
        public int CompanyId { get; set; }

        public string Name { get; set; }

        public long? CountryId { get; set; }

        public string Line1 { get; set; }

        public string Line2 { get; set; }

        public string PostCode { get; set; }
    }
}