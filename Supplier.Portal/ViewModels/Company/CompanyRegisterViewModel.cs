﻿using System.ComponentModel.DataAnnotations;

namespace Supplier.Portal.ViewModels.Company
{
    public class CompanyRegisterViewModel
    {     
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        public long? CountryId { get; set; }

        [MaxLength(50)]
        public string Line1 { get; set; }

        [MaxLength(50)]
        public string Line2 { get; set; }

        [MaxLength(10)]
        public string PostCode { get; set; }
}
}