﻿namespace Supplier.Portal.ViewModels.Company
{
    public class SupplierViewModel
    {
        public long SupplierId { get; set; }

        public long CompanyId { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string Town { get; set; }

        public string County { get; set; }

        public string CountryCode { get; set; }

        public string Postcode { get; set; }

        public string TelePhone { get; set; }

        public string Extension { get; set; }

        public string Fax { get; set; }

        public string ShipperAttnName { get; set; }

        public string LabelPrinterName { get; set; }

        public string StandarPrinterName { get; set; }

        public string LabelPrinterIP { get; set; }

        public string UpsAccountNumber { get; set; }
    }
}