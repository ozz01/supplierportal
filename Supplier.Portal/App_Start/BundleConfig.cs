﻿using System.Web.Optimization;

namespace Supplier.Portal
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/WGStyles/css").Include("~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include("~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include("~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include("~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include("~/Scripts/bootstrap.js","~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Site/Css").Include("~/Content/bootstrap.css", "~/Content/Site.css", "~/Content/font-awesome.css"));

            
            //*** Angular Only Bundles
            bundles.Add(new StyleBundle("~/angularStyles/css").Include("~/Content/angular/*.css"));
            bundles.Add(new StyleBundle("~/toasterStyles/css").Include("~/Content/toaster/*.css"));
            bundles.Add(new StyleBundle("~/bootstrapStyles/css").Include("~/Content/bootstrap.css", "~/Content/ui-bootstrap-csp.css"));

            bundles.Add(new ScriptBundle("~/bundles/angularLibraries")
           .Include(
               "~/Scripts/Angular/angular.js"
               , "~/Scripts/Angular/angular-animate.js"
               , "~/Scripts/Angular/angular-ui-router.js"
               , "~/Scripts/Angular/angular-resource.js"
               , "~/Scripts/Angular/angular-strap.js"
               , "~/Scripts/Angular/angular-strap.tpl.js"
               , "~/Scripts/Angular/toaster.js"
               , "~/Scripts/Angular/loading-bar.js"
               , "~/Scripts/Angular/checklist-model.js"
               , "~/Scripts/Angular/angular-messages.js"
               , "~/Scripts/Angular/angular-cookies.js"
           ));

            bundles.Add(new ScriptBundle("~/bundles/UIbootstrapLibraries").Include("~/Scripts/bootstrap/ui-bootstrap-tpls.js"));


            /** Angular App libraries **/
            bundles.Add(new ScriptBundle("~/bundles/app").Include("~/app/app.js"));
            bundles.Add(new ScriptBundle("~/bundles/core").Include("~/app/core/core.module.js", "~/app/core/*.js"));
            bundles.Add(new ScriptBundle("~/bundles/common").Include("~/app/common/common.module.js", "~/app/common/*.js"));
            bundles.Add(new ScriptBundle("~/bundles/widgets").Include("~/app/widgets/widgets.module.js", "~/app/widgets/*.js"));
            bundles.Add(new ScriptBundle("~/bundles/data").Include("~/app/data/data.module.js", "~/app/data/*.js"));

            bundles.Add(new ScriptBundle("~/bundles/settingsBlock").Include("~/app/blocks/settings/settings.module.js", "~/app/blocks/settings/*.js"));
            
            bundles.Add(new ScriptBundle("~/bundles/loggerBlock").Include("~/app/blocks/logger/logger.module.js", "~/app/blocks/logger/*.js"));
            bundles.Add(new ScriptBundle("~/bundles/routerBlock").Include("~/app/blocks/router/router.module.js", "~/app/blocks/router/*.js"));
            bundles.Add(new ScriptBundle("~/bundles/exceptionBlock").Include("~/app/blocks/exception/exception.module.js", "~/app/blocks/exception/*.js"));
            bundles.Add(new ScriptBundle("~/bundles/securityBlock").Include("~/app/blocks/security/security.module.js", "~/app/blocks/security/*.js"));

            /** WG App libraries **/
            bundles.Add(new ScriptBundle("~/bundles/layout").Include("~/app/layout/layout.module.js", "~/app/layout/*.js"));
            bundles.Add(new ScriptBundle("~/bundles/shared").Include("~/app/shared/directives/*.js"));
            bundles.Add(new ScriptBundle("~/bundles/admin").Include("~/app/admin/admin.module.js", "~/app/admin/*.js"));
            bundles.Add(new ScriptBundle("~/bundles/home").Include("~/app/home/home.module.js", "~/app/home/*.js"));
            bundles.Add(new ScriptBundle("~/bundles/supplier")
                .Include(
                    "~/app/Supplier/supplier.module.js",
                    "~/app/supplier/*.js", 
                    "~/app/Supplier/admin/*.js", 
                    "~/app/Supplier/stockPicking/*.js", 
                    "~/app/Supplier/orderShipping/*.js"));
           
            bundles.Add(new ScriptBundle("~/bundles/wgAdmin").Include("~/app/WG/admin/wgAdmin.module.js", "~/app/WG/admin/*.js"));
            bundles.Add(new ScriptBundle("~/bundles/wgSupplierSupport").Include("~/app/WG/supplierSupport/wgSupplierSupport.module.js", "~/app/WG/supplierSupport/*.js"));
            bundles.Add(new ScriptBundle("~/bundles/wgCustomerSupport").Include("~/app/WG/customerSupport/wgCustomerSupport.module.js", "~/app/WG/customerSupport/*.js"));
        }
    }
}
