﻿(function () {
    "use strict";

    angular
        .module("app.admin")
        .run(appRun);

    appRun.$inject = ["routerHelper"];

    function appRun(routerHelper) {
        routerHelper.configureStates(getStates(), "");
    }

    function getStates() {
        return [
			{
				state: "Admin",
			    config: {
			        url: "/Admin",
			        templateUrl: "app/admin/index.html",
			        controller: "adminCtrl",
			        controllerAs: "vm"
			    }
			}
        ];
    }
})();