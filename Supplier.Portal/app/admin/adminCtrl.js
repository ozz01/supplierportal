﻿(function () { 
    'use strict';

    angular
       .module("app.admin").controller("adminCtrl", adminCtrl);

    adminCtrl.$inject = ["$state", "logger"];

    function adminCtrl($state, logger) {
        var vm = this;
        vm.appPoolAwake = true;
    };
})();