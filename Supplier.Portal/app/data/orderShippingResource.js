﻿(function () {
    "use strict";

    angular
        .module("app.data")
        .factory("orderShippingResource", orderShippingResource);

    orderShippingResource.$inject = ["$resource", "settings"];

    function orderShippingResource($resource, settings)
    {
        var hostUrl = settings.GetApiPath();

        hostUrl = hostUrl + "Order";

        var resource = $resource(hostUrl,
           null,
            {
                "getSupplierOrders": { method: "GET", isArray: true },
                "getOrder": { method: "GET", url: hostUrl + "/Reload", isArray: false },
                "resetOrderStatus": { method: "PUT", url: hostUrl + "/Reset", isArray: false },
                "cancelShipping": { method: "PUT", url: hostUrl + "/Cancel", isArray: false },
                "shipOrder": { method: "POST", isArray: false },             
            });

        return resource;
    }
})();