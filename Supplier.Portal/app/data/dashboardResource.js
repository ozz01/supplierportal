﻿(function () {
    "use strict";

    angular
        .module("app.data")
        .factory("dashboardResource", dashboardResource);

    dashboardResource.$inject = ["$resource", "settings"];

    function dashboardResource($resource, settings)
    {    
        var hostUrl = settings.GetApiPath();

        hostUrl = hostUrl + "Dashboard";

        var resource = $resource(hostUrl,
           null,
            {
                "get": { method: "GET", isArray: false },
                "getSupplierOrders": { method: "GET", url: hostUrl + "/SupplierDashboard", isArray: false },
                "getSupplierStockData": { method: "GET", url: hostUrl + "/SupplierStockData", isArray: false },
            });

        return resource;
    }
})();