﻿(function () {
    "use strict";

    angular
        .module("app.data")
        .factory("userResource", userResource);

    userResource.$inject = ["$resource", "$location", "settings"];

    function userResource($resource, $location, settings)
    {
        var hostUrl = settings.GetApiPath();

        var resource = $resource(hostUrl + "User",
           null,
            {
                "get" : { method: "GET", isArray: false },
                "login": { method: "POST", isArray: false }   
            });

        return resource;
    }
})();