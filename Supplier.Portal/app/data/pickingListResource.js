﻿(function () {
    "use strict";

    angular
        .module("app.data")
        .factory("pickingListResource", pickingListResource);

    pickingListResource.$inject = ["$resource", "settings"];

    function pickingListResource($resource, settings)
    {
        var hostUrl = settings.GetApiPath();

        hostUrl = hostUrl + "PickingList";

        var resource = $resource(hostUrl,
           null,
            {
                "getUserBatch": { method: "GET", isArray: true },
                "savePickingList": { method: "POST", isArray: false },
                "reassignBatch": { method: "GET", url: hostUrl + "/ReAssign", isArray: false },
                "getOutOfStockItems": { method: "GET", url: hostUrl + "/OutOfStockItems", isArray: true },
                "emailUserBatch": { method: "GET", url: hostUrl + "/Email", isArray: false },
            });

        return resource;
    }
})();