﻿(function () {
    "use strict";

    angular
        .module("blocks.settings")
        .factory("settings", settings);

    settings.$inject = ["$q", "$location"];

    function settings($q, $location) {

        var service = {
            GetApiPath: getApiPath,
        };

        return service;       

        function getApiPath() {
            var url = $location.absUrl();
            var angularIdentifierPostition = url.indexOf('#');

            var hostUrl = url.substring(0, angularIdentifierPostition) + "api/";

            return hostUrl;
        }
    }
}
());