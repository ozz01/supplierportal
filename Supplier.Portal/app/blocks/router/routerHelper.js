﻿(function () {
    "use strict";

    angular
        .module("blocks.router")
        .provider("routehelperConfig", routehelperConfig)
        .factory("routerHelper", routerHelper);

    routerHelper.$inject = ["$rootScope", "$state", "routehelperConfig", "logger"];
    routehelperConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function routehelperConfig($stateProvider, $urlRouterProvider) {
        this.config = {
            $stateProvider: $stateProvider,
            $urlRouterProvider: $urlRouterProvider
            // docTitle: ''
            // resolveAlways: {ready: function(){ } }
        };

        this.$get = function () {
            return {
                config: this.config
            };
        };
    }

    function routerHelper($rootScope, $state, routehelperConfig, logger) {
        var $stateProvider = routehelperConfig.config.$stateProvider;
        var $urlRouterProvider = routehelperConfig.config.$urlRouterProvider;
        var handlingRouteChangeError = false;
        var hasOtherwise = false;
        var stateCounts = {
            errors: 0
        };

        var service = {
            configureStates: configureStates,
            getStates: getStates
        };

        init();

        return service;

        ///////////////

        function configureStates(states, otherwisePath) {
            states.forEach(function (state) {
                $stateProvider.state(state.state, state.config);
            });

            if (otherwisePath && !hasOtherwise) {
                hasOtherwise = true;
                $urlRouterProvider.otherwise(otherwisePath);
            }
        }

        function handleRoutingErrors() {
            // Route cancellation:
            // On routing error, go to the dashboard.
            // Provide an exit clause if it tries to do it twice.
            $rootScope.$on("$stateChangeError",
                function (event, toState, toParams, fromState, fromParams, error) {
                    if (handlingRouteChangeError) {
                        return;
                    }

                    stateCounts.errors++;
                    handlingRouteChangeError = true;

                    logger.warning("Warning", error);
                    $state.go("home");
                }
            );
        }

        function init() {
            handleRoutingErrors();
        }

        function getStates() { return $state.get(); }
    }
})();