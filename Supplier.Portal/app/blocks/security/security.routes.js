﻿(function () {
    "use strict";

    angular
        .module("blocks.security")
        .run(appRun);

    appRun.$inject = ["routerHelper"];

    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
			{
			    state: "unauthorised",
			    config: {
			        templateUrl: "app/blocks/security/unauthorised.html",
			        url: "/unauthorised"
			    }
			}
        ];
    }
})();