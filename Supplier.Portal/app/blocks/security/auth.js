﻿(function () {
    "use strict";

    angular
        .module("blocks.security")
        .factory("auth", auth);

    auth.$inject = ["$q", "$state", "$stateParams", "$cookies"];

    function auth($q, $state, $stateParams, $cookies) {
        var user;

        var service = {
            GetUser: getUser,
            SetUser: setuser,
            CheckStateForUser: checkSate,           
        };

        return service;

        function setuser(data) {
            user = data;

            setUserCookie(data);
        }

        function getUser() {
            if (user == null) {
                user = $cookies.getObject('user');

                if (user == undefined) {
                    $state.go("Home");
                }
            }

            return user;
        }
     
        function setUserCookie(user) {
            //$cookies.remove('user');
            
            var cookieLifeSpanMinutes = 60;
            var today = new Date();
            var expired = new Date(today);

            expired.setMinutes(today.getMinutes() + cookieLifeSpanMinutes); 

            $cookies.putObject('user', user, { expires: expired });
        }

        function checkSate(state) {
            getUser();

            if (user === undefined || user === null) {
                $state.go("Home");
            }

            if (user.Role.Name === "Supplier Order Processor") {
                if (state === "supplierStockPick" || state === "supplierOrderDespatch") return true;
            } else if (user.Role.Name === "Supplier Admin Processor") {
                 if (state === "supplierAdmin") return true;
            } else if (user.Role.Name === "WG Tools Administrator") {
                if (state === "wgAdmin") return true;
            } else if (user.Role.Name === "WG Supplier Support") {
                if (state === "wgSupplierSupport") return true;
            } else if (user.Role.Name === "WG Customer Support") {
                if (state === "wgCustomerSupport") return true;
            }

            return false;
        }
    }
}
());