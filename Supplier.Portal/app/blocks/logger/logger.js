﻿(function () {
    "use strict";

    angular
        .module("blocks.logger")
        .factory("logger", logger);

    logger.$inject = ["$log", "toaster"];

    function logger($log, toaster) {
        var service = {
            showToasts: true,

            error: error,
            info: info,
            success: success,
            warning: warning,

            // straight to console; bypass toastr
            log: $log.log
        };

        return service;
        /////////////////////

        function error(message, title, data) {
            toaster.error(message, title);
            $log.error("Error: " + message, data);
        }

        function info(message, title, data) {
            toaster.info(message, title);
            $log.info("Info: " + message, data);
        }

        function success(message, title, data) {
            toaster.success(message, title);
            $log.info("Success: " + message, data);
        }

        function warning(message, title, data) {
            toaster.warning(message, title);
            $log.warn("Warning: " + message, data);
        }
    }
}());