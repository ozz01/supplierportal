﻿(function () {
    "use strict";

    angular
        .module("blocks.exception")
        .factory("exception", exception);

    exception.$inject = ["logger"];

    function exception(logger) {
        var service = {
            catcher: catcher
        };

        return service;

        ////////////

        function catcher(message, reason) {
            var errorMessage = evaluateReason(reason);
            logger.error(message, errorMessage);
            return errorMessage;
        }

        function evaluateReason(reason) {
            if (reason.data) {
                if (reason.data.Message) {
                    return reason.data.Message;
                } else {
                    return reason.data;
                }
            } else {
                return reason;
            }
        }
    }
})();