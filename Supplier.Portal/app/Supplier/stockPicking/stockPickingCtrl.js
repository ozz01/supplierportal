﻿(function () { 
    'use strict';

    angular
        .module("app.supplier")
        .controller("stockPickingCtrl", stockPickingCtrl);

    stockPickingCtrl.$inject = ["$state", "$window", "logger", "auth", "settings", "dashboardResource", "pickingListResource"];

    function stockPickingCtrl($state, $window, logger, auth, settings, dashboardResource, pickingListResource) {
        var vm = this;
        var user = null;


        /*** Public Variables ***/
        vm.IsAdminUser = false;
        vm.UserFullName = null;
        vm.UserEmail = null;
        vm.Company = {"Name": "Bedeck"};
        vm.DashboardData = {};
        vm.PartialStock = false;
        vm.StockValidated = false;
        vm.PickingList = [];
        vm.Errors = [];
        vm.fileFormat = {Excel : "Excel", Csv: "CSV" }
        vm.overideEmailAddress = null;
        vm.emailFormat = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
        vm.PrintApplicationUrl = null;

        /*** Public Functions ***/

        vm.EmailBatch = emailBatch;
        vm.AssignBatch = assignBatch;
        vm.PickAll = pickAll;
        vm.OutOfStockClicked = outOfStockClicked;
        vm.SaveData = validatePickingList;
        vm.Download = download;

        //////////////////////////////


        getUserDetails();
        
        function getUserDetails() {
            if (!auth.CheckStateForUser($state.$current.self.name)) {
                $state.go("Home");

                return;
            }
            
            user = auth.GetUser();

            if (user != null) {
                vm.Company = user.Company;
                vm.UserFullName = user.FirstName + " " + user.LastName;
                vm.UserEmail = user.Email;

                if (user.Role.Name === "SupplierAdmin") {
                    vm.IsAdminUser = true;
                }

                getDashboardData();
            } 
        }


        /*** Dashboard Functions ***/
        
        function getDashboardData() {
            vm.appPoolAwake = false;

            dashboardResource.getSupplierOrders({ "userId": user.UserId })
                .$promise
                .then(function (data) {
                    vm.DashboardData = data;
                    //vm.PickingList = data.PickingList;

                    if (vm.DashboardData.MyPickingList > 0) {
                        getPickingList();
                    }
                    else if (vm.DashboardData.HaveOrdersToShip === true) {
                        $state.go("supplierOrderDespatch");
                    }

                    vm.appPoolAwake = true;
                })
                .catch(function (response) {
                    vm.appPoolAwake = true;

                    logger.error("", response.data.Message);
                });
        };
          
        function assignBatch() {
            if (vm.IsAdminUser) {
                logger.info("", "Administrators are not permited to process orders.");
                return;
            }

            if (vm.DashboardData.MyPickingList > 0) {
                logger.info("", "You must process your current picking list before processing a new batch.");
                return;
            }

            vm.appPoolAwake = false;

            pickingListResource.reassignBatch({ "userId": user.UserId })
                .$promise
                .then(function () {                
                    vm.appPoolAwake = true;

                    logger.info("","The batch has been assigned to you");

                    getDashboardData();
                })
                .catch(function (response) {
                    vm.appPoolAwake = true;

                    logger.error("", response.data.Message);
                });
        }     
       
        function emailBatch() {
            vm.appPoolAwake = false;
            var emailAddress = "";

            if (vm.overideEmailAddress != null ) {
                emailAddress = vm.overideEmailAddress;
            } else {
                emailAddress = user.Email;
            }

            pickingListResource.emailUserBatch({ "userId": user.UserId, "emailAddress": emailAddress })
                .$promise
                .then(function () {
                    vm.appPoolAwake = true;
                    logger.info("", "Email Sent");
                })
                .catch(function (response) {
                    vm.appPoolAwake = true;

                    logger.error("", response.data.Message);
                });
        }   

        /*** Picking List Functions ***/

        function getPickingList() {
            vm.appPoolAwake = false;

            pickingListResource.getUserBatch({ "userId": user.UserId })
                .$promise
                .then(function (data) {
                    vm.PickingList = data;

                    setPrinterLink();
                    vm.appPoolAwake = true;                  
                })
                .catch(function (response) {
                    vm.appPoolAwake = true;

                    logger.error("", response.data.Message);
                });
        }

        //todo: retest
        function download(fileFormat) {
            var hostUrl = settings.GetApiPath();
            
            var url = hostUrl + "PickingList/Download"
                + "?userId=" + user.UserId
                + "&fileFormat=" + fileFormat;

            $window.open(url, '_blank');
        }

        function pickAll() {
            vm.PickingList.forEach(function (item) {
                item.QtyAvailable = item.OrderQty;
                item.OutOfStock = false;
            });

            vm.StockValidated = true;
        }

        function pickInStockOnly() {
            vm.PickingList.forEach(function (item) {
                if (!item.OutOfStock) {
                    item.QtyAvailable = item.OrderQty;
                } else {
                    if (item.QtyAvailable === null || item.QtyAvailable === undefined || item.QtyAvailable === "")
                        item.QtyAvailable = 0;
                }
            });

            vm.StockValidated = true;
        }

        function outOfStockClicked(item) {
            vm.StockValidated = false;

            if (item.OutOfStock) {
                item.QtyAvailable = 0;
            } else {
                item.QtyAvailable = item.OrderQty;
            }

            pickInStockOnly();
            checkIfAnyOutOfStock();
        }

        function checkIfAnyOutOfStock() {
            vm.PartialStock = false;

            vm.PickingList.forEach(function (item) {
                if (item.OutOfStock) {
                    vm.PartialStock = true;
                }
            });

            vm.StockValidated = true;
        }

        function validatePickingList() {
            if (vm.PartialStock) {
                var counter = 0;
                var error = {};
                vm.Errors = [];

                vm.PickingList.forEach(function (item) {
                    counter++;

                    //Remover Leading zero's by converting to integer
                    if (item.QtyAvailable.length > 1) {
                        item.QtyAvailable = parseInt(item.QtyAvailable, 10);
                    }

                    if (item.OutOfStock) {                     
                        if (item.QtyAvailable === undefined || item.QtyAvailable === null || item.QtyAvailable === "") {
                            error = { "Row": counter, "Error": "Please enter the Qty In Stock if known or enter 0" }
                            vm.Errors.push(error);
                        }
                        else if (item.QtyAvailable < 0) {
                            error = { "Row": counter, "Error": "The Qty In Stock value is less than 0" }
                            vm.Errors.push(error);
                        }
                        else if (item.QtyAvailable >= item.OrderQty) {
                            error = { "Row": counter, "Error": "The Qty In Stock value is greater than or equal to the Quantity ordered but the sku is marked as Out Of Stock" }
                            vm.Errors.push(error);
                        }                       
                    }
                });
                
                if (vm.Errors.length === 0) {
                    savePickingList();
                }
            } else {
                savePickingList();
            }
        }

        function savePickingList() {
            vm.appPoolAwake = false;

            var userBatchId = vm.PickingList[0].UserBatchId;

            pickingListResource.savePickingList({ "userBatchId": userBatchId, "userId": user.UserId }, vm.PickingList)
                .$promise
                .then(function (data) {
                    vm.appPoolAwake = true;
                    vm.PickingList = [];
                    getDashboardData();
                })
                .catch(function (response) {
                    vm.appPoolAwake = true;

                    logger.error("", response.data.Message);
                });
        }

        //todo: test
        function setPrinterLink()
        {
            var hostUrl = settings.GetApiPath();

            hostUrl = hostUrl + "FileDownload/PickingList/?userId=" + user.UserId;

            vm.PrintApplicationUrl = "app/ClickOnce/Supplier.PrinterServer.application" + "?url=" + hostUrl;
        }
    };
})();