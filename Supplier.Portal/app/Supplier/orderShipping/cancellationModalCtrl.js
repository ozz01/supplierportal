﻿(function() {
    "use strict";

    angular
        .module("app.supplier")
        .controller("cancellationModalCtrl", cancellationModalCtrl);

    cancellationModalCtrl.$inject = ["$modalInstance", "$scope","logger", "orderShippingResource", "Order", "TrackingNumber"];

    function cancellationModalCtrl($modalInstance, $scope, logger, orderShippingResource, Order, TrackingNumber) {
        var vm = this;
        vm.Reason = "";

        vm.SelectedOrder = Order;
        vm.TrackingNumber = TrackingNumber;
        vm.Submit = procesCancellation;
        vm.Close = close;

        ///////////////////

        function close() {
            $modalInstance.dismiss('cancel');
        }

        function procesCancellation() {
            vm.appPoolAwake = false;

            orderShippingResource.cancelShipping({ "trackingNumber": vm.TrackingNumber, "reason": vm.Reason }, vm.SelectedOrder)
                .$promise
                .then(function (data) {
                    vm.appPoolAwake = true;

                    logger.info("", "Shipping Cancelled");
                    $modalInstance.close();
                })
                .catch(function (response) {
                    vm.appPoolAwake = true;
                    logger.error("", response.data.Message);
                    $modalInstance.close();
                });
        }
    };
})();