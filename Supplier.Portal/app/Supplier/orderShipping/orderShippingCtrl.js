﻿(function() {
    'use strict';

    angular
        .module("app.supplier")
        .controller("orderShippingCtrl", orderShippingCtrl);

    orderShippingCtrl.$inject = ["$state", "$window", "$modal", "$interval", "logger", "auth", "settings",  "orderShippingResource"];

    function orderShippingCtrl($state, $window, $modal, $interval, logger, auth, settings, orderShippingResource) {
        var vm = this;
        var user = null;

        /*** Public Variables ***/
        vm.Company = { "Name": "Bedeck" };
        vm.UserFullName = null;
        vm.UserEmail = null;
        vm.Errors = [];
        vm.Orders = [];
        vm.OrdersComplete = false;
        vm.Printing = false;
        vm.counter = 3;

        vm.SelectedOrder = null;
        vm.SelectedOrderIndex = 0;
        vm.OrderCount = 0;
        vm.FirstRecord = false;
        vm.LastRecord = false;

        //vm.fileFormat = { Excel: "Excel", Csv: "CSV" }

        vm.PrintApplicationUrl = null;

        /*** Public Functions ***/

        vm.ValidateOrder = validateOrder;
        vm.ResetOrder = resetOrder;
        vm.CancelShipping = cancelShipping;
        vm.ShipOrder = shipOrder;
        vm.GoToNextOrder = goToNextOrder;
        vm.GotoPreviousOrder = gotoPreviousOrder;
        vm.PrinterResult = printerResult;

        //////////////////////////////        

        getUserDetails();

        function getUserDetails() {
            if (!auth.CheckStateForUser($state.$current.self.name)) {
                $state.go("Home");

                return;
            }

            user = auth.GetUser();

            if (user != null) {
                vm.Company = user.Company;
                vm.UserFullName = user.FirstName + " " + user.LastName;
                vm.UserEmail = user.Email;

                //vm.PrintApplicationUrl = "http://wgb2013/ClickOnce/Supplier.PrinterServer.application?userId="
                //    + user.UserId
                //    + "&fileName=ShippingLabel.zls"
                //    + "&fileFormat=Zebra";
            }

            getOrdersForShipping();
        }

        /*** Shipping Functions ***/
        ////////////////////////////

        function getOrdersForShipping() {
            vm.appPoolAwake = false;

            orderShippingResource.getSupplierOrders({ "userId": user.UserId })
                .$promise
                .then(function(data) {

                    //Check we have data
                    if (data.length > 0) {
                        vm.Orders = data;

                        initialiseOrders();
                    }

                    vm.appPoolAwake = true;
                })
                .catch(function(response) {
                    vm.appPoolAwake = true;

                    logger.error("", response.data.Message);
                });
        };

        function shipOrder() {
            vm.appPoolAwake = false;

            orderShippingResource.shipOrder(vm.SelectedOrder)
                .$promise
                .then(function(data) {
                    vm.appPoolAwake = true;

                    vm.Orders[vm.SelectedOrderIndex - 1] = data;
                    vm.SelectedOrder = data;

                    printDocuments();
})
                .catch(function(response) {
                    vm.appPoolAwake = true;
                    logger.error("", response.data.Message);
                });
        }
    
        //todo: check for last order 
        function resetOrder() {
            vm.appPoolAwake = false;

            orderShippingResource.resetOrderStatus({ "transactionId": vm.SelectedOrder.TransactionId}, null)
            .$promise
            .then(function() {
                    logger.info("", "Order reset to Pending");

                    vm.Orders.splice(vm.SelectedOrderIndex - 1, 1);
                    vm.OrderCount = vm.Orders.length;

                    vm.appPoolAwake = true;

                    if (vm.OrderCount > 0) {
                        if (vm.SelectedOrderIndex <= vm.OrderCount) {
                            goToNextOrder();
                        } else {
                            gotoPreviousOrder();
                        }
                    } else {
                        setNavigationState();
                    }
                })
            .catch(function(response) {
                vm.appPoolAwake = true;
                logger.error("", response.data.Message);
            });
        }

        function cancelShipping(trackingNumber) {

            vm.modalInstance = $modal.open({
                templateUrl: 'app/Supplier/orderShipping/cancellationModal.html',
                controller: 'cancellationModalCtrl',
                controllerAs: "vm",
                size: 'md',
                resolve: {
                    Order: function () { return vm.SelectedOrder; },
                    TrackingNumber: function () { return trackingNumber; }
                }
            });

            vm.modalInstance.result.then(function () {
                reloadOrder();
            }), function () {
                reloadOrder();
            };
        }

        function reloadOrder() {
            vm.appPoolAwake = false;

            orderShippingResource.getOrder({ "transactionId": vm.SelectedOrder.TransactionId, "userId": user.UserId })
                .$promise
                .then(function (data) {
                    vm.appPoolAwake = true;

                    vm.Orders[vm.SelectedOrderIndex - 1] = data;
                    vm.SelectedOrder = data;
                })
                .catch(function (response) {
                    vm.appPoolAwake = true;
                    logger.error("", response.data.Message);
                });
        }

        function validateOrder() {

            var validRowCount = 0;

            vm.SelectedOrder.OrderItems.forEach(function (item) {

                if (item.UserBarcode != null && item.UserBarcode != "") {
                    validRowCount ++;
                }
            });

            vm.SelectedOrder.OrderValid = (vm.SelectedOrder.OrderItems.length === validRowCount);
        }

        /*** Paging Function ***/
        /////////////////////////

        function initialiseOrders() {
            vm.OrderCount = vm.Orders.length;
            vm.SelectedOrderIndex = 1;
            vm.SelectedOrder = vm.Orders[vm.SelectedOrderIndex - 1];
            vm.SelectedOrder.OrderValid = false;

            setNavigationFlags();
        }

        function gotoPreviousOrder() {
            if (vm.SelectedOrderIndex > 1) {
                vm.SelectedOrderIndex = vm.SelectedOrderIndex - 1;
            }

            vm.SelectedOrder = vm.Orders[vm.SelectedOrderIndex - 1];
            vm.SelectedOrder.OrderValid = false;

            setNavigationState();
        }

        function goToNextOrder() {
            if (vm.SelectedOrderIndex < vm.OrderCount) {
                vm.SelectedOrderIndex = vm.SelectedOrderIndex + 1;
            }

            vm.SelectedOrder = vm.Orders[vm.SelectedOrderIndex - 1];
            vm.SelectedOrder.OrderValid = false;

            setNavigationState();
        }

       function setNavigationState() {
            vm.FirstRecord = false;
            vm.LastRecord = false;
            
            if (vm.SelectedOrderIndex === 1) {
                vm.FirstRecord = true;
            }

            if (vm.SelectedOrderIndex === vm.OrderCount || vm.OrderCount === 0) {
                vm.LastRecord = true;
            }

            if (vm.LastRecord && allOrdersShipped() === true) {
                logger.success("All Orders Processed", " Redirecting to Dashboard. . .");

                $interval(redirect, 1000);
            }
        }

        function redirect() {
            vm.counter--;

            if (vm.counter === 0)
                $state.go("supplierStockPick");
        }


        /*** Printing Functions ***/
        ////////////////////////////

        //todo: test
        function printDocuments() {
            vm.Printing = true;

            var hostApiUrl = settings.GetApiPath();
            
            var printDocsUrl = hostApiUrl + vm.SelectedOrder.PrinterDownloadEndPoint;
            var printApplicationUrl = "app/ClickOnce/Supplier.PrinterServer.application" + "?url=" + printDocsUrl;
        
            //$window.open(printApplicationUrl, '_blank');
        }

        //todo: test
        function printerResult(result) {
            vm.Printing = false;

            //printed succesfully
            if (result === true) {
                goToNextOrder();
            //printing not succesful
            } else {
                getDocumentsForManualPrinting();                
            }
        }

        //todo: implement
        function getDocumentsForManualPrinting() {
            if (allOrdersShipped() === false) {
                logger.info("Additional pending orders", "Please navigate to the next order manaully once you have downloaded and printied the documents.");
            }
        }



        /*** Helper Functions ***/
        //////////////////////////

        function allOrdersShipped() {
            var result = false;

            //Check if all orders processed
            var shippedCount = 0;

            vm.Orders.forEach(function (item) {

                if (item.Shipped) {
                    shippedCount++;
                }
            });

            result = vm.Orders.length === shippedCount;

            return result;
        }

    }
})();