﻿(function () { 
    'use strict';

    angular
        .module("app.supplier")
        .controller("supplierAdminCtrl", supplierAdminCtrl);

    supplierAdminCtrl.$inject = ["$state", "$window", "settings", "logger", "auth", "dashboardResource", "pickingListResource"];

    function supplierAdminCtrl($state, $window, settings, logger, auth, dashboardResource, pickingListResource) {
        var vm = this;
        var user = null;
        
        /*** Public Variables ***/

        vm.DashboardData = {};
        vm.FileUploadData = {};
        vm.OutOfStockData = [];
        vm.IsOutOfStockCollapsed = false;

        /*** Public Functions ***/

        vm.GetOutOfStockItems = getOutOfStockItems;

        //////////////////////////////


        getUserDetails();
        
        function getUserDetails() {
            if (!auth.CheckStateForUser($state.$current.self.name)) {
                $state.go("Home");

                return;
            }
            
            user = auth.GetUser();

            getDashboardData();
            getFileUploadData();
        }


        /*** Dashboard Functions ***/
        
        function getDashboardData() {
            vm.appPoolAwake = false;

            dashboardResource.getSupplierOrders({ "userId": user.UserId })
                .$promise
                .then(function (data) {
                    vm.DashboardData = data;
                    vm.PickingList = data.PickingList;

                    vm.appPoolAwake = true;
                })
                .catch(function (response) {
                    vm.appPoolAwake = true;

                    logger.error("", response.data.Message);
                });
        };

        function getFileUploadData() {
            vm.appPoolAwake = false;

            dashboardResource.getSupplierStockData({ "userId": user.UserId })
                .$promise
                .then(function (data) {
                    vm.FileUploadData = data;

                    vm.appPoolAwake = true;
                })
                .catch(function (response) {
                    vm.appPoolAwake = true;

                    logger.error("", response.data.Message);
                });
        }

        function getOutOfStockItems() {
            if (vm.DashboardData != null && vm.DashboardData.OutOfStockOrders === 0) {
                logger.info("", "There are no out of stock orders");
                return;
            }

            vm.appPoolAwake = false;

            pickingListResource.getOutOfStockItems({ "userId": user.UserId })
                .$promise
                .then(function (data) {
                    vm.OutOfStockData = data;

                    vm.appPoolAwake = true;
                })
                .catch(function (response) {
                    vm.appPoolAwake = true;

                    logger.error("", response.data.Message);
                });
        }
    };
})();