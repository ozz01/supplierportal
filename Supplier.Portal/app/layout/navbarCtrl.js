﻿(function () {
    "use strict";

    angular
        .module("app.layout")
        .controller("navbarCtrl", navbarCtrl);

    navbarCtrl.$inject = ["$state", "logger", "auth"];

    function navbarCtrl($state, logger, auth) {
        var vm = this;
        vm.navbarExpanded = false;
        vm.user = null;

        getUserDetails();

        function getUserDetails() {

            auth.GetUser().$promise
                .then(function (data) {
                    vm.user = data;

                    //if (!vm.user.UserName) {
                    //    $state.go("Login");
                    //}
                    //vm.user.IsAdmin = (data.Roles.indexOf("Admin") >= 0);
                    //vm.user.IsSupportAdmin = (data.Roles.indexOf("SupportAdmin") >= 0);
                })
                .catch(function (response) {
                   // $state.go("Login");
                });
        }
    }
})();