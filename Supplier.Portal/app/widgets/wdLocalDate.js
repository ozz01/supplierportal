﻿(function () {
    'use strict';

    angular
        .module('app.widgets')
        .directive('wdLocalDate', localDate);

    function localDate() {
        var directive = {
            restrict: 'A',
            require: 'ngModel',
            link: link()
        };

        return directive;

        /////////////////////

        function link() {
            return function (scope, element, attr, ngModel) {
                ngModel.$parsers.push(function (viewValue) {
                    if (viewValue != undefined) {
                        viewValue.setHours(0);
                        viewValue.setMinutes(-viewValue.getTimezoneOffset());
                        return viewValue;
                    }
                    return null;
                });
            };
        }
    };
}());