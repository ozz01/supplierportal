﻿(function () {
    'use strict';
    var app = angular.module('app');

    app.directive('paginationInfo', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/shared/views/paginationInfo.html'
        };
    });
})();