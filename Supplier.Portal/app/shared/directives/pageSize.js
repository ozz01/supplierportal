﻿(function () {
    'use strict';
    var app = angular.module('app.core');

    app.directive('pageSize', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/shared/views/pageSize.html'
        };
    });
})();