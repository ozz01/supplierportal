﻿(function () {
    "use strict";

    angular
        .module("app.home")
        .controller("homeCtrl", homeCtrl);

    homeCtrl.$inject = ["$state", "logger", "userResource", "auth", "$cookies"];

    function homeCtrl($state, logger, userResource, auth, $cookies) {
        var vm = this;
      
        vm.loginModel = { Username: "", Password: ""};

        vm.submit = attemptLogin;

        function attemptLogin() {
            vm.appPoolAwake = false;
            vm.error = null;

            vm.appPoolAwake = false;

            return userResource.login(vm.loginModel)
                .$promise
                .then(function (data) {
                    vm.appPoolAwake = true;

                    vm.user = data;
                    auth.SetUser(data);
                    SetState();                          

                }).catch(function (response) {
                    logger.error("", response.data.Message);

                    vm.appPoolAwake = true;
            });
        }
   
        function SetState() {
            if (vm.user.Role.Name === "Supplier Admin Processor") {
                $state.go("supplierAdmin");
            } else if (vm.user.Role.Name === "Supplier Order Processor") {
                $state.go("supplierStockPick");
            } else if (vm.user.Role.Name === "WG Tools Administrator") {
                $state.go("wgAdmin");
            } else if (vm.user.Role.Name === "WG Supplier Support") {
                $state.go("wgSupplierSupport");
            } else if (vm.user.Role.Name === "WG Customer Support") {
                $state.go("wgCustomerSupport");
            }
        }
    }
})();