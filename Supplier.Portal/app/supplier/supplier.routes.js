﻿(function () {
    "use strict";

    angular
        .module("app.supplier")
        .run(appRun);

    appRun.$inject = ["routerHelper"];

    function appRun(routerHelper) {
        routerHelper.configureStates(getStates(), "");
    }

    function getStates() {
        return [	
            {
                state: "supplierStockPick",
                config: {
                    url: "/Supplier/StockPicking",
                    templateUrl: "app/Supplier/stockPicking/index.html",
                    controller: "stockPickingCtrl",
                    controllerAs: "vm"
                }
            },
            {
                state: "supplierOrderDespatch",
                config: {
                    url: "/Supplier/OrderDespatching",
                    templateUrl: "app/Supplier/orderShipping/index.html",
                    controller: "orderShippingCtrl",
                    controllerAs: "vm"
                }
            },
            {
                state: "supplierAdmin",
                config: {
                    url: "/Supplier/Admin",
                    templateUrl: "app/Supplier/admin/index.html",
                    controller: "supplierAdminCtrl",
                    controllerAs: "vm"
                }
            },
        ];
    }
})();