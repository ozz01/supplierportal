﻿(function () {
    "use strict";

    angular.module("app.core", [
        /* Angular Modules */
        "ui.router",
        "ngResource",
        "ngMessages",
        "ngCookies",

        /* Reusable cross app code modules */
        "blocks.exception",
        "blocks.logger",
        "blocks.router",
        'blocks.security',
        'blocks.settings',
        "app.common",

         /* 3rd Party Modules */
        //"angular-loading-bar",
        "toaster",
        "ui.bootstrap",
        "checklist-model"
        //"mgcrea.ngStrap.datepicker",
        //"mgcrea.ngStrap.tooltip",
        //"mgcrea.ngStrap.popover"
    ]);
})();