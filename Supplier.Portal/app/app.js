﻿(function () {
    "use strict";

    var app = angular.module("app", [
        "app.core",
        "app.widgets",
        "app.data",
        "app.home",                 // wg application module
        "app.layout",               // wg application module
        "app.admin",                // wg application module      
        "app.supplier",             // wg application module
        "app.wgAdmin",              // wg application module
        "app.wgSupplierSupport",    // wg application module
        "app.wgCustomerSupport",    // wg application module
    ]);

    var home = {
        url: "/",
        templateUrl: "app/home/Login.html",
        controller: "homeCtrl",
        controllerAs: "vm",
        //resolve: {
        //    app: [
        //        'auth', function (auth) {
        //            auth.IsAuthorized('User');
        //        }
        //    ]
        //}
    };


    app.config([
        "$stateProvider", "$urlRouterProvider", "$provide", function ($stateProvider, $urlRouterProvider, $provide) {

            $stateProvider.state("Home", home);

            $urlRouterProvider.otherwise("/");

        }]);

    app.run(function ($rootScope, $location) {
        //$rootScope.param = "Test"
    });
})();