﻿(function () { 
    'use strict';

    angular
       .module("app.wgAdmin").controller("wgAdminCtrl", wgAdminCtrl);

    wgAdminCtrl.$inject = ["$state", "auth", "logger", "dashboardResource"];

    function wgAdminCtrl($state, auth, logger, dashboardResource) {
        var vm = this;
        vm.processing = false;
        vm.transactionData = null;
        vm.xmlContent = null;
        vm.xmlTitle = "";
  
        vm.emailFormat = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;

        vm.LookupById = lookupTransactionById;
        vm.LookupByEmail = lookupTransactionByEmail;
        vm.LookupByMertexRef = lookupTransactionByMertexRef;
        vm.ShowXml = showXml;

        //////////////////////////

        checkUserDetails();

        function checkUserDetails() {
            //auth.GetUser();
           
            if (!auth.CheckStateForUser($state.$current.self.name)) {
                $state.go("Home");

                return;
            }
        }

        function lookupTransactionById() {
            vm.transactionData = null;
            vm.xmlContent = null;
            vm.processing = true;

            dashboardResource.get({ "transactionId": vm.transactionId })
                .$promise
                .then(function (data) {
                    if (data.TransactionId) {
                        vm.transactionData = data;
                    } else {
                        logger.info("", "No transaction found for transaction id: " + vm.transactionId);
                    }
                    vm.processing = false;
                })
                .catch(function (response) {
                    vm.processing = false;
                    logger.error("", response.data.Message);
                });
        };
        
        function lookupTransactionByEmail() {
            vm.transactionData = null;
            vm.xmlContent = null;
            vm.processing = true;

            dashboardResource.get({ "email": vm.email })
                .$promise
                .then(function (data) {
                    if (data.TransactionId) {
                        vm.transactionData = data;
                    } else {
                        logger.info("", "No transaction found for email address: " + vm.email);
                    }

                    vm.processing = false;
                })
                .catch(function (response) {
                    logger.error("", response.data.Message);
                    vm.processing = false;
                });
        };
        
        function lookupTransactionByMertexRef() {
            vm.transactionData = null;
            vm.xmlContent = null;
            vm.processing = true;

            dashboardResource.get({ "reference": vm.mertexRef })
                .$promise
                .then(function (data) {
                    if (data.TransactionId) {
                        vm.transactionData = data;
                    } else {
                        logger.info("", "No transaction found for Mertex reference: " + vm.mertexRef);
                    }

                    vm.processing = false;
                })
                .catch(function (response) {
                    logger.error("", response.data.Message);
                    vm.processing = false;
                });
        };

        function showXml(data, title) {
            vm.xmlContent = data;
            vm.xmlTitle = title;
        }
    };
})();