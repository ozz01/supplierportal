﻿(function () {
    "use strict";

    angular
        .module("app.wgAdmin")
        .run(appRun);

    appRun.$inject = ["routerHelper"];

    function appRun(routerHelper) {
        routerHelper.configureStates(getStates(), "");
    }

    function getStates() {
        return [
			{
			    state: "wgAdmin",
			    config: {
			        url: "/WG/Admin",
			        templateUrl: "app/WG/admin/index.html",
			        controller: "wgAdminCtrl",
			        controllerAs: "vm"
			    }
			}
        ];
    }
})();