﻿(function () { 
    'use strict';

    angular
       .module("app.wgSupplierSupport").controller("wgSupplierSupportCtrl", wgSupplierSupportCtrl);

    wgSupplierSupportCtrl.$inject = ["$state", "auth", "logger", "dashboardResource"];

    function wgSupplierSupportCtrl($state, auth, logger, dashboardResource) {
        var vm = this;
        vm.processing = false;
        vm.transactionData = null;
 
        //////////////////////////

        checkUserDetails();

        function checkUserDetails() {
            //auth.GetUser();
           
            if (!auth.CheckStateForUser($state.$current.self.name)) {
                $state.go("Home");

                return;
            }

            getDashboardData();
        }

        function getDashboardData() {
            vm.appPoolAwake = false;

            dashboardResource.get()
                .$promise
                .then(function (data) {
                    vm.data = data;
                    vm.appPoolAwake = true;

                })
                .catch(function (response) {
                    vm.appPoolAwake = true;

                    logger.error("", response.data.Message);
                });
        };
    };
})();