﻿(function () {
    "use strict";

    angular
        .module("app.wgSupplierSupport")
        .run(appRun);

    appRun.$inject = ["routerHelper"];

    function appRun(routerHelper) {
        routerHelper.configureStates(getStates(), "");
    }

    function getStates() {
        return [
			{
			    state: "wgSupplierSupport",
			    config: {
			        url: "/WG/SupplierSupport",
			        templateUrl: "app/WG/supplierSupport/index.html",
			        controller: "wgSupplierSupportCtrl",
			        controllerAs: "vm"
			    }
			}
        ];
    }
})();