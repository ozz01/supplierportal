﻿(function () {
    "use strict";

    angular
        .module("app.wgCustomerSupport")
        .run(appRun);

    appRun.$inject = ["routerHelper"];

    function appRun(routerHelper) {
        routerHelper.configureStates(getStates(), "");
    }

    function getStates() {
        return [
			{
			    state: "wgCustomerSupport",
			    config: {
			        url: "/WG/CustomerSupport",
			        templateUrl: "app/WG/customerSupport/index.html",
			        controller: "wgCustomerSupportCtrl",
			        controllerAs: "vm"
			    }
			}
        ];
    }
})();