﻿(function () { 
    'use strict';

    angular
       .module("app.wgCustomerSupport").controller("wgCustomerSupportCtrl", wgCustomerSupportCtrl);

    wgCustomerSupportCtrl.$inject = ["$state", "auth", "logger", "dashboardResource"];

    function wgCustomerSupportCtrl($state, auth, logger, dashboardResource) {
        var vm = this;
      
        //////////////////////////

        checkUserDetails();

        function checkUserDetails() {
            //auth.GetUser();
           
            if (!auth.CheckStateForUser($state.$current.self.name)) {
                $state.go("Home");

                return;
            }

            getDashboardData();
        }

        function getDashboardData() {
            vm.appPoolAwake = false;

            dashboardResource.get()
                .$promise
                .then(function (data) {
                    vm.data = data;
                    vm.appPoolAwake = true;

                })
                .catch(function (response) {
                    vm.appPoolAwake = true;

                    logger.error("", response.data.Message);
                });
        };
    };
})();