﻿(function () {
    'use strict';
    var app = angular.module('app.common');

    app.controller('dateController', ['$scope', function ($scope) {
        $scope.open = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.opened = true;
        };

        $scope.dateOptions = {
            startingDay: 1
        };

        $scope.monthYearDateOptions = {
            datepickerMode: "'month'",
            minMode: 'month',
            maxMode: 'year'
        };

        $scope.yearOptions = {
            datepickerMode: "'year'",
            minMode: 'year',
            maxMode: 'year'
        };

        // Disable weekend selection
        $scope.disableWeekend = function (date, mode) {
            return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
        };

        $scope.thisYear = new Date().getFullYear();
        $scope.minDate = new Date($scope.thisYear, 0, 1);
        $scope.nextYearDate = new Date($scope.thisYear +1, 0, 1);

        $scope.monthPopupMode = 'month';

        $scope.format = 'd/M/y';
        $scope.dayMonthFormat = 'dd MMM';
        $scope.standardFormat = 'dd/MM/yyyy';
        $scope.yearFormat = 'yyyy';
    }]);
})();