﻿(function () {
    "use strict";

    angular
        .module("app.common")
        .factory("dateHelper", dateHelper);

    function dateHelper() {
        var service = {
            getDateLastMonth: getDateLastMonth,
            getDateYesterday: getDateYesterday
        };

        return service;

        //////////////

        function getDateYesterday() {
            var today = new Date();
            var todayLess1 = new Date().setDate(today.getDate() - 1);
            return new Date(todayLess1);
        }

        function getDateLastMonth() {
            var date = new Date();
            var year = date.getFullYear();
            var month = date.getMonth() - 5; // minus the date
            var day = date.getDay();

            var nd = new Date(year, month, 1);

            return nd;
        }
    }
})();