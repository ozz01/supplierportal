﻿(function () {
    "use strict";

    angular
        .module("app.common")
        .factory("arrayHelper", arrayHelper);

    function arrayHelper() {
        var service = {
            sum: sum,
            nominalFromTitle: nominalFromTitle
        };

        return service;

        //////////////

        function nominalFromTitle(array, columnValue) {
            for (var i = 0; i < array.length; i++) {
                if (array[i].NominalTitle === columnValue) return array[i].Nominal;
            }
            return null;
        }

        function sum(array, propertyName) {
            var total = 0;

            for (var i = 0; i < array.length; i++) {
                var currentValue = getValue(array, i, propertyName);

                if (currentValue != null && currentValue !== undefined && !isNaN(currentValue)) {
                    total += array[i][propertyName];
                }
            }

            return total;
        }

        function getValue(array, index, propertyName) {
            if (propertyName !== undefined) {
                return array[index][propertyName];
            }

            return array[index];
        }
    }
})();