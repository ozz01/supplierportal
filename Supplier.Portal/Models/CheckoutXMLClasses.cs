﻿using System.Xml.Serialization;

namespace Supplier.Portal.Models
{
    [System.SerializableAttribute()]
    [XmlRoot("Checkout", Namespace = "", IsNullable = false)]
    public partial class CheckoutType
    {
        //private BasketType basketField;

        //private itemType[] addressesField;

        //private string checkoutStageField;

        //private itemType[] additionalInfoField;

        //private TransactionDeliveriesType transactionDeliveriesField;

        //private string acceptTermsAndConditionsField;

        //public BasketType Basket
        //{
        //    get { return this.basketField; }
        //    set { this.basketField = value; }
        //}

        [XmlArrayItem("item", IsNullable = false)]
        public itemType[] Addresses { get; set; }

        public PersonType Person { get; set; }
       
        //public string CheckoutStage
        //{
        //    get { return this.checkoutStageField; }
        //    set { this.checkoutStageField = value; }
        //}

        //[XmlArrayItem("item", IsNullable = false)]
        //public itemType[] AdditionalInfo
        //{
        //    get { return this.additionalInfoField; }
        //    set { this.additionalInfoField = value; }
        //}

        //public TransactionDeliveriesType TransactionDeliveries
        //{
        //    get { return this.transactionDeliveriesField; }
        //    set { this.transactionDeliveriesField = value; }
        //}

        //public string AcceptTermsAndConditions
        //{
        //    get { return this.acceptTermsAndConditionsField; }
        //    set { this.acceptTermsAndConditionsField = value; }
        //}
    }

    [System.SerializableAttribute()]
    public partial class BasketType
    {
        private int idField;

        private string siteIdField;

        private string languageIdField;

        private int transactionIdField;

        private string isCompleteField;

        private string cookieGuidField;

        private string userAccountIdField;

        private int trackedVisitorIdField;

        private string lastTrackedField;

        private string lastEmailedField;

        private sbyte emailsSentField;

        private string hasChangedField;

        private BasketLineType[] linesField;

        public int Id
        {
            get { return this.idField; }
            set { this.idField = value; }
        }

        [XmlElement(IsNullable = true)]
        public string SiteId
        {
            get { return this.siteIdField; }
            set { this.siteIdField = value; }
        }

        [XmlElement(IsNullable = true)]
        public string LanguageId
        {
            get { return this.languageIdField; }
            set { this.languageIdField = value; }
        }

        public int TransactionId
        {
            get { return this.transactionIdField; }
            set { this.transactionIdField = value; }
        }

        public string IsComplete
        {
            get { return this.isCompleteField; }
            set { this.isCompleteField = value; }
        }

        public string CookieGuid
        {
            get { return this.cookieGuidField; }
            set { this.cookieGuidField = value; }
        }

        [XmlElement(IsNullable = true)]
        public string UserAccountId
        {
            get { return this.userAccountIdField; }
            set { this.userAccountIdField = value; }
        }

        public int TrackedVisitorId
        {
            get { return this.trackedVisitorIdField; }
            set { this.trackedVisitorIdField = value; }
        }

        [XmlElement(IsNullable = true)]
        public string LastTracked
        {
            get { return this.lastTrackedField; }
            set { this.lastTrackedField = value; }
        }

        [XmlElement(IsNullable = true)]
        public string LastEmailed
        {
            get { return this.lastEmailedField; }
            set { this.lastEmailedField = value; }
        }

        public sbyte EmailsSent
        {
            get { return this.emailsSentField; }
            set { this.emailsSentField = value; }
        }

        public string HasChanged
        {
            get { return this.hasChangedField; }
            set { this.hasChangedField = value; }
        }

        [XmlArrayItem("BasketLine", IsNullable = false)]
        public BasketLineType[] Lines
        {
            get { return this.linesField; }
            set { this.linesField = value; }
        }
    }

    [System.SerializableAttribute()]
    public partial class BasketLineType
    {
        private int idField;

        private string siteIdField;

        private string languageIdField;

        private int basketIdField;

        private int skuIdField;

        private string skuBundleIdField;

        private string skuGroupIdField;

        private sbyte quantityField;

        private float priceExField;

        private float priceIncField;

        private float vatField;

        private float totalIncField;

        private float totalExField;

        private float totalVatField;

        private int transactionDeliveryIdField;

        private sbyte shopIdField;

        private string shopBaseUriField;

        public int Id
        {
            get { return this.idField; }
            set { this.idField = value; }
        }

        [XmlElement(IsNullable = true)]
        public string SiteId
        {
            get { return this.siteIdField; }
            set { this.siteIdField = value; }
        }

        [XmlElement(IsNullable = true)]
        public string LanguageId
        {
            get { return this.languageIdField; }
            set { this.languageIdField = value; }
        }

        public int BasketId
        {
            get { return this.basketIdField; }
            set { this.basketIdField = value; }
        }

        public int SkuId
        {
            get { return this.skuIdField; }
            set { this.skuIdField = value; }
        }

        [XmlElement(IsNullable = true)]
        public string SkuBundleId
        {
            get { return this.skuBundleIdField; }
            set { this.skuBundleIdField = value; }
        }

        [XmlElement(IsNullable = true)]
        public string SkuGroupId
        {
            get { return this.skuGroupIdField; }
            set { this.skuGroupIdField = value; }
        }

        public sbyte Quantity
        {
            get { return this.quantityField; }
            set { this.quantityField = value; }
        }

         public float PriceEx
        {
            get { return this.priceExField; }
            set { this.priceExField = value; }
        }

        public float PriceInc
        {
            get { return this.priceIncField; }
            set { this.priceIncField = value; }
        }

        public float Vat
        {
            get { return this.vatField; }
            set { this.vatField = value; }
        }

        public float TotalInc
        {
            get { return this.totalIncField; }
            set { this.totalIncField = value; }
        }

        public float TotalEx
        {
            get { return this.totalExField; }
            set { this.totalExField = value; }
        }

        public float TotalVat
        {
            get { return this.totalVatField; }
            set { this.totalVatField = value; }
        }

        public int TransactionDeliveryId
        {
            get { return this.transactionDeliveryIdField; }
            set { this.transactionDeliveryIdField = value; }
        }

        public sbyte ShopId
        {
            get { return this.shopIdField; }
            set { this.shopIdField = value; }
        }

        [XmlElement(DataType = "anyURI")]
        public string ShopBaseUri
        {
            get { return this.shopBaseUriField; }
            set { this.shopBaseUriField = value; }
        }
    }

    [System.SerializableAttribute()]
    public partial class CurrencyType
    {
        private sbyte idField;

        private string nameField;

        private string siteIdField;

        private string languageIdField;

        private short lCIDField;

        private float exchangeRateField;

        private string codeField;

        public sbyte Id
        {
            get { return this.idField; }
            set { this.idField = value; }
        }

        public string Name
        {
            get { return this.nameField; }
            set { this.nameField = value; }
        }

        [XmlElement(IsNullable = true)]
        public string SiteId
        {
            get { return this.siteIdField; }
            set { this.siteIdField = value; }
        }

        [XmlElement(IsNullable = true)]
        public string LanguageId
        {
            get { return this.languageIdField; }
            set { this.languageIdField = value; }
        }

        public short LCID
        {
            get { return this.lCIDField; }
            set { this.lCIDField = value; }
        }

        public float ExchangeRate
        {
            get { return this.exchangeRateField; }
            set { this.exchangeRateField = value; }
        }

        public string Code
        {
            get { return this.codeField; }
            set { this.codeField = value; }
        }
    }

    [System.SerializableAttribute()]
    public partial class DeliveryOptionType
    {
        private sbyte idField;

        private string nameField;

        private string siteIdField;

        private string languageIdField;

        private string codeField;

        private string cutOffDayField;

        private sbyte orderField;

        public sbyte Id
        {
            get { return this.idField; }
            set { this.idField = value; }
        }

        public string Name
        {
            get { return this.nameField; }
            set { this.nameField = value; }
        }

        [XmlElement(IsNullable = true)]
        public string SiteId
        {
            get { return this.siteIdField; }
            set { this.siteIdField = value; }
        }

        [XmlElement(IsNullable = true)]
        public string LanguageId
        {
            get { return this.languageIdField; }
            set { this.languageIdField = value; }
        }

        public string Code
        {
            get { return this.codeField; }
            set { this.codeField = value; }
        }

        [XmlElement(IsNullable = true)]
        public string CutOffDay
        {
            get { return this.cutOffDayField; }
            set { this.cutOffDayField = value; }
        }

        public sbyte Order
        {
            get { return this.orderField; }
            set { this.orderField = value; }
        }
    }

    [System.SerializableAttribute()]
    public partial class DeliveryProductChargeType
    {
        private sbyte idField;

        private string siteIdField;

        private string languageIdField;

        private sbyte countryIdField;

        private sbyte currencyIdField;

        private sbyte deliveryOptionIdField;

        private DeliveryOptionType deliveryOptionField;

        private string productTypeField;

        private string priceExTaxField;

        private string priceIncTaxField;

        private string haulierCodeField;

        private string carriageCodeField;

        private string splitDelieryChargeAppliedField;

        private string freeDeliveryAppliedField;

        private float minumumSpendForFreeDeliveryField;

        public sbyte Id
        {
            get { return this.idField; }
            set { this.idField = value; }
        }

        [XmlElement(IsNullable = true)]
        public string SiteId
        {
            get { return this.siteIdField; }
            set { this.siteIdField = value; }
        }

        [XmlElement(IsNullable = true)]
        public string LanguageId
        {
            get { return this.languageIdField; }
            set { this.languageIdField = value; }
        }

        public sbyte CountryId
        {
            get { return this.countryIdField; }
            set { this.countryIdField = value; }
        }

        public sbyte CurrencyId
        {
            get { return this.currencyIdField; }
            set { this.currencyIdField = value; }
        }

        public sbyte DeliveryOptionId
        {
            get { return this.deliveryOptionIdField; }
            set { this.deliveryOptionIdField = value; }
        }

        public DeliveryOptionType DeliveryOption
        {
            get { return this.deliveryOptionField; }
            set { this.deliveryOptionField = value; }
        }

        public string ProductType
        {
            get { return this.productTypeField; }
            set { this.productTypeField = value; }
        }

        [XmlElement(IsNullable = true)]
        public string PriceExTax
        {
            get { return this.priceExTaxField; }
            set { this.priceExTaxField = value; }
        }

        [XmlElement(IsNullable = true)]
        public string PriceIncTax
        {
            get { return this.priceIncTaxField; }
            set { this.priceIncTaxField = value; }
        }

        public string HaulierCode
        {
            get { return this.haulierCodeField; }
            set { this.haulierCodeField = value; }
        }

        public string CarriageCode
        {
            get { return this.carriageCodeField; }
            set { this.carriageCodeField = value; }
        }

        public string SplitDelieryChargeApplied
        {
            get { return this.splitDelieryChargeAppliedField; }
            set { this.splitDelieryChargeAppliedField = value; }
        }

        public string FreeDeliveryApplied
        {
            get { return this.freeDeliveryAppliedField; }
            set { this.freeDeliveryAppliedField = value; }
        }

        public float MinumumSpendForFreeDelivery
        {
            get { return this.minumumSpendForFreeDeliveryField; }
            set { this.minumumSpendForFreeDeliveryField = value; }
        }
    }

    [System.SerializableAttribute()]
    public partial class TransactionDeliveryType
    {
        private int idField;

        private string siteIdField;

        private string languageIdField;

        private int transactionIdField;

        private string orderReferenceField;

        private sbyte deliveryProductChargeIdField;

        private string productTypeField;

        private DeliveryProductChargeType deliveryProductChargeField;

        private sbyte currencyIdField;

        private CurrencyType currencyField;

        private float priceIncTaxField;

        private float priceExTaxField;

        private sbyte transactionStatusIdField;

        public int Id
        {
            get { return this.idField; }
            set { this.idField = value; }
        }

        [XmlElement(IsNullable = true)]
        public string SiteId
        {
            get { return this.siteIdField; }
            set { this.siteIdField = value; }
        }

        [XmlElement(IsNullable = true)]
        public string LanguageId
        {
            get { return this.languageIdField; }
            set { this.languageIdField = value; }
        }

        public int TransactionId
        {
            get { return this.transactionIdField; }
            set { this.transactionIdField = value; }
        }

        public string OrderReference
        {
            get { return this.orderReferenceField; }
            set { this.orderReferenceField = value; }
        }

        public sbyte DeliveryProductChargeId
        {
            get { return this.deliveryProductChargeIdField; }
            set { this.deliveryProductChargeIdField = value; }
        }

        public string ProductType
        {
            get { return this.productTypeField; }
            set { this.productTypeField = value; }
        }

        public DeliveryProductChargeType DeliveryProductCharge
        {
            get { return this.deliveryProductChargeField; }
            set { this.deliveryProductChargeField = value; }
        }

        public sbyte CurrencyId
        {
            get { return this.currencyIdField; }
            set { this.currencyIdField = value; }
        }

        public CurrencyType Currency
        {
            get { return this.currencyField; }
            set { this.currencyField = value; }
        }

        public float PriceIncTax
        {
            get { return this.priceIncTaxField; }
            set { this.priceIncTaxField = value; }
        }

        public float PriceExTax
        {
            get { return this.priceExTaxField; }
            set { this.priceExTaxField = value; }
        }

        public sbyte TransactionStatusId
        {
            get { return this.transactionStatusIdField; }
            set { this.transactionStatusIdField = value; }
        }
    }

    [System.SerializableAttribute()]
    public partial class TransactionDeliveriesType
    {
        private TransactionDeliveryType transactionDeliveryField;

        public TransactionDeliveryType TransactionDelivery
        {
            get { return this.transactionDeliveryField; }
            set { this.transactionDeliveryField = value; }
        }
    }

    [System.SerializableAttribute()]
    public partial class PersonType
    {
        public int Id { get; set; }

        //[XmlElement(IsNullable = true)]
        //public string SiteId { get; set; }

        //[XmlElement(IsNullable = true)]
        //public string LanguageI { get; set; }

        //public string AllowMarketing { get; set; }

        //public string AllowThirdParty { get; set; }

        //public string Newsletter { get; set; }

        //[XmlElement(IsNullable = true)]
        //public string DateOfBirth { get; set; }

        public string Title { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        //public string Gender { get; set; }

        //[XmlElement(IsNullable = true)]
        //public string PrimaryAddressId { get; set; }

        [XmlElement(IsNullable = true)]
        public string PrimaryTelephoneId { get; set; }

        public string EmailAddress { get; set; }

        //[XmlElement(IsNullable = true)]
        //public string UserAccountId { get; set; }

        //public string PreferredContactMethod { get; set; }
    }

    [System.SerializableAttribute()]
    public partial class AddressType
    {
        public sbyte Id { get; set; }

        //[XmlElement(IsNullable = true)]
        //public string SiteId { get; set; }

        //[XmlElement(IsNullable = true)]
        //public string LanguageId { get; set; }

        public int PersonId {get; set;}

        //public string Description { get; set; }

        //public string IsPrimary { get; set; }

        public string Title { get; set; }

        public string FirstName { get; set; }
        
        public string LastName { get; set; }

        public string Line1 { get; set; }

        public string Line2 { get; set; }

        public string Line3 { get; set; }
        
        public string Line4 { get; set; }

        public string Town { get; set; }

        public string County { get; set; }

        public string Postcode { get; set; }
      
        public long CountryId { get; set; }      
    }

    [System.SerializableAttribute()]
    public partial class valueType
    {
        public AddressType Address { get; set; }

        public string anyType { get; set; }
    }

    [System.SerializableAttribute()]
    public partial class keyType
    {
        public string AddressType { get; set; }

        public string @string { get; set; }
    }

    [System.SerializableAttribute()]
    public partial class itemType
    {
        public keyType key { get; set; }

        public valueType value { get; set; }
    }
}