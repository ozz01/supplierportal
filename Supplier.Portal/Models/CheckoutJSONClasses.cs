﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Supplier.Portal.Models
{  
    public class TransactionCheckout
    {
        [JsonProperty("Checkout")]
        public Checkout Checkout { get; set; }
    }

    public class Checkout
    {
        public string AcceptTermsAndConditions { get; set; }

        [JsonProperty("Addresses")]
        public CheckoutAddresses Addresses { get; set; }

        [JsonProperty("Person")]
        public CheckoutPerson Person { get; set; }
    }

    public class CheckoutAddresses
    {
        public List<CheckoutAddressItem> Item { get; set; }
    }

    public class CheckoutAddressItem
    {
        public CheckoutAddressKey Key { get; set; }

        public CheckoutAddressValue Value { get; set; }
    }

    public class CheckoutAddressKey
    {
        public string AddressType { get; set; }
    }

    public class CheckoutAddressValue
    {
        public CheckoutAddress Address { get; set; }
    }

    public class CheckoutAddress
    {
        public string Title { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Line1 { get; set; }

        public string Line2 { get; set; }

        public string Town { get; set; }

        public string County { get; set; }

        public string Postcode { get; set; }

        public string CountryId { get; set; }
    }

    public class CheckoutPerson
    {
        //convet to long before using 
        public int Id { get; set; }

        public string Title { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string EmailAddress { get; set; }

        //convet to long before using 
        public string PrimaryTelephoneId { get; set; }
    }
}