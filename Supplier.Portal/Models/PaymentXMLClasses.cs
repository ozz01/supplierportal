﻿using System.Xml.Serialization;

namespace Supplier.Portal.Models
{
    [System.SerializableAttribute()]
    [XmlRoot("paymentXml", Namespace="", IsNullable=false)]
    public partial class PaymentType {       
        public string PaymentReference { get; set; }
    
        //public PaymentInformationType PaymentInformation { get; set; }
    
        public MertexResponseType MertexResponse { get; set; }
    }
    
    [System.SerializableAttribute()]
    public partial class PaymentInformationType {    
        public ResponseType Response { get; set; }
    }

    [System.SerializableAttribute()]
    public partial class ResponseType {
        public PayPalTxnType PayPalTxn { get; set; }
    
        public string datacash_reference { get; set; }
    
        public string merchantreference { get; set; }
    
        public string mode { get; set; }

        public string reason { get; set; }

        public string status { get; set; }
    
        public string time { get; set; }
    }

    [System.SerializableAttribute()]
    public partial class PayPalTxnType {
    
        private string ackField;
    
        private string amtField;
    
        private string buildField;
    
        private string correlationidField;
    
        private string currencycodeField;
    
        private string feeamtField;
    
        private string insuranceoptionselectedField;
    
        private string ordertimeField;
    
        private string paymentstatusField;
    
        private string paymenttypeField;
    
        private string pendingreasonField;
    
        private string protectioneligibilityField;
    
        private string reasoncodeField;
    
        private string shippingoptionisdefaultField;
    
        private string taxamtField;
    
        private string timestampField;
    
        private string tokenField;
    
        private string transactionidField;
    
        private string transactiontypeField;
    
        private string versionField;
    
        /// <remarks/>
        public string ack {
            get {
                return this.ackField;
            }
            set {
                this.ackField = value;
            }
        }
    
        /// <remarks/>
        public string amt {
            get {
                return this.amtField;
            }
            set {
                this.amtField = value;
            }
        }
    
        /// <remarks/>
        public string build {
            get {
                return this.buildField;
            }
            set {
                this.buildField = value;
            }
        }
    
        /// <remarks/>
        public string correlationid {
            get {
                return this.correlationidField;
            }
            set {
                this.correlationidField = value;
            }
        }
    
        /// <remarks/>
        public string currencycode {
            get {
                return this.currencycodeField;
            }
            set {
                this.currencycodeField = value;
            }
        }
    
        /// <remarks/>
        public string feeamt {
            get {
                return this.feeamtField;
            }
            set {
                this.feeamtField = value;
            }
        }
    
        /// <remarks/>
        public string insuranceoptionselected {
            get {
                return this.insuranceoptionselectedField;
            }
            set {
                this.insuranceoptionselectedField = value;
            }
        }
    
        /// <remarks/>
        public string ordertime {
            get {
                return this.ordertimeField;
            }
            set {
                this.ordertimeField = value;
            }
        }
    
        /// <remarks/>
        public string paymentstatus {
            get {
                return this.paymentstatusField;
            }
            set {
                this.paymentstatusField = value;
            }
        }
    
        /// <remarks/>
        public string paymenttype {
            get {
                return this.paymenttypeField;
            }
            set {
                this.paymenttypeField = value;
            }
        }
    
        /// <remarks/>
        public string pendingreason {
            get {
                return this.pendingreasonField;
            }
            set {
                this.pendingreasonField = value;
            }
        }
    
        /// <remarks/>
        public string protectioneligibility {
            get {
                return this.protectioneligibilityField;
            }
            set {
                this.protectioneligibilityField = value;
            }
        }
    
        /// <remarks/>
        public string reasoncode {
            get {
                return this.reasoncodeField;
            }
            set {
                this.reasoncodeField = value;
            }
        }
    
        /// <remarks/>
        public string shippingoptionisdefault {
            get {
                return this.shippingoptionisdefaultField;
            }
            set {
                this.shippingoptionisdefaultField = value;
            }
        }
    
        /// <remarks/>
        public string taxamt {
            get {
                return this.taxamtField;
            }
            set {
                this.taxamtField = value;
            }
        }
    
        /// <remarks/>
        public string timestamp {
            get {
                return this.timestampField;
            }
            set {
                this.timestampField = value;
            }
        }
    
        /// <remarks/>
        public string token {
            get {
                return this.tokenField;
            }
            set {
                this.tokenField = value;
            }
        }
    
        /// <remarks/>
        public string transactionid {
            get {
                return this.transactionidField;
            }
            set {
                this.transactionidField = value;
            }
        }
    
        /// <remarks/>
        public string transactiontype {
            get {
                return this.transactiontypeField;
            }
            set {
                this.transactiontypeField = value;
            }
        }
    
        /// <remarks/>
        public string version {
            get {
                return this.versionField;
            }
            set {
                this.versionField = value;
            }
        }
    }

    [System.SerializableAttribute()]
    public partial class OrderType {
        [XmlAttribute()]
        public string Number { get; set; }

        [XmlAttribute()]
        public string Account { get; set; }    

        [XmlAttribute()]
        public string Type {get;set;}

        //[XmlAttribute()]
        //public string Brand { get; set; }
    }

    [System.SerializableAttribute()]
    public partial class OrdersType {
      
        public OrderType Order {get; set; }
    }

    [System.SerializableAttribute()]

    public partial class ResultsType {
        public string Header { get; set; }
    
        public string Detail { get; set; }
    
        public string Total { get; set; }
    }

    [System.SerializableAttribute()]
    public partial class MertexResponseType {
        public ResultsType Results { get; set; }
    
        public OrdersType Orders { get; set; }
    }
}