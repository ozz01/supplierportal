﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Supplier.Portal.EntityServices;

namespace Supplier.Portal.Controllers
{
    public class AngularController : Controller
    {
        private OrderShipping _orderShipping;

        // GET: Angular
        public async Task<ViewResult> Index()
        {
            //_orderShipping = new OrderShipping();

            //Tested: LabelRecovery
            //string orderNumber = "";
            //string _imageType = UpsConstants.TestTrackingNumber_Pdf;
            //LabelResponseContainer labelResponseData = new LabelResponseContainer();
            //bool result = _orderShipping.DownloadShippingLabels(orderNumber, _imageType, out labelResponseData);


            //Tested: Void Shipment
            //string shipmentIdNumber = UpsConstants.ShipmentIdNumber;
            //string transactionReference = "TDL Ltd";
            //_orderShipping.CancelShipment(shipmentIdNumber, transactionReference);


            //Tested: 
            //string orderNumber = UpsConstants.TestTrackingNumber_Html;         
            //UpsResponse.PrimaryErrorCode errorData2 = new UpsResponse.PrimaryErrorCode();
            //_orderShipping.ShipOrder(orderNumber, out errorData2);

            return View();
        }
        
        //public ActionResult Settings()
        //{
        //    var settings = new SettingsDto
        //    {
        //        WebHostName = string.Format("{0}/{1}", Request.UrlReferrer != null ? Request.UrlReferrer.AbsoluteUri : "", "api")
        //    };

        //    var serializerSettings = new JsonSerializerSettings
        //    {
        //        ContractResolver = new CamelCasePropertyNamesContractResolver()
        //    };

        //    string settingsJson = JsonConvert.SerializeObject(settings, Formatting.Indented, serializerSettings);

        //    var settingsViewModel = new SettingsViewModel
        //    {
        //        SettingsJson = settingsJson,
        //        AngularModuleName = "app.settings"
        //    };

        //    Response.ContentType = "text/javascript";

        //    return View(settingsViewModel);
        //}   
    }
}