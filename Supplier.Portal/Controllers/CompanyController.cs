﻿using System.Web.Mvc;
using Supplier.Portal.EntityServices;

namespace Supplier.Portal.Controllers
{
    public class CompanyController : Controller
    {
        private Company _company;

        public CompanyController()
        {
            _company = new Company();
        }

        // GET: Company
        public ActionResult Index()
        {
            var companies = _company.All();

            return View(companies);
        }
    }
}