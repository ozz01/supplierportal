﻿using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Supplier.Portal.Controllers
{
    public class ConfigController : Controller
    {
        public const string ConfigAngularModuleName = "app.settings";

        public ActionResult Index()
        {
            var settings = new SettingsDto
            {
                WebHostName = Request.UserHostName
            };

            var serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            string settingsJson = JsonConvert.SerializeObject(settings, Formatting.Indented, serializerSettings);

            var settingsViewModel = new SettingsViewModel
            {
                SettingsJson = settingsJson,
                AngularModuleName = ConfigAngularModuleName
            };

            Response.ContentType = "text/javascript";

            return View(settingsViewModel);
        }       
    }

    public class SettingsDto
    {
        public string WebHostName { get; set; }
    }

    public class SettingsViewModel
    {
        public string SettingsJson { get; set; }

        public string AngularModuleName { get; set; }
    }
}