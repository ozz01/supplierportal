﻿using System.Web.Mvc;
using Supplier.Portal.EntityServices;

namespace Supplier.Portal.Controllers
{
    public class RoleController : Controller
    {
        private Role _role;

        public RoleController()
        {
            _role = new Role();
        }

        // GET: Role
        public ActionResult Index()
        {
            var roles = _role.All();

            return View(roles);
        }

        // GET: Role/Details/5
        //public ActionResult Details(int id)
        //{
        //    return View();
        //}

        // GET: Role/Create
        //public ActionResult Create()
        //{          
        //   return View();
        //}

        // POST: Role/Create
        //[HttpPost]
        //public ActionResult Create(FormCollection collection)
        //{
        //    try
        //    {
        //        RoleViewModel roleModel = new RoleViewModel { Name = "SupplierAdmin" };
        //        _role.Add(roleModel);
               

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        // GET: Role/Edit/5
        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        // POST: Role/Edit/5
        //[HttpPost]
        //public ActionResult Edit(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add update logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        // GET: Role/Delete/5
        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        // POST: Role/Delete/5
        //[HttpPost]
        //public ActionResult Delete(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add delete logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}
