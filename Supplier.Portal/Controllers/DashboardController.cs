﻿using System.Web.Mvc;
using Supplier.Portal.Services;
using Supplier.Portal.ViewModels.Dashboad;

namespace Supplier.Portal.Controllers
{
    public class DashboardController : Controller
    {
        private WebOrderDashboard _webOrderDashboard;

        public DashboardController()
        {
            _webOrderDashboard = new WebOrderDashboard();
           
        }

        // GET: Dashboard
        public ActionResult Index()
        {
            DashboardViewModel model = _webOrderDashboard.MainDashbaordData();

            return View(model);
        }
    }
}