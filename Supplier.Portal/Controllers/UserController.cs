﻿using System.Linq;
using System.Web.Mvc;
using Supplier.Portal.EntityServices;
using Supplier.Portal.ViewModels.User;

namespace Supplier.Portal.Controllers
{
    public class UserController : Controller
    {
        private User _user;

        public UserController()
        {
            _user = new User();
        }

        public ViewResult Users()
        {
            var users = _user.All();

            return View(users.AsEnumerable());
        }
        
        public ActionResult Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                LoggedInUserViewModel userDetails = new LoggedInUserViewModel();

                bool validUser = _user.Login(model, ref userDetails);

                if (validUser)
                {
                    return View("UserInfo", userDetails);
                }

                ModelState.AddModelError("Login", "Invalid Username or Password");
                return View("Login");
            }
            

            return View("Login");
        }

        public ViewResult Register()
        {
            return View();          
        }
    }
}