﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Supplier.UpsModels;
using Supplier.UpsModels.HttpRequest;
using Supplier.UpsModels.HttpResponse;

namespace Supplier.Portal.Services
{
    public class Ups
    {
        private HttpClient _httpClient;
        private string _url;

        public Ups()
        {
            //Ups API's require SSL
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
        }

        public bool SubmitLabelRequest(UpsRequest.LabelRecovery labelRecoveryData, LabelResponseContainer resultData)
        {          
            bool result = false;

            _url += UpsConstants.LabelEndPoint;

            try
            {         
                //Alternative to below method on HttpClient. 
                //Note: Ups API's cannot handle UT8 encoding so must ue Default
                //var jsonString = JsonConvert.SerializeObject(data);
                //var requestMessage = new StringContent(jsonString, Encoding.Default, "application/json");
                //response = _httpClient.PostAsync(_url, requestMessage).Result;


                _httpClient = InitialiseHttpClient(UpsConstants.LabelEndPoint);

                HttpResponseMessage response = _httpClient.PostAsJsonAsync(_url, labelRecoveryData).Result;

                if (response.IsSuccessStatusCode)
                {
                    var contentString = response.Content.ReadAsStringAsync().Result;
                    //var responseObject = JsonConvert.DeserializeObject<dynamic>(contentString);

                    UpsResponse.FaultReponse faultResult = JsonConvert.DeserializeObject<UpsResponse.FaultReponse>(contentString);

                    UpsResponse.Label labelResult = JsonConvert.DeserializeObject<UpsResponse.Label>(contentString);

                    if (faultResult.Fault != null)
                    {
                        resultData.RequestError = faultResult.Fault.detail.Errors.ErrorDetail.PrimaryErrorCode;
                    }
                    else if (labelResult.LabelRecoveryResponse != null)
                    {
                        result = true;

                        resultData.LabelResult = labelResult.LabelRecoveryResponse.LabelResults;                        
                    }

                    return result;
                }
            }
            catch (AggregateException ex)
            {
                //var error = ex.Message;
                //throw ex.Flatten();
            }
            catch (Exception ex)
            {
                //var error = ex.Message;
            }

            return result;
        }
        
        //todo - Need to store timestamp of succesful shipping request to prevent making void request 
        //avialable if over 24 hours
        public bool SubmitVoidShipmentRequest(UpsRequest.VoidShipping voidShippingData, VoidResponseContainer resultData)
        {
            bool result = false;

            try
            {
                _httpClient = InitialiseHttpClient(UpsConstants.VoidShipmentEndPoint);

               HttpResponseMessage response = _httpClient.PostAsJsonAsync(_url, voidShippingData).Result;

                if (response.IsSuccessStatusCode)
                {
                    var contentString = response.Content.ReadAsStringAsync().Result;

                    UpsResponse.FaultReponse faultResult = JsonConvert.DeserializeObject<UpsResponse.FaultReponse>(contentString);

                    UpsResponse.VoidShipping voidResult = JsonConvert.DeserializeObject<UpsResponse.VoidShipping>(contentString);

                    if (faultResult.Fault != null)
                    {
                        resultData.RequestError = faultResult.Fault.detail.Errors.ErrorDetail.PrimaryErrorCode;
                    }
                    else if (voidResult.VoidShipmentResponse != null)
                    {
                        result = true;

                        resultData.VoidShipmentResult = voidResult.VoidShipmentResponse;
                    }

                    return result;
                }
            }
            catch (AggregateException ex)
            {
                //var error = ex.Message;
                //throw ex.Flatten();
            }
            catch (Exception ex)
            {
                //var error = ex.Message;
            }

            return result;
        }

        public bool SubmitShipOrderRequest(UpsRequest.Shipping shippingData, ShippingResponseContainer resultData)
        {
            bool result = false;

            try
            {
                _httpClient = InitialiseHttpClient(UpsConstants.ShippingEndPoint);

                HttpResponseMessage response = _httpClient.PostAsJsonAsync(_url, shippingData).Result;

                if (response.IsSuccessStatusCode)
                {
                    var contentString = response.Content.ReadAsStringAsync().Result;
                   
                    UpsResponse.FaultReponse faultResult = JsonConvert.DeserializeObject<UpsResponse.FaultReponse>(contentString);

                    UpsResponse.Shipping shippingResult = JsonConvert.DeserializeObject<UpsResponse.Shipping>(contentString);

                    if (faultResult.Fault != null)
                    {
                        resultData.RequestError = faultResult.Fault.detail.Errors.ErrorDetail.PrimaryErrorCode;
                    }
                    else if (shippingResult.ShipmentResponse != null)
                    {
                        result = true;

                        resultData.ShipmentData = shippingResult.ShipmentResponse.ShipmentResults;
                    }

                    return result;
                }
            }
            catch (AggregateException ex)
            {
                //throw ex.Flatten();

                resultData.RequestError = new UpsResponse.PrimaryErrorCode
                {
                    Description = ex.InnerExceptions[0].InnerException.Message
                };
            }
            catch (Exception ex)
            {
                resultData.RequestError = new UpsResponse.PrimaryErrorCode
                {
                    Description = ex.Message
                };                
            }

            return result;
        }

        private HttpClient InitialiseHttpClient(string endpointUrl)
        {
            string _clientUrl = string.Format("{0}{1}", UpsConstants.ApiBaseUrlTest,endpointUrl);

            _httpClient = new HttpClient();

            _httpClient.BaseAddress = new Uri(_clientUrl);
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return _httpClient;
        }
    }
}