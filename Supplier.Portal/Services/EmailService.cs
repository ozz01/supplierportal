﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;
using Supplier.Portal.Helpers;

namespace Supplier.Portal.Services
{
    public class EmailService
    {
       //todo - get Host IP on Live Server
       public static bool SendEmail(List<string> recipientEmailAddresses, string emailSubject, bool isHtmlBody, string emailBody, string fromAddress = "")
        {
            try
            {
                SmtpClient emailClient = new SmtpClient();
                MailAddress doNotReplyAddreess = new MailAddress(GlobalContants.DoNotTeplyEmail);

                MailMessage mailMessage = new MailMessage();

                recipientEmailAddresses.ForEach(x => mailMessage.To.Add(new MailAddress(x)));

                mailMessage.Sender = doNotReplyAddreess;

                if (string.IsNullOrEmpty(fromAddress))
                {
                    mailMessage.From = doNotReplyAddreess;
                }
                else
                {
                    mailMessage.From = new MailAddress(fromAddress);
                }

                mailMessage.Subject = emailSubject;
                mailMessage.BodyEncoding = Encoding.UTF8;
                mailMessage.IsBodyHtml = isHtmlBody;
                mailMessage.Body = emailBody;

                emailClient.Send(mailMessage);
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }
    }
}