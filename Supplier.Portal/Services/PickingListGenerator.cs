﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Supplier.Portal.DAL.Entities;
using Supplier.Portal.EntityServices;
using Supplier.Portal.Services.ExcelGeneration;
using Supplier.UpsModels.Common;

namespace Supplier.Portal.Services
{
    public class PickingListGenerator
    {
        private readonly WebOrder _webOrder;
        private readonly ExcelGenerator _excelGenerator;
        private readonly EntityServices.Supplier _supplier;

        public PickingListGenerator()
        {
            _webOrder = new WebOrder();
            _excelGenerator = new ExcelGenerator();
            _supplier = new EntityServices.Supplier();
        }

        public HttpResponseMessage GenerateDownloadStream(int userId, string fileFormat)
        {
            string message = String.Empty;
            HttpResponseMessage response;
            MemoryStream stream = new MemoryStream();
            List<ExcelPickingListDto> pickingList = new List<ExcelPickingListDto>();

            _webOrder.GetUserBatchForExport(userId, ref pickingList, out message);

            if (fileFormat.ToUpper() == "EXCEL")
            {
                 ExcelWorksheetDto worksheetItem = new ExcelWorksheetDto
                 {
                    Data = pickingList,
                    WorksheetName = "Picking List"
                 };

                 _excelGenerator.GenerateFromEnumerable(stream, worksheetItem);
            }
            else if (fileFormat.ToUpper() == "CSV")
            {
                var streamWriter = new StreamWriter(stream, Encoding.Default);

                WriteHeaderLine(streamWriter);
                streamWriter.WriteLine();
                WriteDataLines(streamWriter, pickingList);

                streamWriter.Flush();
            }

            response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new ByteArrayContent(stream.ToArray());
            
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = fileFormat == "Excel" ? "PickingList.xlsx" : "PickingList.csv"
            };

            return response;
        }

        public FileDataModel GenerateWorksheetBase64String(int userId)
        {
            FileDataModel model = new FileDataModel();
            string message = String.Empty;
            MemoryStream stream = new MemoryStream();
            List<ExcelPickingListDto> pickingList = new List<ExcelPickingListDto>();

            _webOrder.GetUserBatchForExport(userId, ref pickingList, out message);
         
            ExcelWorksheetDto worksheetItem = new ExcelWorksheetDto
            {
                Data = pickingList,
                WorksheetName = "Picking List"
            };

            _excelGenerator.GenerateFromEnumerable(stream, worksheetItem);

            model.Action = "PickingList";
            model.WorksheetData = Convert.ToBase64String(stream.ToArray());

            //Get Supplier Printer details
            SupplierEntity supplier = _supplier.GetSupplierForUser(userId);

            model.StandardPrinterName = supplier != null ? supplier.StandarPrinterName : "";
            model.LabelPrinterName = supplier != null ? supplier.LabelPrinterName : "";
            
            return model;
        }

        private void WriteHeaderLine(StreamWriter streamWriter)
        {
            string headers = "Product Code, Ean Code, WG Product Code, Product Name, Order Qty, Stock Qty";

            streamWriter.Write(headers);        
        }

        private void WriteDataLines(StreamWriter streamWriter, List<ExcelPickingListDto> pickingList)
        {
            foreach (ExcelPickingListDto item in pickingList)
            {
                string values = string.Format("{0},{1},{2},{3},{4},0", 
                    item.ProductCode, 
                    item.EanCode, 
                    item.WGProductCode, 
                    item.ProductName, 
                    item.OrderQty);

                streamWriter.WriteLine();
                streamWriter.Write(values);
            }
        }
    }
}