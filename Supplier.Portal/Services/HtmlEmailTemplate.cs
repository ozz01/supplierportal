﻿using System;
using System.Collections.Generic;
using System.IO;
using RazorEngine;
using RazorEngine.Templating;
using Supplier.Portal.Helpers;

namespace Supplier.Portal.Services
{
    public static class HtmlEmailTemplate
    {
        public static string GenerateFromList<T>(string templateName, T model)
        {
            string body = String.Empty;

            // determine the full path the e-mail template.emailTemplatePath
            string templatePath = string.Format(@"{0}EmailTemplates\{1}.cshtml", AppDomain.CurrentDomain.SetupInformation.ApplicationBase, templateName);

            // do the merge and return
            try
            {
                // read in the contents of this template.
                string template = File.ReadAllText(templatePath);
                
                body = Engine.Razor.RunCompile(template, templateName, typeof (List<T>), model);
            }
            catch (Exception ex)
            {
                body = GlobalContants.EmailGenerationError;
            }

            return body;
        }

        public static string GenerateFromModel<T>(string templateName, T model)
        {
            string body = String.Empty;

            // determine the full path the e-mail template.emailTemplatePath
            string templatePath = string.Format(@"{0}EmailTemplates\{1}.cshtml", AppDomain.CurrentDomain.SetupInformation.ApplicationBase, templateName);
        
            // do the merge and return
            try
            {
                // read in the contents of this template.
                string template = File.ReadAllText(templatePath);

                body = Engine.Razor.RunCompile(template, templateName, typeof (T), model);
            }
            catch (Exception ex)
            {
                body = GlobalContants.EmailGenerationError;
            }

            return body;
        } 
    }
}