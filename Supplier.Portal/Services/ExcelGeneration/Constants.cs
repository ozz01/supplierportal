﻿namespace Supplier.Portal.Services.ExcelGeneration
{
    public static class ExcelConstants
    {
        public const int ExcelFontSize = 10;
        public const string ExcelCurrencyFormatting = "£#,##0.00 ;[Red](£#,##0.00)";
        public const string ExcelNumericFormatting = "#,##0 ;";
        public const string ExcelDateFormatting = "dd/mm/yyyy";
        public const string ExcelFontName = "Arial";
        public const double ExcelColumnWidthAdjustment = 1.2;
    }
}