﻿namespace Supplier.Portal.Services.ExcelGeneration
{
    public class ExcelPickingListDto
    {
        //Supplier Product Code
        public string ProductCode { get; set; }

        public string EanCode { get; set; }

        //WG Product Code
        public string WGProductCode { get; set; }

        public string ProductName { get; set; }

        public int OrderQty { get; set; }

        public int StockQty { get; set; }
    }
}
