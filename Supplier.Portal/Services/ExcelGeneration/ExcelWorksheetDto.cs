﻿using System.Collections.Generic;

namespace Supplier.Portal.Services.ExcelGeneration
{
    public class ExcelWorksheetDto
    {
        private string _worksheetTitle;

        public string WorksheetName { get; set; }
        
        public IEnumerable<object> Data { get; set; }
       
        public string TableName
        {
            get {return WorksheetName.Replace(" ", ""); }
        }

        public string WorksheetTitle
        {
            get { return (string.IsNullOrEmpty(_worksheetTitle) ? WorksheetName : _worksheetTitle); }
            set { _worksheetTitle = value; }
        }
    }
}