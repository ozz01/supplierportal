﻿using System.IO;
using System.Linq;
using ClosedXML.Excel;

namespace Supplier.Portal.Services.ExcelGeneration
{
    public class ExcelGenerator
    {
        private XLWorkbook _wb;

        public void GenerateFromEnumerable(Stream stream, ExcelWorksheetDto excelDataModel)
        {
            _wb = new XLWorkbook
            {
                PageOptions =
                {
                    PaperSize = XLPaperSize.A4Paper,
                    PageOrientation = XLPageOrientation.Portrait,
                }
            };

            _wb.Style.Font.FontName = ExcelConstants.ExcelFontName;
            _wb.Style.Font.FontSize = ExcelConstants.ExcelFontSize;
        
            var ws = _wb.Worksheets.Add(excelDataModel.WorksheetName);

            if (!excelDataModel.Data.Any())
            {
                ws.Cell(3, 1).Value = "No Data";
            }
            else
            {
                var table = ws.Cell(3, 1).InsertTable(excelDataModel.Data, excelDataModel.TableName, true);
 
                table.ShowTotalsRow = true;
                table.Field("OrderQty").TotalsRowFunction = XLTotalsRowFunction.Sum;
            }

            ws.Column(3).Cells().Style.NumberFormat.Format = ExcelConstants.ExcelNumericFormatting;

            ws.Columns().AdjustToContents();

            ws.Column(3).Width = AdjustNumberColumnWidth(ws.Column(3).Width);

            SetHeadings(ws, excelDataModel);

            SetPageOptions(ws);

            _wb.SaveAs(stream);
        }

        protected void SetHeadings(IXLWorksheet ws, ExcelWorksheetDto excelDataModel, bool showDates = true)
        {
            ws.Cell(1, 1).Value = excelDataModel.WorksheetTitle;
            ws.Cell(1, 1).Style.Font.Bold = true;

            //ws.Cell(1, 2).Value = DateTime.Today;
            //ws.Cell(1, 2).Style.Font.Bold = true;
        }

        protected double AdjustNumberColumnWidth(double colWidth)
        {
            return colWidth * ExcelConstants.ExcelColumnWidthAdjustment;
        }

        protected void SetPageOptions(IXLWorksheet ws)
        {
            ws.PageSetup.Margins.Top = 1;
            ws.PageSetup.Margins.Bottom = 1.25;
            ws.PageSetup.Margins.Left = 0.5;
            ws.PageSetup.Margins.Right = 0.75;
            ws.PageSetup.Margins.Footer = 0.15;
            ws.PageSetup.Margins.Header = 0.30;

            ws.PageSetup.CenterHorizontally = true;

            ws.PageSetup.FitToPages(1, 0);
        }
    }
}