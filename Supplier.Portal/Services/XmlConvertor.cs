﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Supplier.Portal.Models;

namespace Supplier.Portal.Services
{
    public static class XmlConvertor
    {
        //Not In Use - Reference Only
        public static TransactionCheckout GetCheckoutDataFromXmlString(string xmlString)
        {
            TransactionCheckout serializedCheckoutData = new TransactionCheckout();

            try
            {
                if (!string.IsNullOrEmpty(xmlString))
                {
                    var xmlDocument = new XmlDocument();
                    xmlDocument.LoadXml(xmlString);
                    string json = JsonConvert.SerializeXmlNode(xmlDocument);

                    //Handle nullable xml fields
                    json = json.Replace("{\"@xsi:nil\":\"true\"}", "\"\"");

                    serializedCheckoutData = JsonConvert.DeserializeObject<TransactionCheckout>(json);
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
            }

            return serializedCheckoutData;
        }

        public static CheckoutType GetCheckoutDataFromXml(string xmlString)
        {
            CheckoutType serializedData = new CheckoutType();

            try
            {
                if (!string.IsNullOrEmpty(xmlString))
                {
                    StringReader _xmlStringInput = new StringReader(xmlString);

                    var serializer = new XmlSerializer(typeof(CheckoutType));
                    serializedData = (CheckoutType)serializer.Deserialize(_xmlStringInput);
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
            }

            return serializedData;
        }

        public static PaymentType GetPaymentDataFromXml(string xmlString)
        {
            PaymentType serializedData = new PaymentType();

            try
            {
                if (!string.IsNullOrEmpty(xmlString))
                {
                    StringReader _xmlStringInput = new StringReader(xmlString);

                    var serializer = new XmlSerializer(typeof(PaymentType));
                    serializedData = (PaymentType)serializer.Deserialize(_xmlStringInput);
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
            }

            return serializedData;
        }     
    }
}