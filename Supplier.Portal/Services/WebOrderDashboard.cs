﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Supplier.Portal.DAL;
using Supplier.Portal.DAL.WebEntities;
using Supplier.Portal.EntityServices;
using Supplier.Portal.Helpers;
using Supplier.Portal.ViewModels;
using Supplier.Portal.ViewModels.Dashboad;

namespace Supplier.Portal.Services
{
    public class WebOrderDashboard
    {
        private Company _company;
        private FileImportLog _log;
        private UserBatch _userBatch;

        private string pendingStatus = GlobalContants.GetDespatchStatus(GlobalContants.DespatchStatus.Pending);
        private string beingPickedStatus = GlobalContants.GetDespatchStatus(GlobalContants.DespatchStatus.BeingPicked);
        private string pickedStatus = GlobalContants.GetDespatchStatus(GlobalContants.DespatchStatus.Picked);
        private string readyToShipStatus = GlobalContants.GetDespatchStatus(GlobalContants.DespatchStatus.ReadyToShip);

        public WebOrderDashboard()
        {
            _log = new FileImportLog();
            _company = new Company();
            _userBatch = new UserBatch();
        }

        public DashboardViewModel MainDashbaordData()
        {
            DateTime filterDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 6, 0, 0);

            DashboardViewModel data = new DashboardViewModel
            {
                FileUploadStats = _log.Stats(filterDate),
                FileImports = _log.All(filterDate),
                OrderStats = OrderStats(filterDate)
            };

            return data;
        }

        public SupplierOrderStatsViewModel SupplierOrderStats(int userId)
        {
            SupplierOrderStatsViewModel stats = new SupplierOrderStatsViewModel();

            ResetIncompleteBatches();

            List<string> supplierCodes = _company.GetSupplierCodesForUser(userId);

            try
            {
                using (WebSupplierContext context = new WebSupplierContext())
                {
                    //Basket Lines for In Correct Transaction Status
                    var initialList = context.BasketLine
                        .Where(i => i.Basket.OrderTransaction.TransactionStatusID == 4);                 //Important  

                    /*****************************************/
                    /***    Pending orders for supplier    ***/
                    /*****************************************/
                    //Filter out orders where basket lines are for other suppliers to prevent split supplier orders
                    var supplierRecords = initialList
                        .Select(i => i.Basket)
                        .Where(i => !i.BasketItems.Any(y => y.Sku.SkuSuppliers
                                .Any(x => !supplierCodes.Contains(x.SupplierCode))))
                        .Distinct();


                    var orderItems = supplierRecords
                        .SelectMany(i => i.BasketItems);
                    
                    /*****************************************************/
                    /***    Pending Un-assigned orders for supplier    ***/
                    /*****************************************************/
                    stats.PendingOrders = supplierRecords
                        .Where(i => i.OrderTransaction.DespatchStatus == pendingStatus)
                        .Select(x => x.OrderTransaction.OrderNumber)
                        .Distinct()
                        .Count();

                    stats.PendingOrderItems = orderItems
                        .Count(i => i.Basket.OrderTransaction.UserBatchId == null);

                    /***************************************************************************/
                    /***     In Progress orders for supplier and assigned to current user    ***/
                    /***************************************************************************/
                    
                    //Get Last In-Complete UserBatchId for user
                    int inprogressUserBatchId = _userBatch.GetCurrentUserBatchIdForUser(userId);

                    //Only process if user has a current batchId
                    if (inprogressUserBatchId != 0)
                    {
                        stats.MyPickingList = supplierRecords
                            .Where(i => i.OrderTransaction.DespatchStatus == beingPickedStatus
                                        && i.OrderTransaction.UserBatchId == inprogressUserBatchId)
                            .Select(x => x.OrderTransaction.OrderNumber)
                            .Distinct()
                            .Count();

                        stats.MyPickingListItems = orderItems
                            .Count(i => i.Basket.OrderTransaction.DespatchStatus == beingPickedStatus
                                        && i.Basket.OrderTransaction.UserBatchId == inprogressUserBatchId);
                    }

                    /************************************************************************/
                    /***     In Progress orders for supplier and assigned to any user     ***/
                    /************************************************************************/
                    stats.OrdersBeingProcessed = supplierRecords
                        .Where(i => i.OrderTransaction.DespatchStatus == beingPickedStatus)
                        .Select(x => x.OrderTransaction.OrderNumber)
                        .Distinct()
                        .Count();

                    stats.OrderItemsBeingProcessed = orderItems
                        .Count(i => i.Basket.OrderTransaction.DespatchStatus == beingPickedStatus);


                    /*******************************************************/
                    /***     Picked Out Of Stock orders for supplier     ***/
                    /*******************************************************/
                    var outOfStockOrderItems = context.BasketLine
                        .Where(i => i.Basket.OrderTransaction.TransactionStatusID == 4                   //Important
                            && i.Basket.OrderTransaction.DespatchStatus == pickedStatus
                            && i.Sku.SkuSuppliers.Any(x => supplierCodes.Contains(x.SupplierCode)));

                    stats.OutOfStockOrders = outOfStockOrderItems
                        .Select(x => x.Basket.OrderTransaction.OrderNumber)
                        .Distinct()
                        .Count();

                    stats.OutOfStockOrderItems = outOfStockOrderItems.Count();

                    /****************************************************************/
                    /***       Processed In Stock Picked orders for supplier      ***/
                    /****************************************************************/               
                    var inStockOrders = context.Basket
                        .Where(i => i.OrderTransaction.TransactionStatusID == 4                             //Important
                            && (!i.BasketItems.Any(x => x.OutOfStock.HasValue))
                            && i.OrderTransaction.DespatchStatus == readyToShipStatus
                            && i.BasketItems.Any(z => z.Sku.SkuSuppliers
                                    .Any(x => supplierCodes.Contains(x.SupplierCode))))                             
                        .ToList();

                    stats.InStockOrders = inStockOrders.Count();

                    stats.InStockOrderItems = inStockOrders
                        .SelectMany(x => x.BasketItems)
                        .Count();


                    /***********************************************************/
                    /***      Processed In Stock Picked orders for User      ***/
                    /***********************************************************/

                    int lastCompleteUserBatchId = _userBatch.GetLastCompleteUserBatchIdForUser(userId);

                    stats.HaveOrdersToShip = inStockOrders
                        .Any(x => x.OrderTransaction.UserBatchId == lastCompleteUserBatchId);
                }
            }
            catch (Exception ex)
            {
                
            }

            return stats;
        }

        public SupplierStockUpdatesViewModel SupplierStockUpdateData(int userId)
        {
            DateTime filterDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 6, 0, 0);
            
            SupplierStockUpdatesViewModel model = new SupplierStockUpdatesViewModel
            {
                FileUploadStats = _log.CompanyStats(userId, filterDate),
                FileImports = _log.SupplierLogs(userId, filterDate),
            };

            return model;
        }


        /// Private Methods ///

        private OrderStatsViewModel OrderStats(DateTime datefilter)
        {
            ResetIncompleteBatches();
           
            OrderStatsViewModel stats = new OrderStatsViewModel { ProcessDate = datefilter };

            using (WebSupplierContext context = new WebSupplierContext())
            {
                //Get the Un-processed [Order] records
                var orderRecords = context.Order
                    .Where(i => i.TransactionStatusID == 4 );

                //Get the Un-processed [OrderItem] records
                var orderItems = orderRecords
                    .SelectMany(i => i.Basket
                        .SelectMany(x => x.BasketItems));

                /********************************************/
                /***                Pending               ***/
                /********************************************/

                //Group By OrderNo Where BatchId has NOT been assigned i.e. NOT being processed
                stats.PendingOrders = orderRecords
                    .Where(i => i.DespatchStatus == pendingStatus)
                    .Select(x => x.OrderNumber)
                    .Distinct()
                    .Count();

                //Count records where BatchId has NOT been assigned i.e. NOT being processed
                stats.PendingOrderItems = orderItems
                    .Count(i => i.Basket.OrderTransaction.DespatchStatus == pendingStatus);

                /********************************************/
                /***           Being Processed            ***/
                /********************************************/

                //Group By OrderNo Where BatchId has been assigned i.e. being processed
                stats.OrdersBeingProcessed = orderRecords
                    .Where(i => i.DespatchStatus == beingPickedStatus)
                    .Select(x => x.OrderNumber)
                    .Distinct()
                    .Count();


                //Count records where BatchId has been assigned i.e. being processed
                stats.OrderItemsBeingProcessed = orderItems
                    .Count(i => i.Basket.OrderTransaction.DespatchStatus == beingPickedStatus);


                /*********************************/
                /***    Out Of Stock orders    ***/
                /*********************************/
                var outOfStockOrderItems = context.BasketLine
                  .Where(i => i.Basket.OrderTransaction.TransactionStatusID == 4                   //Important
                      && i.Basket.OrderTransaction.DespatchStatus == pickedStatus);

                stats.OutOfStockOrders = outOfStockOrderItems
                    .Select(x => x.Basket.OrderTransaction.OrderNumber)
                    .Distinct()
                    .Count();

                stats.OutOfStockOrderItems = outOfStockOrderItems.Count();


                /*************************************/
                /***        In Stock orders        ***/
                /*************************************/
                var inStockOrderItems = context.BasketLine
                  .Where(i => i.Basket.OrderTransaction.TransactionStatusID == 4                   //Important
                      && i.Basket.OrderTransaction.DespatchStatus == readyToShipStatus);

                stats.InStockOrders = inStockOrderItems
                    .Select(x => x.Basket.OrderTransaction.OrderNumber)
                    .Distinct()
                    .Count();

                stats.InStockOrderItems = outOfStockOrderItems.Count();          
            }

            return stats;
        }

        private void ResetIncompleteBatches()
        {          
            try
            {
                //Get list of old userBatchId's
                List<int> userBatchIds = _userBatch.GetUserBatches()
                  .Where(i => i.BatchDate < DateTime.Today)
                  .Select(x => x.UserBatchId)
                  .ToList();
              
                using (WebSupplierContext context = new WebSupplierContext())
                {
                    var incompleteOrderBatches = context.Order
                        .Where(i => i.DespatchStatus == beingPickedStatus
                            && i.UserBatchId.HasValue
                            && userBatchIds.Contains(i.UserBatchId.Value))
                        .ToList();

                    foreach (TransactionEntity orderTransaction in incompleteOrderBatches)
                    {
                        orderTransaction.UserBatchId = null;
                        orderTransaction.DespatchStatus = string.Empty;
                        context.Order.AddOrUpdate(orderTransaction);
                    }

                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }
        }
    }
}