﻿using System.Data.Entity;
using Supplier.StockUpdater.DAL.Entities;

namespace Supplier.StockUpdater.DAL
{
    public class SupplierContext : DbContext
    {
        public SupplierContext(string connStr)
            : base(connStr)
        {
            //Set to null to prevent EF from comparing models with Existing entities in DB (i.e. DB First -> no migrations)
            Database.SetInitializer<SupplierContext>(null);
        }

        public virtual DbSet<FileimportLogEntity> FileimportLog { get; set; }
    }
}