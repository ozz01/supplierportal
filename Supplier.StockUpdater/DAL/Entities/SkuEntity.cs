﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Supplier.StockUpdater.DAL.Entities
{
    [Table("commerce_Sku")]
    public class SkuEntity
    {
        [Key]
        public long SkuId { get; set; }

        [Required]
        public string Code { get; set; }

        public long StockLevel { get; set; }

        [Required]
        public bool Deleted { get; set; }
    }
}