﻿using System.Data.Entity;
using Supplier.StockUpdater.DAL.Entities;

namespace Supplier.StockUpdater.DAL
{
    public class WebContext : DbContext
    {
        public WebContext(string connStr)
            : base(connStr)
        {
            //Set to null to prevent EF from comparing models with Existing entities in DB (i.e. DB First -> no migrations)
            Database.SetInitializer<WebContext>(null);
        }

        public DbSet<SkuEntity> Sku { get; set; }
    }
}