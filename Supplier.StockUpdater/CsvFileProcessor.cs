﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Supplier.StockUpdater.DAL.Entities;
using Supplier.StockUpdater.Helpers;

namespace Supplier.StockUpdater
{
    public class CsvFileProcessor
    {
        private static Sku _sku;
        private static string _sourcePath = String.Empty;
        private static string _destinationPath = String.Empty;

        public static void Main()
        {
            int companyId = 0;
            _sourcePath = ConfigurationManager.AppSettings["ImportFolder"];

            string[] folderPaths = Directory.GetDirectories(_sourcePath);

            //Loop through company folders
            foreach (string folder in folderPaths)
            {
                string folderName = folder.Replace(_sourcePath + "\\", "").ToUpper();

                switch (folderName)
                {
                    case  "MERTEX":
                        companyId = 1;
                        break;
                    case "COMPANY2":
                        companyId = 2;
                        break;
                };

                _destinationPath = string.Format("{0}\\Processed", folder);
                string[] filePaths = Directory.GetFiles(folder, "*.csv");

                //Loop through supplier files
                foreach (string fileName in filePaths)
                {
                    ReadFileData(fileName, companyId);
                }               
            }
        }

        private static void ReadFileData(string filePath, int companyId)
        {
            string fileName = filePath.Split('\\').Last().ToString();

            FileimportLogEntity logEntry = new FileimportLogEntity(fileName);

            logEntry.CompanyId = companyId;

            var lines = File.ReadAllLines(filePath).Select(a => a.Split(','));    


            string[][] rows = (from line in lines select line.ToArray()).Skip(1).ToArray();  //skip row 1 - 

            if (rows.Any())
            {
                List<SkuEntity> skusToUpdate = ConvertCsvArrayToEntities(rows, logEntry);

                if (skusToUpdate.Any())
                {
                    UpdateSkuRecords(skusToUpdate, logEntry);
                }
            }

            CreateFileImportLog(logEntry);
            MoveProcessedFile(fileName, filePath);
        }

        private static List<SkuEntity> ConvertCsvArrayToEntities(string[][] rows, FileimportLogEntity logEntry)
        {
            int fileRow = 1;
            _sku = new Sku();
            List<SkuEntity> skuEntitiesToUpdate = new List<SkuEntity>();

            try
            {
                foreach (string[] row in rows)
                {
                    string skuCode = row[0];
                    long qty = long.Parse(row[1]);

                    SkuEntity skuEntity = _sku.QueryableWithoutTracking().FirstOrDefault(i => i.Code == skuCode && i.Deleted == false);

                    if (skuEntity != null)
                    {
                        skuEntity.StockLevel = qty;
                        skuEntitiesToUpdate.Add(skuEntity);
                    }
                    else
                    {
                        logEntry.ErrorDescription += string.Format("Row ({0}): Sku ({1}) not found. ", fileRow, skuCode);                        
                    }

                    fileRow++;
                }
            }
            catch (Exception ex)
            {

                logEntry.ErrorDescription += string.Format("Row ({0}): {1} File import aborted", fileRow, ex.Message);
            }
            finally
            {
                _sku.Dispose();
            }

            return skuEntitiesToUpdate;
        }

        private static void UpdateSkuRecords(List<SkuEntity> skus, FileimportLogEntity logEntry)
        {
            int skuUpdateCount = 0;
            _sku = new Sku();

            foreach (SkuEntity skuEntity in skus)
            {
                int updateResult = _sku.Update(skuEntity);

                if (updateResult == 1)
                    skuUpdateCount += 1;
            }

            _sku.Dispose();
            logEntry.SkusUpdated = skuUpdateCount;
        }

        private static void CreateFileImportLog(FileimportLogEntity logEntry)
        {
            using (FileImport filerImporter = new FileImport())
            {
                filerImporter.Add(logEntry);
            }
        }

        //todo - change Copy to Move
        private static void MoveProcessedFile(string fileName, string sourceFilePath)
        {
            string fileDestination = string.Format("{0}\\PROCESSED_{1}", _destinationPath, fileName);

            if (File.Exists(fileDestination))
            {
                File.Delete(fileDestination);
            }

            File.Copy(sourceFilePath, fileDestination);
        }
    }
}