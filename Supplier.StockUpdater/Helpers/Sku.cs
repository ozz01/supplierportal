﻿using System;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Linq;
using Supplier.StockUpdater.DAL;
using Supplier.StockUpdater.DAL.Entities;

namespace Supplier.StockUpdater.Helpers
{
    public class Sku : IDisposable
    {
        private WebContext _context;
        private bool _disposed;

        public Sku()
        {
            _context = new WebContext(Constants.WebContextConnection);
        }

        public int Update(SkuEntity sku)
        {
            try
            {
                _context.Sku.AddOrUpdate(sku);

                return _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);

                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }

                throw;
            }
        }

        public IQueryable<SkuEntity> QueryableWithoutTracking()
        {
            return _context.Sku.AsNoTracking();
        }

        public bool IsDisposed { get { return _disposed; } }

        public void Dispose()
        {
            if (!_disposed)
            {
                _context.Dispose();
                _disposed = true;
            }
        }
    }
}