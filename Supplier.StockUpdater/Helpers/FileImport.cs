﻿using System;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using Supplier.StockUpdater.DAL;
using Supplier.StockUpdater.DAL.Entities;

namespace Supplier.StockUpdater.Helpers
{
    public class FileImport : IDisposable
    {
        private SupplierContext _context;
        private bool _disposed;

        public FileImport()
        {
            _context = new SupplierContext(Constants.SupplierContextConnection);
        }

        public void Add(FileimportLogEntity logEntity)
        {
            using (_context)
            {
                try
                {
                    _context.FileimportLog.AddOrUpdate(logEntity);

                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);

                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        }
                    }

                    throw;
                }
            }
        }

        public void Dispose()
        {
            if (!_disposed)
            {
                _context.Dispose();
                _disposed = true;
            }
        }
    }
}