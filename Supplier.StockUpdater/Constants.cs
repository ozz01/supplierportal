﻿namespace Supplier.StockUpdater
{
    public static class Constants
    {
        public const string SupplierContextConnection = "Data Source=WGB2013; Initial Catalog=Suppliers; Persist Security Info=True; MultipleActiveResultSets=True; integrated security=True; Asynchronous Processing=True; Max Pool Size=10";
        public const string WebContextConnection = "Data Source=WGB2013; Initial Catalog=WalkerGreenbank; Persist Security Info=True; MultipleActiveResultSets=True; User ID=WG; Password=gr33nbank; Asynchronous Processing=True; Max Pool Size=10";
    }
}
