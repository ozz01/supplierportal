﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;

namespace System.Web.Mvc.Html
{
    public static class HtmlHelpers
    {
        public static MvcHtmlString CustomStyledValidationMessageFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            var elementName = ExpressionHelper.GetExpressionText(expression);
            var normal = htmlHelper.ValidationMessageFor(expression);
            if (normal != null)
            {
                var newValidator = Regex.Replace(normal.ToHtmlString(), @"<span([^>]*)>([^<]*)</span>",
                    string.Format("<div for=\"{0}\" $1>$2</div>", elementName), RegexOptions.IgnoreCase);

                newValidator = newValidator.Replace("field-validation-error", "alertInfo error");
                return MvcHtmlString.Create(newValidator);
            }
            return null;
        }

        public static MvcHtmlString CustomLabelFor<TModel, TProperty>(this HtmlHelper<TModel> html,
         Expression<Func<TModel, TProperty>> expression,
         string htmlFieldName = "", string labelText = null,
         string cssFieldId = "self", string cssClassName = "required", IDictionary<string,
             object> htmlAttributes = null)
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            if (string.IsNullOrWhiteSpace(htmlFieldName))
                htmlFieldName = ExpressionHelper.GetExpressionText(expression);

            return LabelHelper(html, metadata, htmlFieldName, labelText, cssFieldId, cssClassName, htmlAttributes);
        }

        internal static MvcHtmlString LabelHelper(HtmlHelper html, ModelMetadata metadata, string htmlFieldName,
          string labelText = null,
          string cssFieldId = "self", string cssClassName = "required", IDictionary<string,
              object> htmlAttributes = null)
        {
            var resolvedLabelText =
                labelText ?? metadata.DisplayName ??
                metadata.PropertyName ?? htmlFieldName.Split('.').Last();

            var description = metadata.Description;

            if (string.IsNullOrEmpty(resolvedLabelText))
            {
                return MvcHtmlString.Empty;
            }

            var tag = new TagBuilder("label");
            if (htmlAttributes != null) tag.MergeAttributes(htmlAttributes);
            tag.Attributes.Add(
                "for",
                TagBuilder.CreateSanitizedId(
                    html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(htmlFieldName)
                    )
                );

            if (!new Regex(@"<[^>]+>").IsMatch(resolvedLabelText))
            {
                var labelSpan = new TagBuilder("span");
                labelSpan.InnerHtml = resolvedLabelText;
                tag.InnerHtml = labelSpan.ToString();
            }
            else
                tag.InnerHtml = resolvedLabelText;

            //The Required attribute has allow multiple false
            var isRequired = metadata.ContainerType.GetProperty(metadata.PropertyName)
                .GetCustomAttributes(typeof(RequiredAttribute), false)
                .Length == 1;

            if (isRequired)
            {
                var requiredLabel = new TagBuilder("span");
                requiredLabel.AddCssClass("required");
                requiredLabel.InnerHtml += "Required";

                tag.InnerHtml += requiredLabel.ToString();
            }

            if (!string.IsNullOrEmpty(description))
            {
                var descriptionLabel = new TagBuilder("p");

                descriptionLabel.InnerHtml += "<em>" + description + "</em>";

                tag.InnerHtml += descriptionLabel.ToString();
            }

            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
        }
    }
}