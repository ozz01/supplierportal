﻿namespace WalkerGreenBank.CMS.Helpers
{
    public class Constants
    {
        public class TenantTypes
        {
            public const string ColonyCMSBackend = "ColonyCMSBackend";
            public const string ColonyCMSFrontend = "ColonyCMSFrontend";
        }

        public class ClaimTypes
        {
            public const string Tenant = "http://redant.com/colony/claims/tenant";      
        }
    }
}