﻿using System.Linq;
using System.Security.Claims;

namespace WalkerGreenBank.CMS.Helpers
{
    public static class ClaimsPrincipalHelpers
    {
        public static ClaimsIdentity GetForTenant(this ClaimsPrincipal claimsPrincipal, string tenant)
        {
            var user = ClaimsPrincipal.Current.Identities.FirstOrDefault((x) =>
            {
                var tenantClaim = x.FindFirst(Constants.ClaimTypes.Tenant);
                if (tenantClaim != null)
                    return tenantClaim.Value == tenant;

                return false;
            });

            return user;
        }
    }
}