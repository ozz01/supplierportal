﻿using System.Collections.Generic;

namespace WalkerGreenBank.CMS.Services
{
    public class BehaviourDefinition
    {
        public string BehaviourId { get; set; }
        public string MenuParams { get; set; }
        public List<MessageHandlerDefinition> MessageHandlers { get; set; }
        public PageDefinition Page { get; set; }
        public List<ComponentDefinition> ComponentContainer { get; set; }
        public InitialDataDefinition Data { get; set; }
        public BehaviourPermission Permission { get; set; }
    }

    public class ComponentDefinition
    {
        public string Type { get; set; }
        public string Label { get; set; }
        public string ComponentId { get; set; }
        public string ClassName { get; set; }
        public List<MessageDefinition> Messages { get; set; }
        public string DataCollection { get; set; }
        public dynamic Items { get; set; }
        public List<FieldMapping> Map { get; set; }
        public List<FieldMapping> CsvMap { get; set; }
        public List<ComponentDefinition> Components { get; set; }
        public List<ComponentAction> Actions { get; set; }
        public List<ComponentAction> BatchActions { get; set; }
        public object Validation { get; set; }
        public string ParentIdentifier { get; set; }
        public string Helptext { get; set; }
        public string DefaultOptionText { get; set; }
    }

    public class ComponentAction
    {
        public string Icon { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Id { get; set; }
        public List<MessageDefinition> Messages { get; set; }
        public string Trigger { get; set; }
        public string ClassName { get; set; }
        public bool ExternalLink { get; set; }
    }

    public class FieldMapping
    {
        public string FriendlyName { get; set; }
        public string Source { get; set; }
        public string Default { get; set; }
    }

    public class PageDefinition
    {
        public string Label { get; set; }
        public string Back { get; set; }
        public string Backdescription { get; set; }
        public List<MessageDefinition> Messages { get; set; }
    }

    public class MessageDefinition
    {
        public string ActionUri { get; set; }
        public string Message { get; set; }
        public string BehaviourId { get; set; }
        public string Trigger { get; set; }
        public dynamic Params { get; set; }
        public object Success { get; set; }
        public object Failure { get; set; }
        public string Type { get; set; }
    }

    public class BehaviourPermission
    {
        public string Module { get; set; }
        public string Claim { get; set; }
    }

    public class InitialDataDefinition
    {
        public List<MessageHandlerDefinition> Execute { get; set; }
    }

    public class MessageHandlerDefinition
    {
        public string Name { get; set; }
        public string ReturnName { get; set; }
        public string ServiceType { get; set; }
        public string ServiceMethod { get; set; }
        public List<ParameterMapping> ServiceMethodParams { get; set; }
        public List<FieldMapping> Map { get; set; }
        public bool BindEntityFromParameters { get; set; }
    }

    public class ParameterMapping
    {
        public string Key { get; set; }
        public object Value { get; set; }
        public bool IsDynamic { get; set; }
    }
}