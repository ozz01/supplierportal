﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace WalkerGreenBank.CMS.Services
{
    public class BehaviourContext : IDisposable
    {
        private List<BehaviourDefinition> _behaviours;
        private IDictionary<string, string> _navigation;
        private static BehaviourContext _instance;
        private FileSystemWatcher ConfigWatcher;
        private FileSystemWatcher NavigationWatcher;
        private JavaScriptSerializer _serializer = new JavaScriptSerializer();
        
        private readonly string jsonFileFolder = System.Web.HttpContext.Current.Server.MapPath("~/BehaviourDefinitions");
        private readonly string navigationFilePath = System.Web.HttpContext.Current.Server.MapPath("~/Scripts/ColonyCMS/urlmap.js");
        private ReaderWriterLockSlim _behaviourLock = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);
        private ReaderWriterLockSlim _navigationLock = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);
        
        private IEnumerable<BehaviourDefinition> Behaviours
        {
            get
            {
                if (_behaviours == null)
                    _behaviours = new List<BehaviourDefinition>();

                _behaviourLock.EnterReadLock();
                var returnValue = _behaviours;
                
                _behaviourLock.ExitReadLock();
                
                return returnValue;
            }
        }

        public static BehaviourContext Current
        {
            get
            {
                if (_instance == null)
                    _instance = new BehaviourContext();
                return _instance;
            }

        }

        public BehaviourDefinition GetBehaviourByUri(string module, string action = "index")
        {
            return null;
        }

        public dynamic ProcessBehaviour(BehaviourDefinition behaviour, IEnumerable<KeyValuePair<string, string>> queryString)
        {
            ExpandoObject data = new ExpandoObject();

            if (behaviour.Data != null && behaviour.Data.Execute != null)
            {
                foreach (var messageHandler in behaviour.Data.Execute)
                {
                    ExecuteMessageHandler(ref data, messageHandler, queryString);
                }
            }

            return new {ComponentContainer = behaviour.ComponentContainer, Page = behaviour.Page, Data = data};
        }

        public dynamic ProcessBehaviourMessage(string behaviourId, string message, IEnumerable<KeyValuePair<string, string>> queryString)
        {
            try
            {
                BehaviourDefinition behaviour = GetBehaviour(behaviourId);
                if (!behaviour.Equals(default(BehaviourDefinition)))
                {
                    ExpandoObject data = new ExpandoObject();

                    if (behaviour.MessageHandlers != null)
                    {
                        MessageHandlerDefinition messageHandler =
                            behaviour.MessageHandlers.FirstOrDefault(t => t.Name == message);

                        if (messageHandler != null && !messageHandler.Equals(default(MessageHandlerDefinition)))
                        {
                            ExecuteMessageHandler(ref data, messageHandler, queryString);
                        }
                    }
                    return
                        new
                        {
                            ComponentContainer = behaviour.ComponentContainer,
                            Data = data,
                            Success = true,
                            Action = "Redirect",
                            ActionParams = "/sites/"
                        };
                }
                return null;
            }
            catch (Exception e)
            {
                return new {ComponentContainer = "", Data = e, Success = false};
            }
        } 

        public BehaviourDefinition GetBehaviour(string behaviourId)
        {
            var ids = Behaviours.Select(t => t.BehaviourId);
            var behaviour = Behaviours.FirstOrDefault(t => t.BehaviourId == behaviourId);

            if (behaviour == null) return null;

            bool hasPermission = false;
            //using (var dependency = new BehaviourContextDependencyWrapper())
            //{
            //    hasPermission = dependency.LoggedInUserHasPermission(behaviour);
            //}

            if (hasPermission) return behaviour;

            return null;
        }

        //todo - new implementation
        private void ExecuteMessageHandler(ref ExpandoObject data, MessageHandlerDefinition messageHandler, IEnumerable<KeyValuePair<string, string>> queryString)
        {
            var dataDict = data as IDictionary<string, object>;
            //var service = ((Colony.Util.Configuration.IDependencyResolver)Colony.Util.Configuration.DependencyResolver.Current).GetService(messageHandler.ServiceType);
            //Type serviceType = service.GetType();
            HttpRequest request = HttpContext.Current.Request;
            FormCollection f = new FormCollection(request.Form);
            
            object entity = null;

            //if (service != null)
            //{
            //    if (messageHandler.BindEntityFromParameters)
            //    {
            //        var methodinfo = serviceType.Method("GetEntityFromFormCollection", Flags.InstancePublic);
            //        //service.GetType().GetMethod("GetEntityFromFormCollection", BindingFlags.Public | BindingFlags.Instance);
            //        entity = methodinfo.Call(service, new object[] { f });

            //        //methodinfo.Invoke(service, new object[] { f });
            //        if (messageHandler.ServiceMethodParams == null)
            //        {
            //            messageHandler.ServiceMethodParams = new List<ParameterMapping>();
            //        }

            //        messageHandler.ServiceMethodParams.RemoveAll(v => v.Key == "entity");
            //        messageHandler.ServiceMethodParams.Add(new ParameterMapping() { Key = "entity", Value = entity });

            //        if (entity is BaseModeratedEntity && (messageHandler.ServiceMethod == "Update" || messageHandler.ServiceMethod == "Create"))
            //        {
            //            if (f.AllKeys.Contains("Publish"))
            //            {
            //                var value = f.GetValue("Publish").AttemptedValue.ToLowerInvariant();

            //                if (value == "on" || value == "true" || value == "yes")
            //                {
            //                    messageHandler.ServiceMethodParams.RemoveAll(t => t.Key == "publish");
            //                    messageHandler.ServiceMethodParams.Add(new ParameterMapping() { Key = "publish", Value = true });
            //                }
            //            }
            //        }

            //        //Validate model 
            //        var results = new List<ValidationResult>();
            //        var context = new ValidationContext(entity, null, null);

            //        if (!Validator.TryValidateObject(entity, context, results))
            //        {
            //            data.AddProperty("ValidationErrors", results);
            //            return;
            //        }
            //    }
            //    else
            //    {
            //        if (messageHandler.ServiceMethodParams != null)
            //        {
            //            foreach (var p in messageHandler.ServiceMethodParams)
            //            {
            //                if (!p.IsDynamic)
            //                {
            //                    var q = queryString.FirstOrDefault(v => v.Key.ToLowerInvariant() == p.Key.ToLowerInvariant());

            //                    if (!q.Equals(default(KeyValuePair<string, string>)))
            //                        p.Value = q.Value;
            //                    else
            //                    {
            //                        var fi = f.GetValue(p.Key.ToLowerInvariant());
            //                        if (fi != null)
            //                        {
            //                            if (!string.IsNullOrEmpty(fi.AttemptedValue))
            //                                p.Value = fi.AttemptedValue;
            //                        }
            //                    }
            //                }
            //                else
            //                {
            //                    var eo = new ExpandoObject();
            //                    var eoCollection = (ICollection<KeyValuePair<string, object>>)eo;

            //                    foreach (var q in queryString)
            //                    {
            //                        eoCollection.Add(new KeyValuePair<string, object>(q.Key, q.Value));
            //                    }

            //                    foreach (var fi in f.AllKeys)
            //                    {
            //                        if (!eoCollection.Any(t => t.Key == fi))
            //                            eoCollection.Add(new KeyValuePair<string, object>(fi, f[fi]));
            //                    }

            //                    p.Value = eo;
            //                }
            //            }
            //        }
            //    }

            //    //Convert all incoming parameters from string to correct type.
            //    IList<MethodInfo> methods = serviceType.Methods(Flags.InstancePublic, messageHandler.ServiceMethod);

            //    MethodInfo method = methods
            //        .FirstOrDefault(v => ((messageHandler.ServiceMethodParams != null && v.GetParameters().Length == messageHandler.ServiceMethodParams.Count)
            //        || (messageHandler.ServiceMethodParams == null && v.GetParameters().Length == 0 && ((!messageHandler.BindEntityFromParameters && entity == null)
            //        || v.GetParameters()[0].ParameterType == entity.GetType()))));

            //    //MethodInfo method = serviceType.GetMethods().Where(v => v.Name == messageHandler.ServiceMethod && ((messageHandler.ServiceMethodParams != null && v.GetParameters().Length == messageHandler.ServiceMethodParams.Count) || (messageHandler.ServiceMethodParams == null && v.GetParameters().Length == 0 && ((!messageHandler.BindEntityFromParameters && entity == null) || v.GetParameters()[0].ParameterType == entity.GetType())))).FirstOrDefault();

            //    if (method != null)
            //    {
            //        if (messageHandler.ServiceMethodParams != null && messageHandler.ServiceMethodParams.Count > 0)
            //        {
            //            ParameterInfo[] methodParamArray = method.GetParameters();

            //            if (methodParamArray != null)
            //            {
            //                foreach (ParameterInfo p in methodParamArray)
            //                {
            //                    var q = messageHandler.ServiceMethodParams.FirstOrDefault(v => v.Key.ToLowerInvariant() == p.Name.ToLowerInvariant());

            //                    if (q != null && q.Value.GetType() != p.ParameterType)
            //                    {
            //                        if (p.ParameterType.IsGenericType && typeof(IList).IsAssignableFrom(p.ParameterType))
            //                        {
            //                            var tmp = Activator.CreateInstance(p.ParameterType) as IList;

            //                            foreach (var item in q.Value.ToString().Split(",".ToCharArray()))
            //                            {
            //                                var x = Convert.ChangeType(item, p.ParameterType.GenericTypeArguments[0]);
            //                                tmp.Add(x);
            //                            }

            //                            q.Value = tmp;

            //                        }
            //                        if (!q.IsDynamic)
            //                        {
            //                            var t = Nullable.GetUnderlyingType(p.ParameterType) ?? p.ParameterType;

            //                            object safeValue = (q.Value == null) ? null : Convert.ChangeType(q.Value, t);

            //                            q.Value = safeValue;
            //                        }
            //                    }

            //                }

            //                dataDict.Add((string.IsNullOrEmpty(messageHandler.ReturnName)) ? messageHandler.Name : messageHandler.ReturnName, method.Call(service, messageHandler.ServiceMethodParams.Select(x => x.Value).ToArray()));
            //            }
            //        }
            //        else
            //            dataDict.Add(messageHandler.Name, method.Call(service, null));
            //    }
        }
    
    #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    ConfigWatcher.Dispose();
                    NavigationWatcher.Dispose();
                }
                _disposed = true;
            }
        }

        ~BehaviourContext()
        {
            Dispose(false);
        }

        #endregion        
    }
}