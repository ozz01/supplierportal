﻿namespace WalkerGreenBank.CMS.Services.Authentication
{
    public class UserAccount
    {
        public UserAccount()
        {
            Tenant = "ColonyCMSBackend";
            Username = "developer@redant.com";
            Email = "developer@redant.com";
            Title = "Mr";
            FirstName = "Developer";
            LastName = "Account";
        }

        public string Tenant { get; set; }
        public string Username { get; set; }
        public string Email { get; internal set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}