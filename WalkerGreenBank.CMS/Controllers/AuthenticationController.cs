﻿using System.Security.Claims;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using WalkerGreenBank.CMS.Helpers;
using WalkerGreenBank.CMS.Models;
using WalkerGreenBank.CMS.Services.Authentication;

namespace WalkerGreenBank.CMS.Controllers
{
    public class AuthenticationController : Controller
    {
        //private readonly IAuthenticationService _authenticationService;
        //private readonly IUserAccountService _userAccountService;

        [HttpPost]
        public ActionResult GetLoggedInUserAjax()
        {
            var user = new UserAccount(); //_authenticationService.GetLoggedInUser(Constants.TenantTypes.ColonyCMSBackend);

            object data;

            if (user != null)
                data = new { success = true, user };
            else
                data = new { success = false };

            var json = JsonConvert.SerializeObject(data, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                NullValueHandling = NullValueHandling.Ignore,
                MaxDepth = 10,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            return new ContentResult
            {
                Content = json,
                ContentType = "application/json"
            };
        }

        [HttpPost]
        public ActionResult LoginAjax(LoginInputViewModel model)
        {
            if (ModelState.IsValid)
            {
                //var authResult = _userAccountService.Authenticate(Constants.TenantTypes.ColonyCMSBackend, model.Email, model.Password);

                //if (authResult.LoginIsValid)
                //{
                    //_authenticationService.SignIn(Constants.TenantTypes.ColonyCMSBackend, model.Email);

                    var data = new
                        {
                            success = true,
                            user = new UserAccount() //_authenticationService.GetLoggedInUser(Constants.TenantTypes.ColonyCMSBackend)
                        };

                    var json = JsonConvert.SerializeObject(data, new JsonSerializerSettings
                    {
                        ContractResolver = new CamelCasePropertyNamesContractResolver(),
                        NullValueHandling = NullValueHandling.Ignore,
                        MaxDepth = 3,
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    });
                    
                    if (string.IsNullOrEmpty(json))
                        return Json(new { success = false, message = "Could not serialize user account information" });

                    return new ContentResult
                    {
                        Content = json,
                        ContentType = "application/json"
                    };
                //}

                //return Json(new { success = false, message = "Invalid Username or Password" });
            }

            return Json(new { success = false, message = "" });
        }

        [HttpPost]
        public ActionResult LogoutAjax()
        {
            PerformLogout(Constants.TenantTypes.ColonyCMSBackend);
        
            return Json(new { success = true });
        }
        
        private void PerformLogout(string tenant)
        {
            var cp = User as ClaimsPrincipal;
            if (cp != null)
            {
                var user = cp.GetForTenant(tenant);

                if (user != null && user.IsAuthenticated)
                {
                    //_authenticationService.SignOut(tenant == Constants.TenantTypes.ColonyCMSBackend);
                }
            }
            HttpContext.Items["loggedinuser"] = null;
            Session.Remove("facebooktoken");
        }
    }
}