﻿using System.Web.Mvc;

namespace WalkerGreenBank.CMS.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}