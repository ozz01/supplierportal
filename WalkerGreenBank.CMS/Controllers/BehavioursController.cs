﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WalkerGreenBank.CMS.Services;

namespace WalkerGreenBank.CMS.Controllers
{
    public class BehavioursController : ApiController
    {
        private readonly BehaviourContext Context = BehaviourContext.Current;

        public dynamic Get([FromUri] string behaviourId)
        {
            var behaviour = Context.GetBehaviour(behaviourId);

            return GetBehaviour(behaviour);
        }

        public dynamic GetByUri([FromUri] string module, [FromUri] string moduleaction = "index")
        {
            var behaviour = Context.GetBehaviourByUri(module, moduleaction);

            return GetBehaviour(behaviour);
        }

        public dynamic GetByBackendUri([FromUri] string uri)
        {
            var behaviour = Context.GetBehaviourByUri(uri);
            if (behaviour != null)
            {
                var behaviourResponse = GetBehaviour(behaviour, Request.GetQueryNameValuePairs());

                return behaviourResponse;
            }

            throw new HttpResponseException(HttpStatusCode.NotFound);
        }

        public dynamic Post([FromUri] string behaviourId, [FromUri] string message)
        {
            var behaviourResponse = Context.ProcessBehaviourMessage(behaviourId, message, Request.GetQueryNameValuePairs());

            return behaviourResponse;
        }

        private dynamic GetBehaviour(BehaviourDefinition behaviour, IEnumerable<KeyValuePair<string, string>> queryString = null)
        {
            return Context.ProcessBehaviour(behaviour, queryString);
        }
    }
}
