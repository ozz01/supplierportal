﻿{
    "behaviourId": "CmsListImageDownloadRequest",
	"messageHandlers": [{
		"Name": "ApproveRequest",
		"ServiceType": "WalkerGreenbank.Modules.PressMembers.Areas.ImageDownloadService.Services.IDownloadRequestService, WalkerGreenbank.Modules",
		"ServiceMethod": "Approve",
		"ServiceMethodParams": [
			{ "Key": "id", "Value": 1 }
		]
	}, {
	    "Name": "ApproveRequestWithGuidelines",
	    "ServiceType": "WalkerGreenbank.Modules.PressMembers.Areas.ImageDownloadService.Services.IDownloadRequestService, WalkerGreenbank.Modules",
	    "ServiceMethod": "ApproveWithGuidelines",
	    "ServiceMethodParams": [
			{ "Key": "id", "Value": 1 }
	    ]
	}, {
		"Name": "RejectRequest",
		"ServiceType": "WalkerGreenbank.Modules.PressMembers.Areas.ImageDownloadService.Services.IDownloadRequestService, WalkerGreenbank.Modules",
		"ServiceMethod": "Reject",
		"ServiceMethodParams": [
			{ "Key": "id", "Value": 1 }
		]
	}, {
	    "Name": "DeleteRequest",
	    "ServiceType": "WalkerGreenbank.Modules.PressMembers.Areas.ImageDownloadService.Services.IDownloadRequestService, WalkerGreenbank.Modules",
	    "ServiceMethod": "DeleteById",
	    "ServiceMethodParams": [
			{ "Key": "id", "Value": 1 }
	    ]
	}, {
	    "Name": "SearchImageDownloadRequest",
	    "ReturnName": "imagedownloadrequest",
	    "ServiceType": "WalkerGreenbank.Modules.PressMembers.Areas.ImageDownloadService.Services.IDownloadRequestService, WalkerGreenbank.Modules",
	    "ServiceMethod": "CmsSearch",
	    "ServiceMethodParams": [
			{"Key": "searchCriteria", "Value": "", "IsDynamic": true }
	    ]
	}],
	"page": {
		"label": "Press Image Download Requests"
	},
	"componentContainer": [{
	    "label": "",
	    "type": "tablist",
	    "componentId": "ImageDownloadTabs",
	    "componentId": "FeaturedTabs",
	    "actions": [
        {
            "icon": "file-alt",
            "url": "/brandguidelines/index/?siteId=<%=App.Colony.currentSiteId%>",
            "name": "Manage Brand Guidelines"
        }]
	}, {
	    "label": "Search",
	    "type": "searchform",
	    "components": [
			{
			    "label": "keywords",
			    "type": "fieldset",
			    "componentId": "keywordsFieldset",
			    "className": "half",
			    "components": [
					{
                        "label": "Filter by Site",
                        "componentId": "SiteID",
                        "type": "dropdown",
                        "dataCollection": "site"
                    }
			    ]
			}
	    ],
	    "messages": [
			{
			    "message": "SearchImageDownloadRequest",
			    "behaviourId": "CmsListImageDownloadRequest",
			    "Trigger": "submitSearch"
			}
	    ]
	}, 
    {
		"label": "",
		"type": "datatable",
		"componentId": "StateList",
		"dataCollection": "imagedownloadrequest",
		"map": [{
			"friendlyName": "Usage",
			"source": "usage"
		}, {
		    "friendlyName": "Publication Name",
		    "source": "publicationName"
		}, {
		    "friendlyName": "Publication Date",
		    "source": "publicationDate"
		}],
		"actions": [{
			"icon": "pencil",
			"url": "/edit/#/pressimagedownloads/edit/?id=<%=id%>",
			"name": "Edit this State"
		}, {
			"icon": "remove",
			"name": "Delete this State",
			"url": "/edit/#/pressimagedownloads/delete/?id=<%=id%>"
		}]
	}],
	"data": {
		"execute": [{
			"name": "imagedownloadrequest",
			"ServiceType": "WalkerGreenbank.Modules.PressMembers.Areas.ImageDownloadService.Services.IDownloadRequestService, WalkerGreenbank.Modules",
			"ServiceMethod": "GetAll"
		},
        {
            "name": "Site",
            "ServiceType": "Colony.Services.Core.Abstract.Sites.ISitesService, Colony.Services",
            "ServiceMethod": "GetAll",
            "map": [{
                "Name": "name",
                "PageID": "id"
            }]
        }
		]
	}
}