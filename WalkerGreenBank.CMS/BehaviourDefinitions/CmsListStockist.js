﻿{
	"behaviourId": "CmsListStockist",
	"messageHandlers": [{
		"Name": "EditStockist",
		"ServiceType": "Colony.Commerce.Services.Stockists.Abstract.IStockistService, Colony.Commerce.Services",
		"ServiceMethod": "Update",
		"BindEntityFromParameters": "true"
	}, {
		"Name": "AddStockist",
		"ServiceType": "Colony.Commerce.Services.Stockists.Abstract.IStockistService, Colony.Commerce.Services",
		"ServiceMethod": "Create",
		"BindEntityFromParameters": "true"
	}, {
		"Name": "DeleteStockist",
		"ServiceType": "Colony.Commerce.Services.Stockists.Abstract.IStockistService, Colony.Commerce.Services",
		"ServiceMethod": "DeleteById",
		"ServiceMethodParams": [
			{ "Key": "Id", "Value": 1 }
		]
	}, {
		"Name": "SearchStockist",
		"ReturnName": "stockist",
		"ServiceType": "Colony.Commerce.Services.Stockists.Abstract.IStockistService, Colony.Commerce.Services",
		"ServiceMethod": "CmsSearch",
		"ServiceMethodParams": [
			{"Key": "searchCriteria", "Value": "", "IsDynamic": true }
		]
	},
	{
	    "Name": "DeleteBatch",
	    "ServiceType": "Colony.Commerce.Services.Stockists.Abstract.IStockistService, Colony.Commerce.Services",
	    "ServiceMethod": "DeleteBatch",
	    "ServiceMethodParams": [
           { "Key": "ids", "Value": 0 }
	    ]
	}],
	"page": {
		"label": "Stockist Management"
	},
	"componentContainer": [{
		"label": "",
		"type": "tablist",
		"componentId": "StockistTabs",
		"actions": [{
			"icon": "plus",
			"url": "/stockists/add/",
			"name": "Add Stockist"
		}, {
			"icon": "globe",
			"url": "/countries/index/",
			"name": "Manage Countries/Regions"
		}, {
			"icon": "map-marker",
			"url": "/states/index/",
			"name": "Manage US States/Territories"
		}]
	}, {
		"label": "Search",
		"type": "searchform",
		"components": [
			{
				"label": "keywords",
				"type": "fieldset",
				"componentId": "keywordsFieldset",
				"className": "half",
				"components": [
					{
						"label": "Enter keywords",
						"componentId": "Keywords",
						"type": "text",
						"map": [
								{"friendlyName": "value", "source": "cache.keywords" }
						],
						"validation": {
							"required": true
						}
					},
					{
						"label": "Filter by shop",
						"componentId": "ShopID",
						"type": "dropdown",
						"dataCollection": "shop"
					},
					{
						"label": "Filter by geo-coding result",
						"componentId": "NonGeocodedStockists",
						"type": "dropdown",
						"items": [
						{"id": "1", "name": "Stockists without geo-coding information" },
						{"id": "0", "name": "Stockists with geo-coding information" }
						]
					}
				]
			}
		],
		"messages": [
			{
				"message": "SearchStockist",
				"behaviourId": "CmsListStockist",
				"Trigger": "submitSearch"
			}
		]
	}, {
		"label": "",
		"type": "datatable",
		"componentId": "StockistList",
		"dataCollection": "stockist",
		"map": [{
			"friendlyName": "Name",
			"source": "name"
		}],
		"csvMap": [{
		    "friendlyName": "Name",
		    "source": "name"
		}],
		"actions": [{
			"icon": "pencil",
			"url": "/edit/#/stockists/edit/?id=<%=id%>",
			"name": "Edit this Stockist"
		}, {
			"icon": "remove",
			"name": "Delete this Stockist",
			"url": "/edit/#/stockists/delete/?id=<%=id%>"
		}],
		"batchactions": [{
		    "icon": "remove",
		    "name": "Delete",
		    "messages": [{
		        "message": "DeleteBatch",
		        "behaviourId": "CmsListStockist"
		    }]
		}]
	}],
	"data": {
		"execute": [{
			"name": "shop",
			"ServiceType": "Colony.Commerce.Services.Shops.IShopsService, Colony.Commerce.Services",
			"ServiceMethod": "GetAll"
		}]
	}
}