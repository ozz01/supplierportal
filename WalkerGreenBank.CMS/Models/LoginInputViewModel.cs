﻿using System.ComponentModel.DataAnnotations;

namespace WalkerGreenBank.CMS.Models
{
    public class LoginInputViewModel
    {
        [Required]
        [EmailAddress]
        public virtual string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [ScaffoldColumn(false)]
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember Me")]
        [UIHint("Checkbox")]
        public bool RememberMe { get; set; }
    }
}