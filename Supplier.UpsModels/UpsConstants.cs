﻿using System.Collections.Generic;

namespace Supplier.UpsModels
{
    public static class UpsConstants
    {
        //Use Http Post for all requests
        public const string ApiBaseUrl = "https://onlinetools.ups.com/rest/";   //todo: Set in Config
        public const string ApiBaseUrlTest = "https://wwwcie.ups.com/rest/";    //todo: Set in Config

        //Test Values
        public const string UserName = "MATT ATKINSON";                 //todo: Setup in DB
        public const string Password = "Shipping1";                     //todo: Setup in DB
        public const string AccessLicenseNumber = "ED10F6D4AA974E68";   //todo: Setup in DB
        public const string ShipperNumber = "6A4421";                   //todo: Setup in DB

        public const string ShippingEndPoint = "Ship";
        public const string LabelEndPoint = "LBRecovery";
        public const string VoidShipmentEndPoint = "Void";

        public const string WeightCode = "KGS";                         //todo: Get from settings?
        public const string PackageWeight = "7";

        //todo - Put in settings or front end
        public const string LabelHeight = "6";                          //Fails if using anything else
        public const string LabelWidth = "4";

        //Test Values
        public const string TestTrackingNumber_Pdf = "1Z12345E8791315509";  
        public const string TestTrackingNumber_Html = "1Z12345E8791315413";
        public const string ShipmentIdNumber = "1Z6A44216892491739";


        public enum LabelFormat
        {
            GIF,
            ZPL,
            EPL,
            SPL,
            STARPL,
            PDF
        }

        public enum RequestValidationOption
        {
            Validated,
            NotValidated
        }

        //** Label Formats **//
        private static readonly Dictionary<LabelFormat, string> _labelFormatCodes = new Dictionary<LabelFormat, string>
        {
            {LabelFormat.PDF, "PDF"},
            {LabelFormat.GIF, "GIF"},
            {LabelFormat.ZPL, "ZPL"},
            {LabelFormat.EPL, "EPL"},
            {LabelFormat.SPL, "SPL"},
            {LabelFormat.STARPL, "STARPL"}  
        };

        public static string GetLabelFormatCode(LabelFormat formatCode)
        {
            string value;
            _labelFormatCodes.TryGetValue(formatCode, out value);

            return value;
        }

        private static readonly Dictionary<RequestValidationOption, string> _shippingRequestType = new Dictionary<RequestValidationOption, string>
        {
            {RequestValidationOption.Validated, "validate"},
            {RequestValidationOption.NotValidated, "nonvalidate"},
        };


        public static string GetShippingRequestType(RequestValidationOption typeKey)
        {
            string value;
            _shippingRequestType.TryGetValue(typeKey, out value);

            return value;
        }
    }
}

 /* Other Error Codes */     
/*
    {120107, "Missing/Invalid Shipper PostalCode. Postal Code length must be between 5 and 7."},
    {121210, "The selected service is not available from the origin to the destination."},          //
    {120108, "Missing or invalid shipper country code"},
    {120206, "Missing or invalid ship to state province code"},
    {120208, "Missing or invalid ship to country code"},
    {250001, "Invalid Access License for the tool. Please re-license."},
    {250002, "Invalid Authentication Information."},
    {250003, "Invalid Access License number."},
    {250004, "Incorrect UserId or Password."},
    {250005, "No Access and Authentication Credentials provided."},
    {250006, "The maximum number of user access attempts was exceeded."},
    {250007, "The UserId is currently locked out; please try again in 24 hours."},
    {250009, "License Number not found in the UPS database."},
    {250050, "License system not available."}
 */