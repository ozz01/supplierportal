﻿using System.Collections.Generic;

namespace Supplier.UpsModels.Common
{
    public class FileDataModel
    {
        public FileDataModel()
        {
            LabelData = new List<string>();
        }

        public string Action { get; set; }

        public string StandardPrinterName { get; set; }

        public string LabelPrinterName { get; set; }

        public string WorksheetData { get; set; }

        public string DespatchNoteData { get; set; }

        public List<string> LabelData { get; set; }
    }
}