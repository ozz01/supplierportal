﻿namespace Supplier.UpsModels.Common
{
    public partial class Shared
    {
        public class LabelStockSize
        {
            public string Height { get; set; }

            public string Width { get; set; }
        }
    }
}