﻿namespace Supplier.UpsModels.Common
{
    public partial class Shared
    {
        public class LookupType
        {
            public string Code { get; set; }
            public string Description { get; set; }
        }
    }
}