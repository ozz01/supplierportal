﻿namespace Supplier.UpsModels.Common
{
    public partial class Shared
    {
        public class Address
        {
            public string AddressLine { get; set; }
            public string City { get; set; }
            public string StateProvinceCode { get; set; }
            public string PostalCode { get; set; }
            public string CountryCode { get; set; }
        }        
    }
}