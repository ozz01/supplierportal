﻿namespace Supplier.UpsModels.Common
{
    public partial class Shared
    {
        public class Phone
        {
            public string Number { get; set; }
            public string Extension { get; set; }
        }
    }
}