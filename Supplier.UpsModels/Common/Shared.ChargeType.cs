﻿namespace Supplier.UpsModels.Common
{
    public partial class Shared
    {
        public class ChargeType
        {
            public string CurrencyCode { get; set; }
            public decimal MonetaryValue { get; set; }
        }
    }
}