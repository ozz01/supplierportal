﻿namespace Supplier.UpsModels.Common
{
    public partial class Shared
    {
        public class ZplLabelSpecification
        {
            public ZplLabelSpecification()
            {
                LabelImageFormat = new LookupType();
                LabelStockSize = new LabelStockSize();
            }

            public LookupType LabelImageFormat { get; set; }
            
            public LabelStockSize LabelStockSize { get; set; }

            public string HTTPUserAgent { get; set; }        
        }
    }
}