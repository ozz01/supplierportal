﻿namespace Supplier.UpsModels.Common
{
    public partial class Shared
    {
        //todo - Get AccessLicenseNumber, UserName and Password from DB or settings
        public class UpsSecurity
        {
            public UpsSecurity()
            {
                UsernameToken = new UsernameToken();
                ServiceAccessToken = new ServiceAccessToken();
            }

            public UsernameToken UsernameToken { get; set; }
            public ServiceAccessToken ServiceAccessToken { get; set; }
        }

        public class UsernameToken
        {
            public string Username { get; set; }

            public string Password { get; set; }
        }

        public class ServiceAccessToken
        {
            public string AccessLicenseNumber { get; set; }
        }
    }
}