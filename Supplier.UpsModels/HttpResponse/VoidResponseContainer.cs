﻿namespace Supplier.UpsModels.HttpResponse
{
    public class VoidResponseContainer
    {
        public UpsResponse.PrimaryErrorCode RequestError { get; set; }

        public UpsResponse.VoidShipmentResponse VoidShipmentResult { get; set; }
    }
}