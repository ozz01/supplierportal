﻿namespace Supplier.UpsModels.HttpResponse
{
    public partial class UpsResponse
    {
        public class FaultReponse
        {
            public Fault Fault { get; set; }    
        }

        public class Fault
        {
            public string faultcode { get; set; }
            public string faultstring { get; set; }
            public Detail detail { get; set; }
        }

        public class Detail
        {
            public Errors Errors { get; set; }
        }

        public class Errors
        {
            public ErrorDetail ErrorDetail { get; set; }
        }

        public class ErrorDetail
        {
            public string Severity { get; set; }
            public PrimaryErrorCode PrimaryErrorCode { get; set; }
            //public Location Location { get; set; }
            //public SubErrorCode SubErrorCode { get; set; }
        }

        public class PrimaryErrorCode
        {
            public string Code { get; set; }
            public string Description { get; set; }
        }

        //public class SubErrorCode
        //{
        //    public string Code { get; set; }
        //    public string Description { get; set; }
        //}

        //public class Location
        //{
        //    public string LocationElementName { get; set; }
        //    public string XPathOfElement { get; set; }
        //    public string OriginalValue { get; set; }
        //}
    }
}