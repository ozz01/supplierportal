﻿using Supplier.UpsModels.Common;

namespace Supplier.UpsModels.HttpResponse
{
    public partial class UpsResponse
    {
        public class Label
        {        
            public LabelRecoveryResponse LabelRecoveryResponse { get; set; }                  
        }

        public class LabelRecoveryResponse
        {
            public VoidResponse Response { get; set; }
            public LabelResults LabelResults { get; set; }
        }

        public class VoidResponse
        {
            public Shared.LookupType ResponseStatus { get; set; }
        }

        public class LabelResults
        {
            public string TrackingNumber { get; set; }
            public LabelImage LabelImage { get; set; }
            public Receipt Receipt { get; set; }
        }
        
        public class LabelImage
        {
            //Set if using Html Or PDF Type TrackingNumber
            public string GraphicImage { get; set; }

            //Set if using Html Type TrackingNumber
            public string HTMLImage { get; set; }

            //Set if using Html Type TrackingNumber
            public string PDF417 { get; set; }

            //Set if using Pdf / ZPL etc Type TrackingNumber
            public ImageFormat LabelImageFormat { get; set; }
        }

        public class Receipt
        {
            //Set if using Html Type TrackingNumber
            public string HTMLImage { get; set; }

            //Set if using Pdf / ZPL etc Type TrackingNumber
            public ImageType Image { get; set; }         
        }      

        public class ImageType
        {
            public ImageFormat ImageFormat { get; set; }

            public string GraphicImage { get; set; }
        }

        public class ImageFormat
        {
            public string Code { get; set; }
            public string GraphicImage { get; set; }
        }
    }
}