﻿namespace Supplier.UpsModels.HttpResponse
{
    public class LabelResponseContainer
    {
        public UpsResponse.PrimaryErrorCode RequestError { get; set; }

        public UpsResponse.LabelResults LabelResult { get; set; }
    }
}