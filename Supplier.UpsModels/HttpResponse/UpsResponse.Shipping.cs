﻿using Supplier.UpsModels.Common;

namespace Supplier.UpsModels.HttpResponse
{
    public partial class UpsResponse
    {
        public class Shipping
        {
            public ShipmentResponse ShipmentResponse { get; set; }
        }

        public class TransactionReference
        {
            public string CustomerContext { get; set; }
        }

        public class Response
        {
            public Shared.LookupType ResponseStatus { get; set; }
            public TransactionReference TransactionReference { get; set; }
        }
        
        public class ShipmentCharges
        {
            public Shared.ChargeType TransportationCharges { get; set; }
            public Shared.ChargeType ServiceOptionsCharges { get; set; }
            public Shared.ChargeType TotalCharges { get; set; }
        }     

        public class NegotiatedRateCharges
        {
            public Shared.ChargeType TotalCharge { get; set; }
        }


        public class BillingWeight
        {
            public Shared.LookupType UnitOfMeasurement { get; set; }
            public string Weight { get; set; }
        }


        public class ShippingLabel
        {
            public Shared.LookupType ImageFormat { get; set; }
            public string GraphicImage { get; set; }
            public string HTMLImage { get; set; }
        }

        public class PackageResults
        {
            public string TrackingNumber { get; set; }
            public Shared.ChargeType ServiceOptionsCharges { get; set; }
            public ShippingLabel ShippingLabel { get; set; }
        }

        public class ShipmentResults
        {
            public ShipmentCharges ShipmentCharges { get; set; }
            public NegotiatedRateCharges NegotiatedRateCharges { get; set; }
            public BillingWeight BillingWeight { get; set; }
            public string ShipmentIdentificationNumber { get; set; }
            public PackageResults PackageResults { get; set; }
        }

        public class ShipmentResponse
        {
            public Response Response { get; set; }
            public ShipmentResults ShipmentResults { get; set; }
        }
    }
}