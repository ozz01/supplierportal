﻿namespace Supplier.UpsModels.HttpResponse
{
    public class ShippingResponseContainer
    {
        public UpsResponse.PrimaryErrorCode RequestError { get; set; }

        public UpsResponse.ShipmentResults ShipmentData { get; set; }    
    }
}