﻿using Supplier.UpsModels.Common;

namespace Supplier.UpsModels.HttpResponse
{
    public partial class UpsResponse
    {
        public class VoidShipping
        {
            public VoidShipmentResponse VoidShipmentResponse { get; set; }
        }

        public class VoidShipmentResponse
        {
            public VoidResponseType Response { get; set; }
            public SummaryResult SummaryResult { get; set; }
        }
     
        public class VoidResponseType
        {
            //public Shared.LookupType ResponseStatus { get; set; }
            public Shared.TransactionReference TransactionReference { get; set; }
        }

        public class SummaryResult
        {
            public Shared.LookupType Status { get; set; }
        }
    }
}