﻿using Supplier.UpsModels.Common;

namespace Supplier.UpsModels.HttpRequest
{
    public partial class UpsRequest
    {
        public class VoidShipping
        {
            public VoidShipping()
            {
                UPSSecurity = new Shared.UpsSecurity();
                VoidShipmentRequest = new VoidShipmentRequest();
            }

            public Shared.UpsSecurity UPSSecurity { get; set; }

            public VoidShipmentRequest VoidShipmentRequest { get; set; }
        }

        public class VoidShipmentRequest
        {
            public VoidShipmentRequest()
            {
                Request = new ShipRequest();
                VoidShipment = new VoidShipment();
            }

            public ShipRequest Request { get; set; }
            public VoidShipment VoidShipment { get; set; }
        }

        public class ShipRequest
        {
            public ShipRequest()
            {
                TransactionReference = new Shared.TransactionReference();
            }

            public Shared.TransactionReference TransactionReference { get; set; }
        }

        public class VoidShipment
        {
            public string ShipmentIdentificationNumber { get; set; }
        }

    }
}