﻿using Supplier.UpsModels.Common;

namespace Supplier.UpsModels.HttpRequest
{
    public partial class UpsRequest
    {
        public class Shipping
        {
            public Shipping()
            {
                UPSSecurity = new Shared.UpsSecurity();
                ShipmentRequest = new ShipmentRequest();
            }

            public Shared.UpsSecurity UPSSecurity { get; set; }
            public ShipmentRequest ShipmentRequest { get; set; }
        }

        public class ShipmentRequest
        {
            public ShipmentRequest()
            {
                Request = new Request();
                Shipment = new Shipment();
                LabelSpecification = new Shared.ZplLabelSpecification();
            }

            public Request Request { get; set; }
            public Shipment Shipment { get; set; }
            public Shared.ZplLabelSpecification LabelSpecification { get; set; }
        }

        public class Request
        {
            public Request()
            {
                TransactionReference = new Shared.TransactionReference();
            }

            public string RequestOption { get; set; }
            public Shared.TransactionReference TransactionReference { get; set; }
        }

        public class Shipment
        {
            public Shipment()
            {
                Shipper = new Shipper();
                ShipTo = new ShipTo();
                ShipFrom = new ShipFrom();
                PaymentInformation = new PaymentInformation();
                Service = new Shared.LookupType();
                ShipmentRatingOptions = new ShipmentRatingOptions();
                Package = new Package();
            }

            public string Description { get; set; }
            public Shipper Shipper { get; set; }
            public ShipTo ShipTo { get; set; }
            public ShipFrom ShipFrom { get; set; }
            public PaymentInformation PaymentInformation { get; set; }
            public Shared.LookupType Service { get; set; }
            public ShipmentRatingOptions ShipmentRatingOptions { get; set; }
            public Package Package { get; set; }
        }            

        public class Shipper
        {
            public Shipper()
            {
                Phone = new Shared.Phone();
                Address = new Shared.Address();
            }

            public string Name { get; set; }
            public string AttentionName { get; set; }
            public string TaxIdentificationNumber { get; set; }
            public Shared.Phone Phone { get; set; }
            public string ShipperNumber { get; set; }
            public string FaxNumber { get; set; }
            public Shared.Address Address { get; set; }
        }
		
        //Must populate phone number if you wish to use it.
        public class ShipTo
        {
            public ShipTo()
            {
                //Phone = new Shared.Phone();
                Address = new Shared.Address();
            }

            public string Name { get; set; }
            public string AttentionName { get; set; }
            //public Shared.Phone Phone { get; set; }
            public Shared.Address Address { get; set; }
        }
		
        public class ShipFrom
        {
            public ShipFrom()
            {
                Phone = new Shared.Phone();
                Address = new Shared.Address();
            }

            public string Name { get; set; }
            public string AttentionName { get; set; }
            public Shared.Phone Phone { get; set; }
            public string FaxNumber { get; set; }
            public Shared.Address Address { get; set; }
        }

        public class BillShipper
        {
            public string AccountNumber { get; set; }
        }

        public class ShipmentCharge
        {
            public ShipmentCharge()
            {
                BillShipper = new BillShipper();
            }

            public string Type { get; set; }
            public BillShipper BillShipper { get; set; }
        }

        public class PaymentInformation
        {
            public PaymentInformation()
            {
                ShipmentCharge = new ShipmentCharge();
            }

            public ShipmentCharge ShipmentCharge { get; set; }
        }
        
        public class Dimensions
        {
            public Dimensions()
            {
                UnitOfMeasurement = new Shared.LookupType();
            }

            public Shared.LookupType UnitOfMeasurement { get; set; }
            public string Length { get; set; }
            public string Width { get; set; }
            public string Height { get; set; }
        }   

        public class PackageWeight
        {
            public PackageWeight()
            {
                UnitOfMeasurement = new Shared.LookupType();
            }

            public Shared.LookupType UnitOfMeasurement { get; set; }
            public string Weight { get; set; }
        }

        public class Package
        {
            public Package()
            {
                Packaging = new Shared.LookupType();
                Dimensions = new Dimensions();
                PackageWeight = new PackageWeight();
            }

            public string Description { get; set; }
            public Shared.LookupType Packaging { get; set; }
            public Dimensions Dimensions { get; set; }
            public PackageWeight PackageWeight { get; set; }
        }

        //todo - find out about values to use
        // use "0" or "1" testing
        public class ShipmentRatingOptions
        {
            public string NegotiatedRatesIndicator { get; set; }
        }      
    }
}