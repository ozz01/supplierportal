﻿using Supplier.UpsModels.Common;

namespace Supplier.UpsModels.HttpRequest
{
    public partial class UpsRequest
    {
        public class LabelRecovery
        {
            public LabelRecovery()
            {
                LabelRecoveryRequest = new LabelRecoveryRequest();
                UPSSecurity = new Shared.UpsSecurity();
            }

            public Shared.UpsSecurity UPSSecurity { get; set; }

            public LabelRecoveryRequest LabelRecoveryRequest { get; set; }
        }

        public class LabelRecoveryRequest
        {
            public LabelRecoveryRequest()
            {
                LabelSpecification = new LabelSpecification();

                Translate = new Translate();
            }

            //todo - move to shared class
            public LabelSpecification LabelSpecification { get; set; }

            public Translate Translate { get; set; }

            //Use UpsHelper.TestTrackingNumber_Pdf OR UpsHelper.TestTrackingNumber_Html for testing
            public string TrackingNumber { get; set; }
        }   

        public class Translate
        {
            //Use "eng"
            public string LanguageCode
            {
                get { return "eng"; }
            }

            //Use "GB"
            public string DialectCode
            {
                get { return "GB"; }
            }

            //todo - look into codes to use
            public string Code
            {
                get { return "01"; }
            }
        }

        public class LabelSpecification
        {          
            public string LabelImageFormatCode { get; set; }

            //return actual useagent
            public string HTTPUserAgent { get; set; }     
        }
    }
}