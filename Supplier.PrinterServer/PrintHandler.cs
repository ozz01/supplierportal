﻿using System;
using System.Drawing.Printing;
using System.IO;

namespace Supplier.PrinterServer
{
    public class PrintHandler
    {
        public static void PrintExcelSheet(string fileData, string printerName)
        {
            string _outputPath = "";

            try{
                _outputPath = Path.Combine(StorageProvider.GetAppDataFolder(), "PickingList.xlsx");

                byte[] buffer = Convert.FromBase64String(fileData);

                using (MemoryStream ms = new MemoryStream(buffer))
                {
                    using (Stream output = File.OpenWrite(_outputPath))
                    {
                        ms.CopyTo(output);
                    }
                }

                if (CheckPrinterExists(printerName))
                {
                    ProcessActionProvider.StartProcess(_outputPath, printerName, "Printto");
                }
                else
                {
                    ProcessActionProvider.StartProcess(_outputPath);
                }
                
            }
            //Todo - handle errors 
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //throw;
            }
        }

        public static void PrintZplLabel(string fileData, string printerName)
        {
            try
            {
                string _outputPath = Path.Combine(@"c:\temp\", "label.zpl");
                //string _outputPath = Path.Combine(StorageProvider.GetAppDataFolder(), "label.zpl");

                byte[] buffer = Convert.FromBase64String(fileData);
                
                using (MemoryStream ms = new MemoryStream(buffer))
                {
                    using (Stream output = File.OpenWrite(_outputPath))
                    {
                        ms.CopyTo(output);
                    }
                }

                if (CheckPrinterExists(printerName))
                {
                    RawPrinterHelper.SendFileToPrinter(printerName, _outputPath);
                }
            }
            //Todo - handle errors 
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //throw;
            }
        }
        
        //todo: 
        public static void PrintShippingDocument(string fileData, string printerName)
        {
            try
            {
                string _outputPath = Path.Combine(@"c:\temp\", "shippingList.html");
                //string _outputPath = Path.Combine(StorageProvider.GetAppDataFolder(), "hippingList.html");

                byte[] buffer = Convert.FromBase64String(fileData);

                using (MemoryStream ms = new MemoryStream(buffer))
                {
                    using (Stream output = File.OpenWrite(_outputPath))
                    {
                        ms.CopyTo(output);
                    }
                }

                if (CheckPrinterExists(printerName))
                {
                    RawPrinterHelper.SendFileToPrinter(printerName, _outputPath);
                }            
            }
            //Todo - handle errors 
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //throw;
            }
        }

        
        private static bool CheckPrinterExists(string expectedPrinterName)
        {
            foreach (string printerName in PrinterSettings.InstalledPrinters)
            {
                if (printerName.ToUpper() == expectedPrinterName.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }
    }
}