﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Supplier.UpsModels.Common;

namespace Supplier.PrinterServer
{
    //[WebPermission(SecurityAction.Assert, ConnectPattern = @"http://wgb2013\.contoso\.com/")]
    public class WebRequestProvider
    {
        public static FileDataModel GetFileData(string url)
        {           
             HttpClient _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
          
            string _outputPath = "";

            try
            {
                _httpClient.BaseAddress = new Uri(url);

                HttpResponseMessage response = _httpClient.GetAsync(url).Result;

                if (response.IsSuccessStatusCode)
                {
                    var contentString = response.Content.ReadAsStringAsync().Result;

                    FileDataModel model = JsonConvert.DeserializeObject<FileDataModel>(contentString);

                    return model;
                    //string labelImage = ShippingResult["FileData"];
                    //byte[] buffer = System.Text.Encoding.UTF8.GetBytes(labelImage);
                }
            }
            //Todo - handle errors 
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //throw;
            }


            return null;
        }
    }  
}