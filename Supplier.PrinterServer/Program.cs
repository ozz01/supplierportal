﻿using System;
using System.Collections.Specialized;
using System.Deployment.Application;
using System.Net;
using System.Web;
using Supplier.UpsModels.Common;

namespace Supplier.PrinterServer
{
    public class Program
    {
        private static IPEndPoint printerIP;
        private static string IP = "192.168.0.9";
        private static int port = 9100;
        private static string path = @"c:\temp\test.zpl";
        
        public static void Main(string[] args)
        {
            try
            {
                string _url = "";
             
                NameValueCollection nameValueTable = new NameValueCollection();

                if (ApplicationDeployment.IsNetworkDeployed)
                {
                    string queryString = ApplicationDeployment.CurrentDeployment.ActivationUri.Query;
                    nameValueTable = HttpUtility.ParseQueryString(queryString);

                    _url = nameValueTable.Get("url");

                    Console.WriteLine(_url);

                    if(string.IsNullOrEmpty(_url)) return;

                    FileDataModel model = WebRequestProvider.GetFileData(_url);

                    if (model != null)
                    {
                        switch (model.Action)
                        {
                            case "Shipping":
                                PrintHandler.PrintShippingDocument(model.DespatchNoteData, model.StandardPrinterName);

                                foreach (string labelData in model.LabelData)
                                {
                                    PrintHandler.PrintZplLabel(labelData, model.LabelPrinterName);    
                                }

                                break;
                            case "PickingList":
                                PrintHandler.PrintExcelSheet(model.WorksheetData, model.StandardPrinterName);
                                break;
                        }
                    }
                }
                else
                {
                    //_url = "http://localhost/SupplierPortal/api/FileDownload/PickingList/?userId=9";

                    //FileDataModel model = WebRequestProvider.GetFileData(_url);
                    //PrintHandler.PrintExcelSheet(model.FileData, model.PrinterName);
                    //RawPrinterHelper.SendFileToPrinter(GlobalConstants.StandardPrinter, @"c:\temp\label.zpl");

                    //StorageProvider.GetIsoaltedStorageLocation();

                    //string networkStorageDirectory =  StorageProvider.GetDataDirectory();
                    //Console.WriteLine("Network app data directory: {0}", networkStorageDirectory);

                    //string appdataFolder = StorageProvider.GetAppDataFolder();
                    //Console.WriteLine("Local app data directory: {0}", appdataFolder);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadLine();
        }

        //static void Test()
        //{
        //    //This works
        //    string ZPLString = "^XA^FO50,50^A0N50,50^FD" + "Hello, World!" + "^FS^XZ";
        //    RawPrinterHelper.SendStringToPrinter("GK420d", ZPLString);
        //}
 
        //public static void SendData()
        //{
        //    string ipAddress = "192.168.0.9";
        //    int port = 9100;
        
        //    // ZPL Command(s)
        //    string ZPLString =  "^XA^FO50,50^A0N50,50^FD" + "Hello, World!" +"^FS^XZ"; 

        //    try
        //    {
        //        // Open connection
        //        System.Net.Sockets.TcpClient client = new System.Net.Sockets.TcpClient();
        //        client.Connect(IP, port);

        //        // Write ZPL String to connection
        //        System.IO.StreamWriter writer = new System.IO.StreamWriter(client.GetStream());

        //        writer.Write(ZPLString);
        //        writer.Flush();
 

        //        // Close Connection
        //        writer.Close();
        //        client.Close();
        //    }

        //    catch (Exception ex)
        //    {
        //        // Catch Exception
        //    }
        //}
    }    
}
