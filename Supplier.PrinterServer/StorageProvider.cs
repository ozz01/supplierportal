﻿using System;
using System.Deployment.Application;
using System.IO;
using System.IO.IsolatedStorage;

namespace Supplier.PrinterServer
{
    public class StorageProvider
    {
        private const string LocalPath = "data";

        public static string GetDataDirectory()
        {
            string dir;

            if (ApplicationDeployment.IsNetworkDeployed)
            {
                ApplicationDeployment ad = ApplicationDeployment.CurrentDeployment;
                dir = Path.Combine(ad.DataDirectory, LocalPath);
            }
            else
            {
                dir = LocalPath;
            }

            return dir;
        }

        public static void GetIsoaltedStorageLocation()
        {
            //IsolatedStorageFile appScope = IsolatedStorageFile.GetUserStoreForApplication();
            IsolatedStorageFile isoStore = IsolatedStorageFile.GetStore(IsolatedStorageScope.User | IsolatedStorageScope.Assembly, null, null);

            if (isoStore.FileExists("TestStore.txt"))
            {
                Console.WriteLine("The file already exists in Isolated Storage!");
                Console.WriteLine("Updating Isolated Storage file content.");

                using (IsolatedStorageFileStream isoStream = new IsolatedStorageFileStream("TestStore.txt", FileMode.Create, isoStore))
                {
                    using (StreamWriter writer = new StreamWriter(isoStream))
                    {
                        writer.WriteLine("Updated Isolated Storage File data");
                        Console.WriteLine("You have written to the file.");
                    }
                }

                using (IsolatedStorageFileStream isoStream = new IsolatedStorageFileStream("TestStore.txt", FileMode.Open, isoStore))
                {
                    using (StreamReader reader = new StreamReader(isoStream))
                    {
                        Console.WriteLine("Reading Isolated Storage File data:");
                        Console.WriteLine(reader.ReadToEnd());
                    }
                }
            }
            else
            {
                using (IsolatedStorageFileStream isoStream = new IsolatedStorageFileStream("TestStore.txt", FileMode.CreateNew, isoStore))
                {
                    using (StreamWriter writer = new StreamWriter(isoStream))
                    {
                        writer.WriteLine("New Isolated Storage File data");
                        Console.WriteLine("You have written to the file.");
                    }
                }
            }   
        }

        public static string GetAppDataFolder()
        {
            string dir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);         

            return dir;
        }
    }
}