﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Supplier.PrinterServer
{
    public class ProcessActionProvider
    {
        public static void StartProcess(string path, string printerName = "", string verb = "Open")
        {
            string _output = "";

            try
            {
                ProcessStartInfo _process = new ProcessStartInfo(path);

                List<string> _expectedVerbs = _process.Verbs.Select(x => x.ToLower()).ToList();
                
                string _verbToUse = _process.Verbs.FirstOrDefault(i => i.ToLower() == verb.ToLower());

                if (verb.ToLower() == "printto" && !string.IsNullOrEmpty(printerName) && _expectedVerbs.Contains("printto"))
                {
                    _process.Arguments = "\"" + printerName + "\"";
                    _process.CreateNoWindow = true;
                    _process.WindowStyle = ProcessWindowStyle.Hidden;
                    _process.UseShellExecute = true;
                    _process.Verb = _verbToUse;

                    Process.Start(_process);                    
                }
                else if (verb.ToLower() == "open" && _expectedVerbs.Contains("open"))
                {
                    _process.UseShellExecute = true;
                    _process.Verb = _verbToUse;

                    Process.Start(_process);
                }

                string[] _pathSegments = path.Split('\\');
                string _fileName = _pathSegments[_pathSegments.Length - 1];
                
                Console.WriteLine("Started process({0}) for: {1}", verb, _fileName);
            }
            catch (System.ComponentModel.Win32Exception e)
            {
                _output = "Win32Exception caught!"
                    + Environment.NewLine
                    + string.Format("Win32 error = {0}", e.Message);

                Console.WriteLine(_output);
            }
            catch (System.InvalidOperationException)
            {
                // Catch this exception if the process exits quickly, 
                // and the properties are not accessible.
                _output = string.Format("Process {0} started with verb {1} but exited", path, verb);

                Console.WriteLine(_output);
            }
        }      
    }
}